# High Performance Computing

This project accompanies the lecture [High Performance
Computing](https://scalable.uni-jena.de/opt/hpc/). The answers to the tasks are
located in the dircetory `./answers` and a overview can be found in [answers/README.md](answers/README.md)
