#include <cstdint>
#include <arm_bf16.h>

extern "C" {
  void gemm_asm_sve_bfmmla_16_12_4( bfloat16_t const * i_a,
                                    bfloat16_t const * i_b,
                                    float            * io_c );

  void gemm_asm_sve_bfmmla_16_12_48( bfloat16_t const * i_a,
                                     bfloat16_t const * i_b,
                                     float            * io_c );
}
