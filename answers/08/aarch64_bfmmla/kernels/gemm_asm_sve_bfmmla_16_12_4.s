        .text
        .type gemm_asm_sve_bfmmla_16_12_4, %function
        .global gemm_asm_sve_bfmmla_16_12_4
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (16x12) = (16x4) * (4x12).
         * The input-data is of type bfloat.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */
gemm_asm_sve_bfmmla_16_12_4:
        // set bits for 8 bit view of predicate register p0 to 1
        ptrue p0.b
        mov x4, #0
        incp x4, p0.b

        // store
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        // Load C
        ld1w { z0.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z1.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z2.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z3.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z4.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z5.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z6.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z7.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z8.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z9.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z10.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z11.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z12.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z13.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z14.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z15.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z16.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z17.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z18.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z19.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z20.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z21.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z22.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z23.s}, p0/z, [x2]
        add x2, x2, x4

        // Load A
        ld1h {z24.h}, p0/z, [x0, #0 , MUL VL]
        ld1h {z25.h}, p0/z, [x0, #1 , MUL VL]
        ld1h {z26.h}, p0/z, [x0, #2 , MUL VL]
        ld1h {z27.h}, p0/z, [x0, #3 , MUL VL]

        // Load B
        ld1rqh {z28.h}, p0/z, [x1, #0]
        ld1rqh {z29.h}, p0/z, [x1, #16]
        ld1rqh {z30.h}, p0/z, [x1, #32]

         bfmmla  z0.s, z28.h, z24.h
         bfmmla  z1.s, z28.h, z25.h
         bfmmla  z2.s, z28.h, z26.h
         bfmmla  z3.s, z28.h, z27.h

         bfmmla  z4.s, z29.h, z24.h
         bfmmla  z5.s, z29.h, z25.h
         bfmmla  z6.s, z29.h, z26.h
         bfmmla  z7.s, z29.h, z27.h

         bfmmla  z8.s, z30.h, z24.h
         bfmmla  z9.s, z30.h, z25.h
         bfmmla z10.s, z30.h, z26.h
         bfmmla z11.s, z30.h, z27.h

         // Load B
         ld1rqh {z28.h}, p0/z, [x1, #48]
         ld1rqh {z29.h}, p0/z, [x1, #64]
         ld1rqh {z30.h}, p0/z, [x1, #80]

         bfmmla z12.s, z28.h, z24.h
         bfmmla z13.s, z28.h, z25.h
         bfmmla z14.s, z28.h, z26.h
         bfmmla z15.s, z28.h, z27.h

         bfmmla z16.s, z29.h, z24.h
         bfmmla z17.s, z29.h, z25.h
         bfmmla z18.s, z29.h, z26.h
         bfmmla z19.s, z29.h, z27.h

         bfmmla z20.s, z30.h, z24.h
         bfmmla z21.s, z30.h, z25.h
         bfmmla z22.s, z30.h, z26.h
         bfmmla z23.s, z30.h, z27.h

        // Store C
        sub x2, x2, x4
        st1w {z23.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z22.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z21.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z20.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z19.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z18.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z17.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z16.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z15.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z14.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z13.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z12.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z11.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z10.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z9.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z8.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z7.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z6.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z5.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z4.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z3.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z2.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z1.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z0.s}, p0, [x2]

        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ret
        .size gemm_asm_sve_bfmmla_16_12_4, (. - gemm_asm_sve_bfmmla_16_12_4)
