# include "convert.h"

void convert_a_to_bfmmla( uint64_t           i_m,
                          uint64_t           i_n,
                          uint64_t           i_ld,
                          bfloat16_t const * i_a_col_major,
                          bfloat16_t       * o_a_fmmla ) {
  for (uint64_t l_n = 0; l_n < i_n; l_n++) {
    for (uint64_t l_m = 0; l_m < i_m; l_m++) {
      uint64_t l_n_mod_4 = l_n % 4;
      uint64_t block_col = (l_n - l_n_mod_4) / 4;
      auto pos = l_m * 4 + l_n_mod_4 + block_col * i_ld * 4;
      o_a_fmmla[pos] = i_a_col_major[l_m + l_n * i_ld];
    }
  }
}

void convert_b_to_bfmmla( uint64_t           i_m,
                          uint64_t           i_n,
                          uint64_t           i_ld,
                          bfloat16_t const * i_b_col_major,
                          bfloat16_t       * o_b_fmmla ) {
  for (uint64_t l_n = 0; l_n < i_n; l_n+=2) {
    for (uint64_t l_m = 0; l_m < i_m; l_m+=4) {
        uint64_t block_pos = l_m * 2 + l_n * i_m;
        o_b_fmmla[block_pos + 0] = i_b_col_major[l_m + 0 + (l_n + 0) * i_ld];
        o_b_fmmla[block_pos + 1] = i_b_col_major[l_m + 1 + (l_n + 0) * i_ld];
        o_b_fmmla[block_pos + 2] = i_b_col_major[l_m + 2 + (l_n + 0) * i_ld];
        o_b_fmmla[block_pos + 3] = i_b_col_major[l_m + 3 + (l_n + 0) * i_ld];
        o_b_fmmla[block_pos + 4] = i_b_col_major[l_m + 0 + (l_n + 1) * i_ld];
        o_b_fmmla[block_pos + 5] = i_b_col_major[l_m + 1 + (l_n + 1) * i_ld];
        o_b_fmmla[block_pos + 6] = i_b_col_major[l_m + 2 + (l_n + 1) * i_ld];
        o_b_fmmla[block_pos + 7] = i_b_col_major[l_m + 3 + (l_n + 1) * i_ld];
    }
  }
}

void convert_c_to_bfmmla( uint64_t         i_m,
                          uint64_t         i_n,
                          uint64_t         i_ld,
                          float    const * i_c_col_major,
                          float          * o_c_fmmla ) {
  for (uint64_t l_n = 0; l_n < i_n; l_n+=2) {
    for (uint64_t l_m = 0; l_m < i_m; l_m+=2) {
        uint64_t block_pos = l_m * 2 + l_n * i_m;
        o_c_fmmla[block_pos + 0] = i_c_col_major[l_m + 0 + (l_n + 0) * i_ld];
        o_c_fmmla[block_pos + 1] = i_c_col_major[l_m + 1 + (l_n + 0) * i_ld];
        o_c_fmmla[block_pos + 2] = i_c_col_major[l_m + 0 + (l_n + 1) * i_ld];
        o_c_fmmla[block_pos + 3] = i_c_col_major[l_m + 1 + (l_n + 1) * i_ld];
    }
  }
}

void convert_c_from_bfmmla( uint64_t         i_m,
                            uint64_t         i_n,
                            uint64_t         i_ld,
                            float    const * i_c_fmmla,
                            float          * o_c_col_major ) {
  for (uint64_t l_n = 0; l_n < i_n; l_n+=2) {
    for (uint64_t l_m = 0; l_m < i_m; l_m+=2) {
        uint64_t block_pos = l_m * 2 + l_n * i_m;
        o_c_col_major[l_m + 0 + (l_n + 0) * i_ld] = i_c_fmmla[block_pos + 0];
        o_c_col_major[l_m + 1 + (l_n + 0) * i_ld] = i_c_fmmla[block_pos + 1];
        o_c_col_major[l_m + 0 + (l_n + 1) * i_ld] = i_c_fmmla[block_pos + 2];
        o_c_col_major[l_m + 1 + (l_n + 1) * i_ld] = i_c_fmmla[block_pos + 3];
    }
  }
}
