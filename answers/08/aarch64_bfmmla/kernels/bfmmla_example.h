#include <arm_bf16.h>

extern "C" {
  void bfmmla_example(bfloat16_t* i_a, bfloat16_t* i_b, float* io_c);
}
