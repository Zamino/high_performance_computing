        .text
        .align 4
        .type bfmmla, %function
        .global bfmmla_example
bfmmla_example:
        ldr z0, [x0]
        ldr z1, [x1]
        ldr z2, [x2]

        bfmmla z2.s, z1.h, z0.h

        str z2, [x2]
        ret
        .size bfmmla_example, (. - bfmmla_example)
