        .text
        .align 4
        .type peak_bfmmla, %function
        .global peak_bfmmla
peak_bfmmla:
        // PCS: save required data in SIMD registers to stack
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

loop_repeat:
        sub x0, x0, #1

        bfmmla z0.s , z31.h, z30.h
        bfmmla z1.s , z31.h, z30.h
        bfmmla z2.s , z31.h, z30.h
        bfmmla z3.s , z31.h, z30.h

        bfmmla z4.s , z31.h, z30.h
        bfmmla z5.s , z31.h, z30.h
        bfmmla z6.s , z31.h, z30.h
        bfmmla z7.s , z31.h, z30.h

        bfmmla z8.s , z31.h, z30.h
        bfmmla z9.s , z31.h, z30.h
        bfmmla z10.s, z31.h, z30.h
        bfmmla z11.s, z31.h, z30.h

        bfmmla z12.s, z31.h, z30.h
        bfmmla z13.s, z31.h, z30.h
        bfmmla z14.s, z31.h, z30.h
        bfmmla z15.s, z31.h, z30.h

        bfmmla z16.s, z31.h, z30.h
        bfmmla z17.s, z31.h, z30.h
        bfmmla z18.s, z31.h, z30.h
        bfmmla z19.s, z31.h, z30.h

        bfmmla z20.s, z31.h, z30.h
        bfmmla z21.s, z31.h, z30.h
        bfmmla z22.s, z31.h, z30.h
        bfmmla z23.s, z31.h, z30.h

        bfmmla z24.s, z31.h, z30.h
        bfmmla z25.s, z31.h, z30.h
        bfmmla z26.s, z31.h, z30.h
        bfmmla z27.s, z31.h, z30.h

        bfmmla z28.s, z31.h, z30.h
        bfmmla z29.s, z31.h, z30.h

        cbnz x0, loop_repeat
// end loop_repeat

        // PCS: restore SIMD registers
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16


        // write number of executions per loop to return register
        mov x0, 30

        ret
        .size peak_bfmmla, (. - peak_bfmmla)
