#include <stdint.h>
#include <arm_bf16.h>

extern "C" {
  uint64_t peak_bfmmla(uint64_t repetitions);
}
