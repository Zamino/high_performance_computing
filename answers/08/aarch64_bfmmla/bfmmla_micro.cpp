#include <omp.h>
#include <cstdint>
#include <cmath>
#include <chrono>
#include <omp.h>
#include <iostream>
#include <string>
#include <functional>

#include <arm_bf16.h>
#include <arm_neon.h>

#include "kernels/peak_bfmmla.h"

void print_csv_header() {
    std::cout
        << "name"
        << ",duration"
        << ",GFLOPS"
        << std::endl;
}

void run_benchmark(
    std::string name, std::function<uint64_t(uint64_t repetitions)> func,
    uint64_t m, uint64_t n, uint64_t k) {
  std::chrono::steady_clock::time_point l_tp0, l_tp1;
  std::chrono::duration< double > l_dur;
  uint64_t flop_per_iteration = m * n * k * 2; // multiplication + addition;
  flop_per_iteration *= 2; // 2 matrix multiplications
  uint64_t l_n_repetitions = 500000000;

  int l_n_threads = 1;
#pragma omp parallel
  {
#pragma omp master
   {
     l_n_threads = omp_get_num_threads();
   }
  }

  uint64_t executions_per_loop = 0;

#pragma omp parallel
  {
    // dry-run
    func(1);
#pragma omp barrier
#pragma omp master
    {
      l_tp0 = std::chrono::steady_clock::now();
    }
    auto tmp = func(l_n_repetitions);
#pragma omp barrier
#pragma omp master
    {
      l_tp1 = std::chrono::steady_clock::now();
      executions_per_loop = tmp;
    }
  }

  l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );

  double l_g_flops = flop_per_iteration;
  l_g_flops *= l_n_repetitions;
  l_g_flops *= l_n_threads;
  l_g_flops *= 1.0E-9;
  l_g_flops *= executions_per_loop;
  l_g_flops /= l_dur.count();

  std::cout << name; // name
  std::cout << "," << l_dur.count(); // duration
  std::cout << "," << l_g_flops; // GFLOPS
  std::cout << std::endl;
}

int main() {
  print_csv_header();
  run_benchmark("bfmmla_micro", &peak_bfmmla,  2,  2,  4);

  return EXIT_SUCCESS;
}
