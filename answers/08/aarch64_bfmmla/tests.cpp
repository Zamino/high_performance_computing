#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <stdint.h>
#include <iostream>

#include "arm_neon.h"
#include "arm_bf16.h"

#include "kernels/convert.h"
#include "kernels/gemm_ref.cpp"
#include "kernels/gemm_asm_sve_bfmmla.h"

void fill_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda,
    float value
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = value;
    }
  }
}

void fill_matrix(
    bfloat16_t* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda,
    float value
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = vcvth_bf16_f32(value);
    }
  }
}

void fill_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = (m * i_n + n) / 100.0;
    }
  }
}

void fill_matrix(
    bfloat16_t* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = vcvth_bf16_f32((m * i_n + n) / 100.0);
    }
  }
}

void create_matrices(
        bfloat16_t** a, bfloat16_t** b, float** c,
        int64_t m, int64_t n, int64_t k
        ) {
  *a = new bfloat16_t[k * m];
  *b = new bfloat16_t[n * k];
  *c = new float[n * m];

  fill_matrix(*a, m, k, m);
  fill_matrix(*b, k, n, k);
  fill_matrix(*c, m, n, m);
}

void print_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      std::cout << a[m + i_lda * n] << ", ";
    }
    std::cout << std::endl;
    if (m % 32 == 31)
      std::cout << std::endl;
  }
  std::cout << std::endl;
}

void print_matrix(
    bfloat16_t* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      std::cout << vcvtah_f32_bf16(a[m + i_lda * n]) << ", ";
    }
    std::cout << std::endl;
    if (m % 32 == 31)
      std::cout << std::endl;
  }
  std::cout << std::endl;
}


void copy_matrix(
    float* a, float* b,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = b[m + i_lda * n];
    }
  }
}

void compare_matrices(
    float* a, float* b,
    int64_t i_m, int64_t i_n,
    int64_t i_lda, int64_t i_ldb,
    int precision
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      REQUIRE_THAT(a[m + i_lda * n],
                   Catch::Matchers::WithinULP(b[m + i_lda * n], precision));
    }
  }
}

void delete_matrices(bfloat16_t* a, bfloat16_t* b, float* c) {
    delete[] a;
    delete[] b;
    delete[] c;
}

TEST_CASE( "gemm_asm_sve_bfmmla_16_12_4", "[gemm_asm_sve_bfmmla_16_12_4]") {
  int64_t   m = 16; int64_t   n = 12; int64_t   k = 4;
  int64_t i_m = 16; int64_t i_n = 12; int64_t i_k = 4;
  bfloat16_t *a, *b;
  float *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  float* c = new float[m * n];
  copy_matrix(
      c, c_ref,
      m, n,
      m
      );

  bfloat16_t bfmmla_a[m*k];
  bfloat16_t bfmmla_b[k*n];
  float      bfmmla_c[m*n];

  convert_a_to_bfmmla(m, k, m, a, bfmmla_a);
  convert_b_to_bfmmla(k, n, k, b, bfmmla_b);
  convert_c_to_bfmmla(m, n, m, c, bfmmla_c);

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  gemm_asm_sve_bfmmla_16_12_4(bfmmla_a, bfmmla_b, bfmmla_c);
  convert_c_from_bfmmla(m, n, m, bfmmla_c, c);

  compare_matrices(
      c_ref, c,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(a, b, c_ref);
  delete[] c;
}

TEST_CASE( "gemm_asm_sve_bfmmla_16_12_48", "[gemm_asm_sve_bfmmla_16_12_48]") {
  int64_t   m = 16; int64_t   n = 12; int64_t   k = 48;
  int64_t i_m = 16; int64_t i_n = 12; int64_t i_k = 48;
  bfloat16_t *a, *b;
  float *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  float* c = new float[m * n];
  copy_matrix(
      c, c_ref,
      m, n,
      m
      );

  bfloat16_t bfmmla_a[m*k];
  bfloat16_t bfmmla_b[k*n];
  float      bfmmla_c[m*n];

  convert_a_to_bfmmla(m, k, m, a, bfmmla_a);
  convert_b_to_bfmmla(k, n, k, b, bfmmla_b);
  convert_c_to_bfmmla(m, n, m, c, bfmmla_c);

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  gemm_asm_sve_bfmmla_16_12_48(bfmmla_a, bfmmla_b, bfmmla_c);
  convert_c_from_bfmmla(m, n, m, bfmmla_c, c);

  compare_matrices(
      c_ref, c,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(a, b, c_ref);
  delete[] c;
}
