#include <arm_bf16.h>
#include <arm_neon.h>
#include <iostream>
#include "kernels/convert.h"

int main( int /*i_argc*/, char const ** /*i_argv*/ ) {

  uint64_t m = 16;
  uint64_t n = 16;
  uint64_t k = 8;

  bfloat16_t i_a[m*k];
  bfloat16_t i_b[k*n];
  float      i_c[m*n];

  for (int64_t i = 0; i < m; ++i) {
    for (int64_t j = 0; j < k; ++j) {
      float f = i*k + j;
      i_a[i+j*m] = vcvth_bf16_f32(f);
    }
  }

  for (int64_t i = 0; i < k; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      float f = i*n + j;
      i_b[i+j*k] = vcvth_bf16_f32(f);
    }
  }

  for (int64_t i = 0; i < m; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      i_c[i+j*m] = i*n + j;
    }
  }

  bfloat16_t o_a[16* 8];
  bfloat16_t o_b[ 8*16];
  float      o_c[16*16];

  convert_a_to_bfmmla(m, k, m, i_a, o_a);
  convert_b_to_bfmmla(k, n, k, i_b, o_b);
  convert_c_to_bfmmla(m, n, m, i_c, o_c);

  std::cout << "converted A:" << std::endl;
  for (int64_t i = 0; i < m*k; ++i) {
    std::cout << vcvtah_f32_bf16(o_a[i]) << "\t";
    if (i % 4 == 3)
      std::cout << std::endl;
    if (i % 8 == 7)
      std::cout << std::endl;
    if (i % 64 == 63)
      std::cout << std::endl;
  }

  std::cout << "B:" << std::endl;
  for (int64_t i = 0; i < k; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      std::cout << vcvtah_f32_bf16(i_b[i+j*k]) << "\t";
    }
    std::cout << std::endl;
  }

  std::cout << "converted B:" << std::endl;
  for (int64_t i = 0; i < 8; ++i) {
    for (int64_t j = 0; j < 2; ++j) {
      uint64_t block_offset = j*8 + i*16;
      for (int64_t k = 0; k < 4; ++k) {
        std::cout << vcvtah_f32_bf16(o_b[block_offset+k])
          << "\t" << vcvtah_f32_bf16(o_b[block_offset+k+4]) << std::endl;
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }


  std::cout << "C:" << std::endl;
  for (int64_t i = 0; i < m; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      std::cout << i_c[i+j*m] << "\t";
    }
    std::cout << std::endl;
  }

  std::cout << "converted C:" << std::endl;
  for (int64_t i = 0; i < 8; ++i) {
    for (int64_t j = 0; j < 8; ++j) {
      uint64_t block_offset = j*4 + i*32;
      for (int64_t k = 0; k < 2; ++k) {
        std::cout << o_c[block_offset+k]
          << "\t" << o_c[block_offset+k+2] << std::endl;
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }

  convert_c_from_bfmmla(m, n, m, o_c, i_c);

  std::cout << "back to C:" << std::endl;
  for (int64_t i = 0; i < m; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      std::cout << i_c[i+j*m] << "\t";
    }
    std::cout << std::endl;
  }

  return EXIT_SUCCESS;
}
