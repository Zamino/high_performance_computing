#include <arm_bf16.h>
#include <arm_neon.h>
#include <iostream>
#include "kernels/bfmmla_example.h"

int main( int /*i_argc*/, char const ** /*i_argv*/ ) {

  bfloat16_t i_a[16];
  bfloat16_t i_b[16];
  float i_c[8];

  for (int64_t i = 0; i < 16; ++i) {
    float f = i;
    i_a[i] = vcvth_bf16_f32(f);
    i_b[i] = vcvth_bf16_f32(f);
  }
  for (int64_t i = 0; i < 8; ++i) {
    float f = i;
    i_c[i] = f;
  }

  std::cout << "before C:" << std::endl;
  for (int64_t i = 0; i < 8; ++i) {
    std::cout << i_c[i] << " ";
    if (i % 2 == 1) {
      std::cout << std::endl;
    }
    if (i % 4 == 3) {
      std::cout << std::endl;
    }
  }

  bfmmla_example(i_a, i_b, i_c);

  std::cout << "A:" << std::endl;
  for (int64_t i = 0; i < 16; ++i) {
    std::cout << vcvtah_f32_bf16(i_a[i]) << " ";
    if (i % 4 == 3) {
      std::cout << std::endl;
    }
    if (i % 8 == 7) {
      std::cout << std::endl;
    }
  }

  std::cout << "B:" << std::endl;
  for (int64_t i = 0; i < 16; ++i) {
    std::cout << vcvtah_f32_bf16(i_b[i]) << " ";
    if (i % 2 == 1) {
      std::cout << std::endl;
    }
    if (i % 8 == 7) {
      std::cout << std::endl;
    }
  }

  std::cout << "C:" << std::endl;
  for (int64_t i = 0; i < 8; ++i) {
    std::cout << i_c[i] << " ";
    if (i % 2 == 1) {
      std::cout << std::endl;
    }
    if (i % 4 == 3) {
      std::cout << std::endl;
    }
  }

  return EXIT_SUCCESS;
}
