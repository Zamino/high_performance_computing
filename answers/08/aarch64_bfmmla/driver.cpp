#include <cstdint>
#include <cmath>
#include <chrono>
#include <omp.h>
#include <iostream>
#include <string>
#include <functional>

#include "arm_bf16.h"
#include "arm_neon.h"

#include "kernels/gemm_ref.cpp"
#include "kernels/gemm_asm_sve_bfmmla.h"

inline void gemm_asm_sve_bfmmla_16_12_4_wrapper(
    bfloat16_t*  const i_a,   bfloat16_t*   const i_b,   float*  io_c,
    [[maybe_unused]] int64_t i_m,   [[maybe_unused]] int64_t i_n,   [[maybe_unused]] int64_t i_k,
    [[maybe_unused]] int64_t i_lda, [[maybe_unused]] int64_t i_ldb, [[maybe_unused]] int64_t i_ldc
    ) {
    gemm_asm_sve_bfmmla_16_12_4(i_a, i_b, io_c);
}

inline void gemm_asm_sve_bfmmla_16_12_48_wrapper(
    bfloat16_t*  const i_a,   bfloat16_t*   const i_b,   float*  io_c,
    [[maybe_unused]] int64_t i_m,   [[maybe_unused]] int64_t i_n,   [[maybe_unused]] int64_t i_k,
    [[maybe_unused]] int64_t i_lda, [[maybe_unused]] int64_t i_ldb, [[maybe_unused]] int64_t i_ldc
    ) {
    gemm_asm_sve_bfmmla_16_12_48(i_a, i_b, io_c);
}


void print_csv_header() {
    std::cout
        << "name"
        << ",m" << ",n" << ",k"
        << ",duration"
        << ",executions"
        << ",GFLOPS"
        << std::endl;
}

void run_benchmark(
    std::string name, std::function<void(
      bfloat16_t*  const, bfloat16_t*   const , float*,  // data
      int64_t           , int64_t             , int64_t, // dimensions
      int64_t           , int64_t             , int64_t  // row lengths
      )> func,
    uint64_t m, uint64_t n, uint64_t k, uint64_t repetitions) {
  bfloat16_t* a = new bfloat16_t[k * m];
  for (uint64_t i = 0; i < k * m; i++) {
    a[i] = vcvth_bf16_f32(0);
  }
  bfloat16_t* b = new bfloat16_t[n * k];
  for (uint64_t i = 0; i < n * k; i++) {
    b[i] = vcvth_bf16_f32(0);
  }
  float* c = new float[n * m];
  for (uint64_t i = 0; i < n * m; i++) {
    c[i] = 0.0;
  }
  std::chrono::steady_clock::time_point l_tp0, l_tp1;
  std::chrono::duration< double > l_dur;
  volatile uint64_t flop_per_iteration = m * n * k * 2; // multiplication + addition;

  // dry-run
  func(
    a, b, c,
    m, n, k,
    m, k, m
  );

  l_tp0 = std::chrono::steady_clock::now();
  for (uint64_t i = 0; i < repetitions; i++) {
      func(
              a, b, c,
              m, n, k,
              m, k, m
          );
  }
  l_tp1 = std::chrono::steady_clock::now();

  l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );

  volatile double l_g_flops = flop_per_iteration * 1.0;
  l_g_flops *= 1.0E-9;
  l_g_flops *= repetitions;
  l_g_flops /= l_dur.count();

  std::cout << name; // name
  std::cout << "," << m << "," << n << "," << k; // dimensions
  std::cout << "," << l_dur.count(); // duration
  std::cout << "," << repetitions; // executions
  std::cout << "," << l_g_flops; // GFLOPS
  std::cout << std::endl;

  delete[] a; delete[] b; delete[] c;
}

int main() {
  print_csv_header();
  // run_benchmark("gemm_ref", &gemm_ref,  16,  6,  1, 10000000);
  run_benchmark(
      "gemm_asm_sve_bfmmla_16_12_4", &gemm_asm_sve_bfmmla_16_12_4_wrapper,
      16, 12, 4,
      50000000);
  run_benchmark(
      "gemm_asm_sve_bfmmla_16_12_48", &gemm_asm_sve_bfmmla_16_12_48_wrapper,
      16, 12, 48,
      50000000);

  return EXIT_SUCCESS;
}
