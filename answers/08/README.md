# 08

## 9. BFLoat16 Extensions

### 9.1 Microkernel

#### 1.

```
$ ./bfmmla_example
before C:
0 1
2 3

4 5
6 7

A:
0 1 2 3
4 5 6 7

8 9 10 11
12 13 14 15

B:
0 1
2 3
4 5
6 7

8 9
10 11
12 13
14 15

C:
14 39
40 129

370 523
524 741

```

$
14  = 0 + (0 * 0) + (1 * 1) + (2 * 2) + (3 * 3)\\
39  = 1 + (4 * 0) + (5 * 1) + (6 * 2) + (7 * 3)\\
40  = 2 + (0 * 4) + (1 * 5) + (2 * 6) + (3 * 7)\\
129 = 3 + (4 * 4) + (5 * 5) + (6 * 6) + (7 * 7)
$


#### 2.

`kernels/peak_bfmmla.h`
```cpp
#include <stdint.h>
#include <arm_bf16.h>

extern "C" {
  uint64_t peak_bfmmla(uint64_t repetitions);
}
```

`./aarch64_bfmmla/kernels/peak_bfmmla.s`
```assembly
        .text
        .align 4
        .type peak_bfmmla, %function
        .global peak_bfmmla
peak_bfmmla:
        // PCS: save required data in SIMD registers to stack
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

loop_repeat:
        sub x0, x0, #1

        bfmmla z0.s , z31.h, z30.h
        bfmmla z1.s , z31.h, z30.h
        bfmmla z2.s , z31.h, z30.h
        bfmmla z3.s , z31.h, z30.h

        bfmmla z4.s , z31.h, z30.h
        bfmmla z5.s , z31.h, z30.h
        bfmmla z6.s , z31.h, z30.h
        bfmmla z7.s , z31.h, z30.h

        bfmmla z8.s , z31.h, z30.h
        bfmmla z9.s , z31.h, z30.h
        bfmmla z10.s, z31.h, z30.h
        bfmmla z11.s, z31.h, z30.h

        bfmmla z12.s, z31.h, z30.h
        bfmmla z13.s, z31.h, z30.h
        bfmmla z14.s, z31.h, z30.h
        bfmmla z15.s, z31.h, z30.h

        bfmmla z16.s, z31.h, z30.h
        bfmmla z17.s, z31.h, z30.h
        bfmmla z18.s, z31.h, z30.h
        bfmmla z19.s, z31.h, z30.h

        bfmmla z20.s, z31.h, z30.h
        bfmmla z21.s, z31.h, z30.h
        bfmmla z22.s, z31.h, z30.h
        bfmmla z23.s, z31.h, z30.h

        bfmmla z24.s, z31.h, z30.h
        bfmmla z25.s, z31.h, z30.h
        bfmmla z26.s, z31.h, z30.h
        bfmmla z27.s, z31.h, z30.h

        bfmmla z28.s, z31.h, z30.h
        bfmmla z29.s, z31.h, z30.h

        cbnz x0, loop_repeat
// end loop_repeat

        // PCS: restore SIMD registers
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16


        // write number of executions per loop to return register
        mov x0, 30

        ret
        .size peak_bfmmla, (. - peak_bfmmla)
```

`bfmmla_micro.cpp`
```cpp
#include <cstdint>
#include <cmath>
#include <chrono>
#include <omp.h>
#include <iostream>
#include <string>
#include <functional>

#include <arm_bf16.h>
#include <arm_neon.h>

#include "kernels/peak_bfmmla.h"

void print_csv_header() {
    std::cout
        << "name"
        << ",m" << ",n" << ",k"
        << ",repetitions"
        << ",duration"
        << ",GFLOPS"
        << std::endl;
}

void run_benchmark(
    std::string name, std::function<uint64_t(uint64_t repetitions)> func,
    uint64_t m, uint64_t n, uint64_t k) {
  std::chrono::steady_clock::time_point l_tp0, l_tp1;
  std::chrono::duration< double > l_dur;
  uint64_t flop_per_iteration = m * n * k * 2; // multiplication + addition;
  flop_per_iteration *= 2; // 2 matrix multiplications
  uint64_t l_n_repetitions = 500000000;

  l_tp0 = std::chrono::steady_clock::now();
  auto executions_per_loop = func(l_n_repetitions);
  l_tp1 = std::chrono::steady_clock::now();

  l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );

  double l_g_flops = flop_per_iteration;
  l_g_flops *= l_n_repetitions;
  l_g_flops *= 1.0E-9;
  l_g_flops *= executions_per_loop;
  l_g_flops /= l_dur.count();

  std::cout << name; // name
  std::cout << "," << m << "," << n << "," << k; // dimensions
  std::cout << "," << l_n_repetitions; // repetitions
  std::cout << "," << l_dur.count(); // duration
  std::cout << "," << l_g_flops; // GFLOPS
  std::cout << std::endl;
}

int main() {
  print_csv_header();
  run_benchmark("bfmmla_micro", &peak_bfmmla,  2,  2,  4);

  return EXIT_SUCCESS;
}
```

**Single core:**
```
$ ./bfmmla_micro
name,duration,GFLOPS
bfmmla_micro,2.88716,332.507
```

**Multi core:**
```
$ ./bfmmla_micro
name,duration,GFLOPS
bfmmla_micro,4.13921,927.714
```

##### Theoretical Peak Performance

**`bfmmla`**:\
$takt\_frequenzy = 2600MHz = 2.6GTakt/s$\
$execution\_throughput = 2OP/Takt$\
$FLOP\_per\_OP = 2 * 2 * 2 * 4 * 2 FLOP/OP = 64FLOP/OP$

$theoretical\_peak\_performance = takt\_frequenzy * cores * execution\_throughput * FLOP\_per\_OP$

$theoretical\_peak\_performance = 2.6GTakt/s * 1 * 2OP/Takt * 64FLOP/OP = 332.8GFLOPS$

$theoretical\_peak\_performance = 2.6GTakt/s * 4 * 2OP/Takt * 64FLOP/OP = 1,331.2GFLOPS$

Without parallelism, the theoretical peak performance is achieved, but when 4
cores compute simultaneously, only 69.7% of it is reached.

#### 3.

`kernels/convert.cpp`
```cpp
# include "convert.h"

void convert_a_to_bfmmla( uint64_t           i_m,
                          uint64_t           i_n,
                          uint64_t           i_ld,
                          bfloat16_t const * i_a_col_major,
                          bfloat16_t       * o_a_fmmla ) {
  for (uint64_t l_n = 0; l_n < i_n; l_n++) {
    for (uint64_t l_m = 0; l_m < i_m; l_m++) {
      uint64_t l_n_mod_4 = l_n % 4;
      uint64_t block_col = (l_n - l_n_mod_4) / 4;
      auto pos = l_m * 4 + l_n_mod_4 + block_col * i_ld * 4;
      o_a_fmmla[pos] = i_a_col_major[l_m + l_n * i_ld];
    }
  }
}

void convert_b_to_bfmmla( uint64_t           i_m,
                          uint64_t           i_n,
                          uint64_t           i_ld,
                          bfloat16_t const * i_b_col_major,
                          bfloat16_t       * o_b_fmmla ) {
  for (uint64_t l_n = 0; l_n < i_n; l_n+=2) {
    for (uint64_t l_m = 0; l_m < i_m; l_m+=4) {
        uint64_t block_pos = l_m * 2 + l_n * i_m;
        o_b_fmmla[block_pos + 0] = i_b_col_major[l_m + 0 + (l_n + 0) * i_ld];
        o_b_fmmla[block_pos + 1] = i_b_col_major[l_m + 1 + (l_n + 0) * i_ld];
        o_b_fmmla[block_pos + 2] = i_b_col_major[l_m + 2 + (l_n + 0) * i_ld];
        o_b_fmmla[block_pos + 3] = i_b_col_major[l_m + 3 + (l_n + 0) * i_ld];
        o_b_fmmla[block_pos + 4] = i_b_col_major[l_m + 0 + (l_n + 1) * i_ld];
        o_b_fmmla[block_pos + 5] = i_b_col_major[l_m + 1 + (l_n + 1) * i_ld];
        o_b_fmmla[block_pos + 6] = i_b_col_major[l_m + 2 + (l_n + 1) * i_ld];
        o_b_fmmla[block_pos + 7] = i_b_col_major[l_m + 3 + (l_n + 1) * i_ld];
    }
  }
}

void convert_c_to_bfmmla( uint64_t         i_m,
                          uint64_t         i_n,
                          uint64_t         i_ld,
                          float    const * i_c_col_major,
                          float          * o_c_fmmla ) {
  for (uint64_t l_n = 0; l_n < i_n; l_n+=2) {
    for (uint64_t l_m = 0; l_m < i_m; l_m+=2) {
        uint64_t block_pos = l_m * 2 + l_n * i_m;
        o_c_fmmla[block_pos + 0] = i_c_col_major[l_m + 0 + (l_n + 0) * i_ld];
        o_c_fmmla[block_pos + 1] = i_c_col_major[l_m + 1 + (l_n + 0) * i_ld];
        o_c_fmmla[block_pos + 2] = i_c_col_major[l_m + 0 + (l_n + 1) * i_ld];
        o_c_fmmla[block_pos + 3] = i_c_col_major[l_m + 1 + (l_n + 1) * i_ld];
    }
  }
}

void convert_c_from_bfmmla( uint64_t         i_m,
                            uint64_t         i_n,
                            uint64_t         i_ld,
                            float    const * i_c_fmmla,
                            float          * o_c_col_major ) {
  for (uint64_t l_n = 0; l_n < i_n; l_n+=2) {
    for (uint64_t l_m = 0; l_m < i_m; l_m+=2) {
        uint64_t block_pos = l_m * 2 + l_n * i_m;
        o_c_col_major[l_m + 0 + (l_n + 0) * i_ld] = i_c_fmmla[block_pos + 0];
        o_c_col_major[l_m + 1 + (l_n + 0) * i_ld] = i_c_fmmla[block_pos + 1];
        o_c_col_major[l_m + 0 + (l_n + 1) * i_ld] = i_c_fmmla[block_pos + 2];
        o_c_col_major[l_m + 1 + (l_n + 1) * i_ld] = i_c_fmmla[block_pos + 3];
    }
  }
}
```

`bfmmla_convert_test.cpp`
```cpp
#include <arm_bf16.h>
#include <arm_neon.h>
#include <iostream>
#include "kernels/convert.h"

int main( int /*i_argc*/, char const ** /*i_argv*/ ) {

  uint64_t m = 16;
  uint64_t n = 16;
  uint64_t k = 8;

  bfloat16_t i_a[m*k];
  bfloat16_t i_b[k*n];
  float      i_c[m*n];

  for (int64_t i = 0; i < m; ++i) {
    for (int64_t j = 0; j < k; ++j) {
      float f = i*k + j;
      i_a[i+j*m] = vcvth_bf16_f32(f);
    }
  }

  for (int64_t i = 0; i < k; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      float f = i*n + j;
      i_b[i+j*k] = vcvth_bf16_f32(f);
    }
  }

  for (int64_t i = 0; i < m; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      i_c[i+j*m] = i*n + j;
    }
  }

  bfloat16_t o_a[16* 8];
  bfloat16_t o_b[ 8*16];
  float      o_c[16*16];

  convert_a_to_bfmmla(m, k, m, i_a, o_a);
  convert_b_to_bfmmla(k, n, k, i_b, o_b);
  convert_c_to_bfmmla(m, n, m, i_c, o_c);

  std::cout << "converted A:" << std::endl;
  for (int64_t i = 0; i < m*k; ++i) {
    std::cout << vcvtah_f32_bf16(o_a[i]) << "\t";
    if (i % 4 == 3)
      std::cout << std::endl;
    if (i % 8 == 7)
      std::cout << std::endl;
    if (i % 64 == 63)
      std::cout << std::endl;
  }

  std::cout << "B:" << std::endl;
  for (int64_t i = 0; i < k; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      std::cout << vcvtah_f32_bf16(i_b[i+j*k]) << "\t";
    }
    std::cout << std::endl;
  }

  std::cout << "converted B:" << std::endl;
  for (int64_t i = 0; i < 8; ++i) {
    for (int64_t j = 0; j < 2; ++j) {
      uint64_t block_offset = j*8 + i*16;
      for (int64_t k = 0; k < 4; ++k) {
        std::cout << vcvtah_f32_bf16(o_b[block_offset+k])
          << "\t" << vcvtah_f32_bf16(o_b[block_offset+k+4]) << std::endl;
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }


  std::cout << "C:" << std::endl;
  for (int64_t i = 0; i < m; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      std::cout << i_c[i+j*m] << "\t";
    }
    std::cout << std::endl;
  }

  std::cout << "converted C:" << std::endl;
  for (int64_t i = 0; i < 8; ++i) {
    for (int64_t j = 0; j < 8; ++j) {
      uint64_t block_offset = j*4 + i*32;
      for (int64_t k = 0; k < 2; ++k) {
        std::cout << o_c[block_offset+k]
          << "\t" << o_c[block_offset+k+2] << std::endl;
      }
      std::cout << std::endl;
    }
    std::cout << std::endl;
  }

  convert_c_from_bfmmla(m, n, m, o_c, i_c);

  std::cout << "back to C:" << std::endl;
  for (int64_t i = 0; i < m; ++i) {
    for (int64_t j = 0; j < n; ++j) {
      std::cout << i_c[i+j*m] << "\t";
    }
    std::cout << std::endl;
  }

  return EXIT_SUCCESS;
}
```

```
$ ./bfmmla_convert_test
converted A:
0       1       2       3
8       9       10      11

16      17      18      19
24      25      26      27

32      33      34      35
40      41      42      43

48      49      50      51
56      57      58      59

64      65      66      67
72      73      74      75

80      81      82      83
88      89      90      91

96      97      98      99
104     105     106     107

112     113     114     115
120     121     122     123


4       5       6       7
12      13      14      15

20      21      22      23
28      29      30      31

36      37      38      39
44      45      46      47

52      53      54      55
60      61      62      63

68      69      70      71
76      77      78      79

84      85      86      87
92      93      94      95

100     101     102     103
108     109     110     111

116     117     118     119
124     125     126     127


B:
0       1       2       3       4       5       6       7       8       9       10      11     12       13      14      15
16      17      18      19      20      21      22      23      24      25      26      27     28       29      30      31
32      33      34      35      36      37      38      39      40      41      42      43     44       45      46      47
48      49      50      51      52      53      54      55      56      57      58      59     60       61      62      63
64      65      66      67      68      69      70      71      72      73      74      75     76       77      78      79
80      81      82      83      84      85      86      87      88      89      90      91     92       93      94      95
96      97      98      99      100     101     102     103     104     105     106     107    108      109     110     111
112     113     114     115     116     117     118     119     120     121     122     123    124      125     126     127
converted B:
0       1
16      17
32      33
48      49

64      65
80      81
96      97
112     113


2       3
18      19
34      35
50      51

66      67
82      83
98      99
114     115


4       5
20      21
36      37
52      53

68      69
84      85
100     101
116     117


6       7
22      23
38      39
54      55

70      71
86      87
102     103
118     119


8       9
24      25
40      41
56      57

72      73
88      89
104     105
120     121


10      11
26      27
42      43
58      59

74      75
90      91
106     107
122     123


12      13
28      29
44      45
60      61

76      77
92      93
108     109
124     125


14      15
30      31
46      47
62      63

78      79
94      95
110     111
126     127


C:
0       1       2       3       4       5       6       7       8       9       10      11     12       13      14      15
16      17      18      19      20      21      22      23      24      25      26      27     28       29      30      31
32      33      34      35      36      37      38      39      40      41      42      43     44       45      46      47
48      49      50      51      52      53      54      55      56      57      58      59     60       61      62      63
64      65      66      67      68      69      70      71      72      73      74      75     76       77      78      79
80      81      82      83      84      85      86      87      88      89      90      91     92       93      94      95
96      97      98      99      100     101     102     103     104     105     106     107    108      109     110     111
112     113     114     115     116     117     118     119     120     121     122     123    124      125     126     127
128     129     130     131     132     133     134     135     136     137     138     139    140      141     142     143
144     145     146     147     148     149     150     151     152     153     154     155    156      157     158     159
160     161     162     163     164     165     166     167     168     169     170     171    172      173     174     175
176     177     178     179     180     181     182     183     184     185     186     187    188      189     190     191
192     193     194     195     196     197     198     199     200     201     202     203    204      205     206     207
208     209     210     211     212     213     214     215     216     217     218     219    220      221     222     223
224     225     226     227     228     229     230     231     232     233     234     235    236      237     238     239
240     241     242     243     244     245     246     247     248     249     250     251    252      253     254     255
converted C:
0       1
16      17

32      33
48      49

64      65
80      81

96      97
112     113

128     129
144     145

160     161
176     177

192     193
208     209

224     225
240     241


2       3
18      19

34      35
50      51

66      67
82      83

98      99
114     115

130     131
146     147

162     163
178     179

194     195
210     211

226     227
242     243


4       5
20      21

36      37
52      53

68      69
84      85

100     101
116     117

132     133
148     149

164     165
180     181

196     197
212     213

228     229
244     245


6       7
22      23

38      39
54      55

70      71
86      87

102     103
118     119

134     135
150     151

166     167
182     183

198     199
214     215

230     231
246     247


8       9
24      25

40      41
56      57

72      73
88      89

104     105
120     121

136     137
152     153

168     169
184     185

200     201
216     217

232     233
248     249


10      11
26      27

42      43
58      59

74      75
90      91

106     107
122     123

138     139
154     155

170     171
186     187

202     203
218     219

234     235
250     251


12      13
28      29

44      45
60      61

76      77
92      93

108     109
124     125

140     141
156     157

172     173
188     189

204     205
220     221

236     237
252     253


14      15
30      31

46      47
62      63

78      79
94      95

110     111
126     127

142     143
158     159

174     175
190     191

206     207
222     223

238     239
254     255


back to C:
0       1       2       3       4       5       6       7       8       9       10      11     12       13      14      15
16      17      18      19      20      21      22      23      24      25      26      27     28       29      30      31
32      33      34      35      36      37      38      39      40      41      42      43     44       45      46      47
48      49      50      51      52      53      54      55      56      57      58      59     60       61      62      63
64      65      66      67      68      69      70      71      72      73      74      75     76       77      78      79
80      81      82      83      84      85      86      87      88      89      90      91     92       93      94      95
96      97      98      99      100     101     102     103     104     105     106     107    108      109     110     111
112     113     114     115     116     117     118     119     120     121     122     123    124      125     126     127
128     129     130     131     132     133     134     135     136     137     138     139    140      141     142     143
144     145     146     147     148     149     150     151     152     153     154     155    156      157     158     159
160     161     162     163     164     165     166     167     168     169     170     171    172      173     174     175
176     177     178     179     180     181     182     183     184     185     186     187    188      189     190     191
192     193     194     195     196     197     198     199     200     201     202     203    204      205     206     207
208     209     210     211     212     213     214     215     216     217     218     219    220      221     222     223
224     225     226     227     228     229     230     231     232     233     234     235    236      237     238     239
240     241     242     243     244     245     246     247     248     249     250     251    252      253     254     255
```

#### 4. and 5.

`kernels/gemm_asm_sve_bfmmla.h`
```cpp
#include <cstdint>
#include <arm_bf16.h>

extern "C" {
  void gemm_asm_sve_bfmmla_16_12_4( bfloat16_t const * i_a,
                                    bfloat16_t const * i_b,
                                    float            * io_c );

  void gemm_asm_sve_bfmmla_16_12_48( bfloat16_t const * i_a,
                                     bfloat16_t const * i_b,
                                     float            * io_c );
}
```

`kernels/gemm_asm_sve_bfmmla_16_12_4.s`
```assembly
        .text
        .type gemm_asm_sve_bfmmla_16_12_4, %function
        .global gemm_asm_sve_bfmmla_16_12_4
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (16x12) = (16x4) * (4x12).
         * The input-data is of type bfloat.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */
gemm_asm_sve_bfmmla_16_12_4:
        // set bits for 8 bit view of predicate register p0 to 1
        ptrue p0.b
        mov x4, #0
        incp x4, p0.b

        // store
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        // Load C
        ld1w { z0.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z1.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z2.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z3.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z4.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z5.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z6.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z7.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z8.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z9.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z10.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z11.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z12.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z13.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z14.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z15.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z16.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z17.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z18.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z19.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z20.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z21.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z22.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z23.s}, p0/z, [x2]
        add x2, x2, x4

        // Load A
        ld1h {z24.h}, p0/z, [x0, #0 , MUL VL]
        ld1h {z25.h}, p0/z, [x0, #1 , MUL VL]
        ld1h {z26.h}, p0/z, [x0, #2 , MUL VL]
        ld1h {z27.h}, p0/z, [x0, #3 , MUL VL]

        // Load B
        ld1rqh {z28.h}, p0/z, [x1, #0]
        ld1rqh {z29.h}, p0/z, [x1, #16]
        ld1rqh {z30.h}, p0/z, [x1, #32]

         bfmmla  z0.s, z28.h, z24.h
         bfmmla  z1.s, z28.h, z25.h
         bfmmla  z2.s, z28.h, z26.h
         bfmmla  z3.s, z28.h, z27.h

         bfmmla  z4.s, z29.h, z24.h
         bfmmla  z5.s, z29.h, z25.h
         bfmmla  z6.s, z29.h, z26.h
         bfmmla  z7.s, z29.h, z27.h

         bfmmla  z8.s, z30.h, z24.h
         bfmmla  z9.s, z30.h, z25.h
         bfmmla z10.s, z30.h, z26.h
         bfmmla z11.s, z30.h, z27.h

         // Load B
         ld1rqh {z28.h}, p0/z, [x1, #48]
         ld1rqh {z29.h}, p0/z, [x1, #64]
         ld1rqh {z30.h}, p0/z, [x1, #80]

         bfmmla z12.s, z28.h, z24.h
         bfmmla z13.s, z28.h, z25.h
         bfmmla z14.s, z28.h, z26.h
         bfmmla z15.s, z28.h, z27.h

         bfmmla z16.s, z29.h, z24.h
         bfmmla z17.s, z29.h, z25.h
         bfmmla z18.s, z29.h, z26.h
         bfmmla z19.s, z29.h, z27.h

         bfmmla z20.s, z30.h, z24.h
         bfmmla z21.s, z30.h, z25.h
         bfmmla z22.s, z30.h, z26.h
         bfmmla z23.s, z30.h, z27.h

        // Store C
        sub x2, x2, x4
        st1w {z23.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z22.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z21.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z20.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z19.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z18.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z17.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z16.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z15.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z14.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z13.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z12.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z11.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z10.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z9.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z8.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z7.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z6.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z5.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z4.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z3.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z2.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z1.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z0.s}, p0, [x2]

        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ret
        .size gemm_asm_sve_bfmmla_16_12_4, (. - gemm_asm_sve_bfmmla_16_12_4)
```

`kernels/gemm_asm_sve_bfmmla_16_12_48.s`
```assembly
        .text
        .type gemm_asm_sve_bfmmla_16_12_48, %function
        .global gemm_asm_sve_bfmmla_16_12_48
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (16x12) = (16x48) * (48x12).
         * The input-data is of type bfloat.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */
gemm_asm_sve_bfmmla_16_12_48:
        // set bits for 8 bit view of predicate register p0 to 1
        ptrue p0.b
        mov x4, #0
        incp x4, p0.b

        // i_k
        mov x5, #48

        // store
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        // Load C
        ld1w { z0.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z1.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z2.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z3.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z4.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z5.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z6.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z7.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z8.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w { z9.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z10.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z11.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z12.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z13.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z14.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z15.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z16.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z17.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z18.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z19.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z20.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z21.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z22.s}, p0/z, [x2]
        add x2, x2, x4
        ld1w {z23.s}, p0/z, [x2]
        add x2, x2, x4

        cbz x5, end
loop_k:
        sub x5, x5, #4

        // Load A
        ld1h {z24.h}, p0/z, [x0]
        add x0, x0, x4
        ld1h {z25.h}, p0/z, [x0]
        add x0, x0, x4
        ld1h {z26.h}, p0/z, [x0]
        add x0, x0, x4
        ld1h {z27.h}, p0/z, [x0]
        add x0, x0, x4

        // Load B
        ld1rqh {z28.h}, p0/z, [x1]
        add x1, x1, #48*2*2
        ld1rqh {z29.h}, p0/z, [x1]
        add x1, x1, #48*2*2
        ld1rqh {z30.h}, p0/z, [x1]
        add x1, x1, #48*2*2

        bfmmla  z0.s, z28.h, z24.h
        bfmmla  z1.s, z28.h, z25.h
        bfmmla  z2.s, z28.h, z26.h
        bfmmla  z3.s, z28.h, z27.h

        bfmmla  z4.s, z29.h, z24.h
        bfmmla  z5.s, z29.h, z25.h
        bfmmla  z6.s, z29.h, z26.h
        bfmmla  z7.s, z29.h, z27.h

        bfmmla  z8.s, z30.h, z24.h
        bfmmla  z9.s, z30.h, z25.h
        bfmmla z10.s, z30.h, z26.h
        bfmmla z11.s, z30.h, z27.h

        // Load B
        ld1rqh {z28.h}, p0/z, [x1]
        add x1, x1, #48*2*2
        ld1rqh {z29.h}, p0/z, [x1]
        add x1, x1, #48*2*2
        ld1rqh {z30.h}, p0/z, [x1]
        sub x1, x1, #5*48*2*2

        incp x1, p0.h // next row B

        bfmmla z12.s, z28.h, z24.h
        bfmmla z13.s, z28.h, z25.h
        bfmmla z14.s, z28.h, z26.h
        bfmmla z15.s, z28.h, z27.h

        bfmmla z16.s, z29.h, z24.h
        bfmmla z17.s, z29.h, z25.h
        bfmmla z18.s, z29.h, z26.h
        bfmmla z19.s, z29.h, z27.h

        bfmmla z20.s, z30.h, z24.h
        bfmmla z21.s, z30.h, z25.h
        bfmmla z22.s, z30.h, z26.h
        bfmmla z23.s, z30.h, z27.h

        cbnz x5, loop_k
end:

        // Store C
        sub x2, x2, x4
        st1w {z23.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z22.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z21.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z20.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z19.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z18.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z17.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z16.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z15.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z14.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z13.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z12.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z11.s}, p0, [x2]
        sub x2, x2, x4
        st1w {z10.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z9.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z8.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z7.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z6.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z5.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z4.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z3.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z2.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z1.s}, p0, [x2]
        sub x2, x2, x4
        st1w { z0.s}, p0, [x2]

        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ret
        .size gemm_asm_sve_bfmmla_16_12_48, (. - gemm_asm_sve_bfmmla_16_12_48)
```

`tests.cpp`
```cpp
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <stdint.h>
#include <iostream>

#include "arm_neon.h"
#include "arm_bf16.h"

#include "kernels/convert.h"
#include "kernels/gemm_ref.cpp"
#include "kernels/gemm_asm_sve_bfmmla.h"

void fill_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda,
    float value
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = value;
    }
  }
}

void fill_matrix(
    bfloat16_t* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda,
    float value
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = vcvth_bf16_f32(value);
    }
  }
}

void fill_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = (m * i_n + n) / 100.0;
    }
  }
}

void fill_matrix(
    bfloat16_t* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = vcvth_bf16_f32((m * i_n + n) / 100.0);
    }
  }
}

void create_matrices(
        bfloat16_t** a, bfloat16_t** b, float** c,
        int64_t m, int64_t n, int64_t k
        ) {
  *a = new bfloat16_t[k * m];
  *b = new bfloat16_t[n * k];
  *c = new float[n * m];

  fill_matrix(*a, m, k, m);
  fill_matrix(*b, k, n, k);
  fill_matrix(*c, m, n, m);
}

void print_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      std::cout << a[m + i_lda * n] << ", ";
    }
    std::cout << std::endl;
    if (m % 32 == 31)
      std::cout << std::endl;
  }
  std::cout << std::endl;
}

void print_matrix(
    bfloat16_t* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      std::cout << vcvtah_f32_bf16(a[m + i_lda * n]) << ", ";
    }
    std::cout << std::endl;
    if (m % 32 == 31)
      std::cout << std::endl;
  }
  std::cout << std::endl;
}


void copy_matrix(
    float* a, float* b,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = b[m + i_lda * n];
    }
  }
}

void compare_matrices(
    float* a, float* b,
    int64_t i_m, int64_t i_n,
    int64_t i_lda, int64_t i_ldb,
    int precision
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      REQUIRE_THAT(a[m + i_lda * n],
                   Catch::Matchers::WithinULP(b[m + i_lda * n], precision));
    }
  }
}

void delete_matrices(bfloat16_t* a, bfloat16_t* b, float* c) {
    delete[] a;
    delete[] b;
    delete[] c;
}

TEST_CASE( "gemm_asm_sve_bfmmla_16_12_4", "[gemm_asm_sve_bfmmla_16_12_4]") {
  int64_t   m = 16; int64_t   n = 12; int64_t   k = 4;
  int64_t i_m = 16; int64_t i_n = 12; int64_t i_k = 4;
  bfloat16_t *a, *b;
  float *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  float* c = new float[m * n];
  copy_matrix(
      c, c_ref,
      m, n,
      m
      );

  bfloat16_t bfmmla_a[m*k];
  bfloat16_t bfmmla_b[k*n];
  float      bfmmla_c[m*n];

  convert_a_to_bfmmla(m, k, m, a, bfmmla_a);
  convert_b_to_bfmmla(k, n, k, b, bfmmla_b);
  convert_c_to_bfmmla(m, n, m, c, bfmmla_c);

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  gemm_asm_sve_bfmmla_16_12_4(bfmmla_a, bfmmla_b, bfmmla_c);
  convert_c_from_bfmmla(m, n, m, bfmmla_c, c);

  compare_matrices(
      c_ref, c,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(a, b, c_ref);
  delete[] c;
}

TEST_CASE( "gemm_asm_sve_bfmmla_16_12_48", "[gemm_asm_sve_bfmmla_16_12_48]") {
  int64_t   m = 16; int64_t   n = 12; int64_t   k = 48;
  int64_t i_m = 16; int64_t i_n = 12; int64_t i_k = 48;
  bfloat16_t *a, *b;
  float *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  float* c = new float[m * n];
  copy_matrix(
      c, c_ref,
      m, n,
      m
      );

  bfloat16_t bfmmla_a[m*k];
  bfloat16_t bfmmla_b[k*n];
  float      bfmmla_c[m*n];

  convert_a_to_bfmmla(m, k, m, a, bfmmla_a);
  convert_b_to_bfmmla(k, n, k, b, bfmmla_b);
  convert_c_to_bfmmla(m, n, m, c, bfmmla_c);

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  gemm_asm_sve_bfmmla_16_12_48(bfmmla_a, bfmmla_b, bfmmla_c);
  convert_c_from_bfmmla(m, n, m, bfmmla_c, c);

  compare_matrices(
      c_ref, c,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(a, b, c_ref);
  delete[] c;
}
```

```
$ ./aarch64_gemm_test
Randomness seeded to: 2885851061
===============================================================================
All tests passed (384 assertions in 2 test cases)
```

`driver.cpp`
```cpp
#include <cstdint>
#include <cmath>
#include <chrono>
#include <omp.h>
#include <iostream>
#include <string>
#include <functional>

#include "arm_bf16.h"
#include "arm_neon.h"

#include "kernels/gemm_ref.cpp"
#include "kernels/gemm_asm_sve_bfmmla.h"

inline void gemm_asm_sve_bfmmla_16_12_4_wrapper(
    bfloat16_t*  const i_a,   bfloat16_t*   const i_b,   float*  io_c,
    [[maybe_unused]] int64_t i_m,   [[maybe_unused]] int64_t i_n,   [[maybe_unused]] int64_t i_k,
    [[maybe_unused]] int64_t i_lda, [[maybe_unused]] int64_t i_ldb, [[maybe_unused]] int64_t i_ldc
    ) {
    gemm_asm_sve_bfmmla_16_12_4(i_a, i_b, io_c);
}

inline void gemm_asm_sve_bfmmla_16_12_48_wrapper(
    bfloat16_t*  const i_a,   bfloat16_t*   const i_b,   float*  io_c,
    [[maybe_unused]] int64_t i_m,   [[maybe_unused]] int64_t i_n,   [[maybe_unused]] int64_t i_k,
    [[maybe_unused]] int64_t i_lda, [[maybe_unused]] int64_t i_ldb, [[maybe_unused]] int64_t i_ldc
    ) {
    gemm_asm_sve_bfmmla_16_12_48(i_a, i_b, io_c);
}


void print_csv_header() {
    std::cout
        << "name"
        << ",m" << ",n" << ",k"
        << ",duration"
        << ",executions"
        << ",GFLOPS"
        << std::endl;
}

void run_benchmark(
    std::string name, std::function<void(
      bfloat16_t*  const, bfloat16_t*   const , float*,  // data
      int64_t           , int64_t             , int64_t, // dimensions
      int64_t           , int64_t             , int64_t  // row lengths
      )> func,
    uint64_t m, uint64_t n, uint64_t k, uint64_t repetitions) {
  bfloat16_t* a = new bfloat16_t[k * m];
  for (uint64_t i = 0; i < k * m; i++) {
    a[i] = vcvth_bf16_f32(0);
  }
  bfloat16_t* b = new bfloat16_t[n * k];
  for (uint64_t i = 0; i < n * k; i++) {
    b[i] = vcvth_bf16_f32(0);
  }
  float* c = new float[n * m];
  for (uint64_t i = 0; i < n * m; i++) {
    c[i] = 0.0;
  }
  std::chrono::steady_clock::time_point l_tp0, l_tp1;
  std::chrono::duration< double > l_dur;
  volatile uint64_t flop_per_iteration = m * n * k * 2; // multiplication + addition;

  // dry-run
  func(
    a, b, c,
    m, n, k,
    m, k, m
  );

  l_tp0 = std::chrono::steady_clock::now();
  for (uint64_t i = 0; i < repetitions; i++) {
      func(
              a, b, c,
              m, n, k,
              m, k, m
          );
  }
  l_tp1 = std::chrono::steady_clock::now();

  l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );

  volatile double l_g_flops = flop_per_iteration * 1.0;
  l_g_flops *= 1.0E-9;
  l_g_flops *= repetitions;
  l_g_flops /= l_dur.count();

  std::cout << name; // name
  std::cout << "," << m << "," << n << "," << k; // dimensions
  std::cout << "," << l_dur.count(); // duration
  std::cout << "," << repetitions; // executions
  std::cout << "," << l_g_flops; // GFLOPS
  std::cout << std::endl;

  delete[] a; delete[] b; delete[] c;
}

int main() {
  print_csv_header();
  // run_benchmark("gemm_ref", &gemm_ref,  16,  6,  1, 10000000);
  run_benchmark(
      "gemm_asm_sve_bfmmla_16_12_4", &gemm_asm_sve_bfmmla_16_12_4_wrapper,
      16, 12, 4,
      50000000);
  run_benchmark(
      "gemm_asm_sve_bfmmla_16_12_48", &gemm_asm_sve_bfmmla_16_12_48_wrapper,
      16, 12, 48,
      50000000);

  return EXIT_SUCCESS;
}
```

```
$ ./aarch64_gemm_micro
name,m,n,k,duration,executions,GFLOPS
gemm_asm_sve_bfmmla_16_12_4,16,12,4,1.46416,50000000,52.4531
gemm_asm_sve_bfmmla_16_12_48,16,12,48,4.00364,50000000,230.19
```

| Team         | program                      | Time (s) | #execution | GFLOPS  | %peak |
|--------------|------------------------------|----------|------------|---------|-------|
| peak climber | gemm_asm_sve_bfmmla_16_12_4  | 1.46416  | 50000000   | 52.4531 | 0.16% |
| peak climber | gemm_asm_sve_bfmmla_16_12_48 | 4.00364  | 50000000   | 230.19  | 0.69% |


### 9.2 Column-Major B and C
