        .text
        .align 4
        .type sve_example_0, %function
        .global sve_example_0
sve_example_0:
        ldr z0, [x0]
        str z0, [x1]
        ret
        .size sve_example_0, (. - sve_example_0)

        .type sve_example_1, %function
        .global sve_example_1
  sve_example_1:
        // only the last bit is considered
        // we have 32 bit vector elements
        // all following options deliver the same result
        // and set all elements in the SVE vector
        //ptrue p0.b // sets all predicate bits to zero
        //ptrue p0.h // only set every other bit
        ptrue p0.s // only set every fourth bit
        ld1w {z0.s}, p0/z, [x0]
        str z0, [x1]
        ret
        .size sve_example_1, (. - sve_example_1)

        .type sve_example_2, %function
        .global sve_example_2
sve_example_2:
        //ptrue p0.d // set every eighths bit
        ptrue p0.s, VL8 // set bits 0, 4, 8, 12, ..., 28
        ld1w {z0.s}, p0/z, [x0]
        str z0, [x1]
        ret
        .size sve_example_2, (. - sve_example_2)

        .type sve_example_3, %function
        .global sve_example_3
sve_example_3:
        // start trace
        .inst 0x2520e020

        // set bits in packs of three if they fit in VL
        // here: {0, 8, 16}, {24, 32, 40}, {48, 56, 64}
        //       |  VL128 |
        //       |            VL256 |
        //       |                      VL384 |
        //       |                              VL512 |
        ptrue p0.d, MUL3
        ld1w {z0.s}, p0/z, [x0]
        str z0, [x1]

        // end trace
        .inst 0x2520e040

        ret
        .size sve_example_3, (. - sve_example_3)
