#include <stdint.h>

extern "C" {
  void sve_example_0(uint32_t* in, uint32_t* out);

  void sve_example_1(uint32_t* in, uint32_t* out);

  void sve_example_2(uint32_t* in, uint32_t* out);

  void sve_example_3(uint32_t* in, uint32_t* out);
}
