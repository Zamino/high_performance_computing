# 07

## 8.1 Arm Instruction Emulator

### 2
```
$ curl --output armie.tar.gz https://armkeil.blob.core.windows.net/developer/Files/downloads/hpc/arm-instruction-emulator/22-0/ARM-Instruction-Emulator_22.0_AArch64_RHEL_8.tar.gz
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  124M  100  124M    0     0  12.8M      0  0:00:09  0:00:09 --:--:-- 16.3M
$ tar -xf armie.tar.gz
$ ls
armie.tar.gz  arm-instruction-emulator_22.0_RHEL-8  git_hashes.json ...
$ cat git_hashes.json
{
    "components": {
        "arm-instruction-emulator": {
            "sources": {},
            "version": "22.0.0.22"
        },
        "gccrt": {
            "sources": {},
            "version": "11.2.0.193"
        }
    }
}
$ ./arm-instruction-emulator_22.0_RHEL-8/arm-instruction-emulator_22.0_RHEL-8.sh --install-to ${HOME}/armie
...
Accept? Type "yes" or "no": yes
Unpacking...
Installing...Verifying packages...
warning: Unable to get systemd shutdown inhibition lock: Permission denied
Preparing packages...
gcc-11.2.0-runtimes-Generic-AArch64-RHEL-8-aarch64-linux-11.2.0-193.aarch64
arm-instruction-emulator-22.0-Generic-AArch64-RHEL-8-aarch64-linux-22.0-22.aarch64
The installed packages contain modulefiles under /home/tamino/armie/modulefiles
You can add these to your environment by running:
                $ module use /home/tamino/armie/modulefiles
Alternatively:  $ export MODULEPATH=$MODULEPATH:/home/tamino/armie/modulefiles
$ export MODULEPATH=$MODULEPATH:${HOME}/armie/modulefiles
$ module load armie22/22.0
$ module unload armie22/22.0
$ cd ./aarch64_sve_examples
$ mkdir build && cd build
$ cmake -DCMAKE_BUILD_TYPE=Debug .. && cmake --build .
...
$ module load armie22/22.0
$ armie -msve-vector-bits=128 -- sve_examples 2
en / in / out: 0        /        2      /        0
...
$ armie -msve-vector-bits=256 -- sve_examples 2
en / in / out: 0        /        2      /        2
en / in / out: 1        /        4      /        4
en / in / out: 2        /        6      /        6
en / in / out: 3        /        8      /        8
en / in / out: 4        /        10     /        10
en / in / out: 5        /        12     /        12
en / in / out: 6        /        14     /        14
en / in / out: 7        /        16     /        16
en / in / out: 8        /        18     /        0
...
$ armie -msve-vector-bits=512 -- sve_examples 2
en / in / out: 0        /        2      /        2
en / in / out: 1        /        4      /        4
en / in / out: 2        /        6      /        6
en / in / out: 3        /        8      /        8
en / in / out: 4        /        10     /        10
en / in / out: 5        /        12     /        12
en / in / out: 6        /        14     /        14
en / in / out: 7        /        16     /        16
en / in / out: 8        /        18     /        0
...
$ armie -msve-vector-bits=128 -- sve_examples 3
en / in / out: 0        /        2      /        0
...
$ armie -msve-vector-bits=256 -- sve_examples 3
en / in / out: 0        /        2      /        2
en / in / out: 1        /        4      /        0
en / in / out: 2        /        6      /        6
en / in / out: 3        /        8      /        0
en / in / out: 4        /        10     /        10
...
$ armie -msve-vector-bits=512 -- sve_examples 3
en / in / out: 0        /        2      /        2
en / in / out: 1        /        4      /        0
en / in / out: 2        /        6      /        6
en / in / out: 3        /        8      /        0
en / in / out: 4        /        10     /        10
en / in / out: 5        /        12     /        0
en / in / out: 6        /        14     /        14
en / in / out: 7        /        16     /        0
en / in / out: 8        /        18     /        18
en / in / out: 9        /        20     /        0
en / in / out: 10       /        22     /        22
...
```

### 3

```
$ armie -msve-vector-bits=512 -i libinscount_emulated.so -- sve_examples 2
Client inscount is running
...
1672292 instructions executed of which 3 were emulated instructions

$ armie -msve-vector-bits=512 -i libinscount_emulated.so -- sve_examples 3
Client inscount is running
...
1672298 instructions executed of which 3 were emulated instructions
```

### 4


```
         .type sve_example_3, %function
         .global sve_example_3
 sve_example_3:
+        // start trace
+        .inst 0x2520e020
+
         // set bits in packs of three if they fit in VL
         // here: {0, 8, 16}, {24, 32, 40}, {48, 56, 64}
         //       |  VL128 |
         //       |            VL256 |
         //       |                      VL384 |
         //       |                              VL512 |
         ptrue p0.d, MUL3
         ld1w {z0.s}, p0/z, [x0]
         str z0, [x1]
 
+        // end trace
+        .inst 0x2520e040
+
         ret
         .size sve_example_3, (. - sve_example_3)
```

```
$ armie -e libmemtrace_sve_512.so -i libmemtrace_emulated.so -a -roi sve_examples 3
Data file /home/tamino/hpc/07/aarch64_sve_examples/build/memtrace.sve_examples.915612.0000.log created
en / in / out: 0        /        2      /        2
en / in / out: 1        /        4      /        0
en / in / out: 2        /        6      /        6
en / in / out: 3        /        8      /        0
en / in / out: 4        /        10     /        10
en / in / out: 5        /        12     /        0
en / in / out: 6        /        14     /        14
en / in / out: 7        /        16     /        0
en / in / out: 8        /        18     /        18
en / in / out: 9        /        20     /        0
en / in / out: 10       /        22     /        22
en / in / out: 11       /        24     /        0
...
$ cat memtrace.sve_examples.915612.0000.log
Format: <sequence number>: <TID>, <isBundle>, <isWrite>, <data size>, <data address>, <PC>
$ cat sve-memtrace.sve_examples.915612.log
0, -1, 0, 1, 0, (nil), (nil)
1, 674551936, 0, 0, 4, 0xffffd90b22f8, 0x400d34
2, 674551936, 0, 0, 4, 0xffffd90b2300, 0x400d34
3, 674551936, 0, 0, 4, 0xffffd90b2308, 0x400d34
4, 674551936, 0, 0, 4, 0xffffd90b2310, 0x400d34
5, 674551936, 0, 0, 4, 0xffffd90b2318, 0x400d34
6, 674551936, 0, 0, 4, 0xffffd90b2320, 0x400d34
7, -2, 0, 1, 0, (nil), (nil)
```

## 8.2

Files are in `./aarch64_asm_sve/`

### 1

`kernels/triad_low.s`
```cpp
        .text
        .type triad_low, %function
        .global triad_low
        /*
         * @param x0 count of elements.
         * @param x1 pointer to A.
         * @param x2 pointer to B.
         * @param x3 pointer to C.
         */
triad_low:
        cbz x0, end


        // store
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        mov x6, #4

        mov x4, #0
        whilelt p0.s, x4, x0
loop:

        fmov z0.s, p0/m, #2

        ld1w {z2.s}, p0/z, [x2]
        ld1w {z1.s}, p0/z, [x1]

        fmla z1.s, p0/m, z2.s, z0.s

        st1w {z1.s}, p0, [x3]

        // increment pointer
        mov x5, #0
        incp x5, p0.s
        madd x3, x5, x6, x3
        madd x2, x5, x6, x2
        madd x1, x5, x6, x1

        add x4, x4, x5

        whilelt p0.s, x4, x0
        b.any loop
end:
        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ret
        .size triad_low, (. - triad_low)
```

### 2

`tests.cpp`
```cpp
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <stdint.h>
#include <iostream>

#include "kernels/triad_high.h"
#include "kernels/triad_low.h"

void create_vectors(
        float** a, float** b, float** c,
        int64_t m
        ) {
  *a = new float[m];
  *b = new float[m];
  *c = new float[m];
}

void fill_vector(float* a, int64_t i_m) {
  for (int64_t m = 0; m < i_m; m++) {
    a[m] = m / 100.0;
  }
}

void print_vector(float* a, int64_t i_m) {
  for (int64_t m = 0; m < i_m; m++) {
    std::cout << a[m] << std::endl;
  }
  std::cout << std::endl;
}

void compare_vector( float* a, float* b, int64_t i_m, int precision) {
  for (int64_t m = 0; m < i_m; m++) {
    REQUIRE_THAT(a[m], Catch::Matchers::WithinULP(b[m], precision));
  }
}

void delete_vectors(float* a, float* b, float* c) {
    delete[] a;
    delete[] b;
    delete[] c;
}

TEST_CASE( "triad_low 32", "[triad_low]" ) {
  int64_t m = 32;
  float *a, *b, *c;
  create_vectors(&a, &b, &c, m);
  fill_vector(a, m); fill_vector(b, m); fill_vector(c, m);

  triad_high( m, a, b, c);

  float* c_result = new float[m];
  fill_vector(c_result, m);

  triad_low( m, a, b, c_result);

  // print_vector(c, m);
  // print_vector(c_result, m);
  compare_vector(c, c_result, m, 3);

  delete_vectors(a, b, c);
  delete[] c_result;
}

TEST_CASE( "triad_low 42", "[triad_low]" ) {
  int64_t m = 42;
  float *a, *b, *c;
  create_vectors(&a, &b, &c, m);
  fill_vector(a, m); fill_vector(b, m); fill_vector(c, m);

  triad_high( m, a, b, c);

  float* c_result = new float[m];
  fill_vector(c_result, m);

  triad_low( m, a, b, c_result);

  // print_vector(c, m);
  // print_vector(c_result, m);
  compare_vector(c, c_result, m, 3);

  delete_vectors(a, b, c);
  delete[] c_result;
}

TEST_CASE( "triad_low 9", "[triad_low]" ) {
  int64_t m = 9;
  float *a, *b, *c;
  create_vectors(&a, &b, &c, m);
  fill_vector(a, m); fill_vector(b, m); fill_vector(c, m);

  triad_high( m, a, b, c);

  float* c_result = new float[m];
  fill_vector(c_result, m);

  triad_low( m, a, b, c_result);

  // print_vector(c, m);
  // print_vector(c_result, m);
  compare_vector(c, c_result, m, 3);

  delete_vectors(a, b, c);
  delete[] c_result;
}
```
