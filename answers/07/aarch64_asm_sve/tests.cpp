#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <stdint.h>
#include <iostream>

#include "kernels/triad_high.h"
#include "kernels/triad_low.h"

void create_vectors(
        float** a, float** b, float** c,
        int64_t m
        ) {
  *a = new float[m];
  *b = new float[m];
  *c = new float[m];
}

void fill_vector(float* a, int64_t i_m) {
  for (int64_t m = 0; m < i_m; m++) {
    a[m] = m / 100.0;
  }
}

void print_vector(float* a, int64_t i_m) {
  for (int64_t m = 0; m < i_m; m++) {
    std::cout << a[m] << std::endl;
  }
  std::cout << std::endl;
}

void compare_vector( float* a, float* b, int64_t i_m, int precision) {
  for (int64_t m = 0; m < i_m; m++) {
    REQUIRE_THAT(a[m], Catch::Matchers::WithinULP(b[m], precision));
  }
}

void delete_vectors(float* a, float* b, float* c) {
    delete[] a;
    delete[] b;
    delete[] c;
}

TEST_CASE( "triad_low 32", "[triad_low]" ) {
  int64_t m = 32;
  float *a, *b, *c;
  create_vectors(&a, &b, &c, m);
  fill_vector(a, m); fill_vector(b, m); fill_vector(c, m);

  triad_high( m, a, b, c);

  float* c_result = new float[m];
  fill_vector(c_result, m);

  triad_low( m, a, b, c_result);

  // print_vector(c, m);
  // print_vector(c_result, m);
  compare_vector(c, c_result, m, 3);

  delete_vectors(a, b, c);
  delete[] c_result;
}

TEST_CASE( "triad_low 42", "[triad_low]" ) {
  int64_t m = 42;
  float *a, *b, *c;
  create_vectors(&a, &b, &c, m);
  fill_vector(a, m); fill_vector(b, m); fill_vector(c, m);

  triad_high( m, a, b, c);

  float* c_result = new float[m];
  fill_vector(c_result, m);

  triad_low( m, a, b, c_result);

  // print_vector(c, m);
  // print_vector(c_result, m);
  compare_vector(c, c_result, m, 3);

  delete_vectors(a, b, c);
  delete[] c_result;
}

TEST_CASE( "triad_low 9", "[triad_low]" ) {
  int64_t m = 9;
  float *a, *b, *c;
  create_vectors(&a, &b, &c, m);
  fill_vector(a, m); fill_vector(b, m); fill_vector(c, m);

  triad_high( m, a, b, c);

  float* c_result = new float[m];
  fill_vector(c_result, m);

  triad_low( m, a, b, c_result);

  // print_vector(c, m);
  // print_vector(c_result, m);
  compare_vector(c, c_result, m, 3);

  delete_vectors(a, b, c);
  delete[] c_result;
}
