        .text
        .type triad_low, %function
        .global triad_low
        /*
         * @param x0 count of elements.
         * @param x1 pointer to A.
         * @param x2 pointer to B.
         * @param x3 pointer to C.
         */
triad_low:
        cbz x0, end


        // store
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        mov x6, #4

        mov x4, #0
        whilelt p0.s, x4, x0
loop:

        fmov z0.s, p0/m, #2

        ld1w {z2.s}, p0/z, [x2]
        ld1w {z1.s}, p0/z, [x1]

        fmla z1.s, p0/m, z2.s, z0.s

        st1w {z1.s}, p0, [x3]

        // increment pointer
        mov x5, #0
        incp x5, p0.s
        madd x3, x5, x6, x3
        madd x2, x5, x6, x2
        madd x1, x5, x6, x1

        add x4, x4, x5

        whilelt p0.s, x4, x0
        b.any loop
end:
        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ret
        .size triad_low, (. - triad_low)
