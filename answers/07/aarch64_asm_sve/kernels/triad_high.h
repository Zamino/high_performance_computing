#include <cstdint>

void triad_high( uint64_t         i_n_values,
                 float    const * i_a,
                 float    const * i_b,
                 float          * o_c );
