#include <cstdint>

extern "C" {
  void triad_low( uint64_t         i_n_values,
                  float    const * i_a,
                  float    const * i_b,
                  float          * o_c );
}
