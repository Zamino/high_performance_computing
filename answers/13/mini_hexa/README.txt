mini_hexa
=========
This is the Hexagon example code of the High Performance Computing class:
https://scalable.uni-jena.de/opt/hpc
For the time being, the code only runs as root user.

The file build_host.sh builds the host code (runs on the CPU).
The file build_device.sh builds the device code (runs on the DSP).
Adjust the HEXAGON_SDK variable according to your environment.

Buid and Run Instructions
-------------------------
bash build_host.sh
bash build_device.sh

export ANDROID_SERIAL=3000a4df
adb shell "mkdir -p /data/local/tmp/tamino/mini_hexa"
adb push build/host/mini_hexa build/host/libmini_hexa.so /data/local/tmp/tamino/mini_hexa
adb push build/device/libmini_hexa_skel.so /data/local/tmp/tamino/mini_hexa

adb shell "echo 0x1f > /data/local/tmp/tamino/mini_hexa/mini_hexa.farf"

adb root
adb shell "DSP_LIBRARY_PATH=/data/local/tmp/tamino/mini_hexa LD_LIBRARY_PATH=/data/local/tmp/tamino/mini_hexa /data/local/tmp/tamino/mini_hexa/mini_hexa"
