export BUILD_DIR=build/host
export HEXAGON_SDK=/opt/HexagonSDK/5.3.0.0/
export ANDROID_NDK=${HEXAGON_SDK}/tools/android-ndk-r19c/
export QAIC=${HEXAGON_SDK}/ipc/fastrpc/qaic/bin/qaic

export ANDROID_CLANG_C=${ANDROID_NDK}/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android26-clang
export ANDROID_CLANG_CXX=${ANDROID_NDK}/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android26-clang++
export ANDROID_C_FLAGS="-target aarch64-linux-android26 -c -fpie -fpic -fPIE -fPIC -Wall -Wno-missing-braces -fpic -g"

mkdir -p ${BUILD_DIR}

${QAIC} -mdll -o ${BUILD_DIR} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef -I${HEXAGON_SDK}/ipc/fastrpc/incs inc/mini_hexa.idl

${ANDROID_CLANG_C} ${ANDROID_C_FLAGS} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef -o ${BUILD_DIR}/mini_hexa_stub.o ${BUILD_DIR}/mini_hexa_stub.c

${ANDROID_CLANG_C} -fpie -fpic -fPIE -fPIC -Wl,-dynamic-linker,/system/bin/linker64 -Wl,-z,nocopyreloc -Wl,-unresolved-symbols=ignore-in-shared-libs -Wl,-rpath-link=${ANDROID_NDK}/platforms/android-26/arch-arm64/usr/lib -Wl,-soname=libmini_hexa.so -nostartfiles -nostdlib -shared -Bsymbolc -o ${BUILD_DIR}/libmini_hexa.so -Wl,--start-group ${BUILD_DIR}/mini_hexa_stub.o -L${HEXAGON_SDK}/ipc/fastrpc/remote/ship/android_aarch64/ -ladsprpc -Wl,--end-group -L${ANDROID_NDK}/platforms/android-26/arch-arm64/usr/lib -lm -lc -ldl -lgcc

${ANDROID_CLANG_C} ${ANDROID_C_FLAGS} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef -I${HEXAGON_SDK}/utils/examples -I${HEXAGON_SDK}/libs/atomic/inc -I${HEXAGON_SDK}/ipc/fastrpc/rpcmem/inc -I${BUILD_DIR} -o ${BUILD_DIR}/mini_hexa_host.o src/mini_hexa_host.c

${ANDROID_CLANG_C} ${ANDROID_C_FLAGS} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef -o ${BUILD_DIR}/dsp_capabilities_utils.o ${HEXAGON_SDK}/utils/examples/dsp_capabilities_utils.c

${ANDROID_CLANG_C} ${ANDROID_C_FLAGS} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef -o ${BUILD_DIR}/pd_status_notification.o ${HEXAGON_SDK}/utils/examples/pd_status_notification.c

${ANDROID_CLANG_CXX} -fpie -fpic -fPIE -fPIC -fPIE -Wl,-dynamic-linker,/system/bin/linker64 -Wl,-z,nocopyreloc -Wl,-unresolved-symbols=ignore-in-shared-libs -Wl,-rpath-link=${ANDROID_NDK}/platforms/android-26/arch-arm64/usr/lib -pie -nostdlib -Bdynamic ${ANDROID_NDK}/platforms/android-26/arch-arm64/usr/lib/crtbegin_dynamic.o ${ANDROID_NDK}/platforms/android-26/arch-arm64/usr/lib/crtend_android.o -Wl,-llog -Wl,-ldl -o ${BUILD_DIR}/mini_hexa -Wl,--start-group ${BUILD_DIR}/mini_hexa_host.o ${BUILD_DIR}/dsp_capabilities_utils.o ${BUILD_DIR}/pd_status_notification.o  ${HEXAGON_SDK}/ipc/fastrpc/rpcmem/prebuilt/android_aarch64/rpcmem.a -L${BUILD_DIR} -lmini_hexa -L${HEXAGON_SDK}/ipc/fastrpc/remote/ship/android_aarch64/ -ladsprpc -Wl,--end-group -L${ANDROID_NDK}/platforms/android-26/arch-arm64/usr/lib -lm -lc -ldl -lgcc
