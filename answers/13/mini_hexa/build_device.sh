export BUILD_DIR=build/device
export HEXAGON_SDK=/opt/HexagonSDK/5.3.0.0/
export HEXAGON_TOOLS=${HEXAGON_SDK}/tools/HEXAGON_Tools/8.6.06/
export QAIC=${HEXAGON_SDK}/ipc/fastrpc/qaic/bin/qaic
export HEXAGON_CLANG_C=${HEXAGON_TOOLS}/Tools/bin/hexagon-clang
export HEXAGON_CLANG_CXX=${HEXAGON_TOOLS}/Tools/bin/hexagon-clang++
export HEXAGON_AR=${HEXAGON_TOOLS}/Tools/bin/hexagon-ar
export HEXAGON_C_FLAGS="-mv69 -c -G0 -g -O2 -Wall -Wstrict-prototypes -fno-strict-aliasing -fno-zero-initialized-in-bss -fdata-sections -fstack-protector -fpic -D__V_DYNAMIC__ -mhvx -mhvx-length=128B -D_DEBUG"
export HEXAGON_ASM_FLAGS="-mv69 -c -G0 -g -O2 -Wall -Werror -Wstrict-prototypes -fpic -mhvx -mhvx-length=128B"

mkdir -p ${BUILD_DIR}/kernels

export QURT_C_FLAGS="-I${HEXAGON_SDK}/rtos/qurt/computev69/include -I${HEXAGON_SDK}/rtos/qurt/computev69/include/qurt"

${QAIC} -mdll -o ${BUILD_DIR} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef -I${HEXAGON_SDK}/ipc/fastrpc/incs inc/mini_hexa.idl

${HEXAGON_CLANG_C} ${HEXAGON_C_FLAGS} ${QURT_C_FLAGS} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef ${QURT_C_FLAGS} -o ${BUILD_DIR}/mini_hexa_skel.o ${BUILD_DIR}/mini_hexa_skel.c

${HEXAGON_CLANG_C} ${HEXAGON_ASM_FLAGS} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef -o ${BUILD_DIR}/kernels/my_asm.o src/kernels/my_asm.s
${HEXAGON_CLANG_C} ${HEXAGON_ASM_FLAGS} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef -o ${BUILD_DIR}/kernels/gemm_ref.o src/kernels/gemm_ref.c

${HEXAGON_CLANG_C} ${HEXAGON_C_FLAGS} -I${HEXAGON_SDK}/incs -I${HEXAGON_SDK}/incs/stddef ${QURT_C_FLAGS} -I${BUILD_DIR} -o ${BUILD_DIR}/mini_hexa_device.o src/mini_hexa_device.c

${HEXAGON_CLANG_C} -mv69 -G0 -Wl,--defsym=ISDB_TRUSTED_FLAG=2 -Wl,--defsym=ISDB_SECURE_FLAG=2 -Wl,--no-threads -fpic -shared -Wl,-Bsymbolic -Wl,--wrap=malloc -Wl,--wrap=calloc -Wl,--wrap=free -Wl,--wrap=realloc -Wl,--wrap=memalign -lc -Wl,-Map=${BUILD_DIR}/libmini_hexa_skel.so.map -Wl,-soname=libmini_hexa_skel.so -o ${BUILD_DIR}/libmini_hexa_skel.so -Wl,--start-group ${BUILD_DIR}/mini_hexa_skel.o ${BUILD_DIR}/mini_hexa_device.o ${BUILD_DIR}/kernels/my_asm.o ${BUILD_DIR}/kernels/gemm_ref.o -Wl,--end-group
