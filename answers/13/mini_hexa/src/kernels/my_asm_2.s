    .text
    .p2align 2
    .p2align 4,,15
    .globl micro_hvx_qf32
    .type micro_hvx_qf32, @function
micro_hvx_qf32:

    loop0( loop_micro_hvx_qf32, r0 )
loop_micro_hvx_qf32:
    { v28.qf32 = vadd( v30.qf32, v31.qf32 ); v29.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v26.qf32 = vadd( v30.qf32, v31.qf32 ); v27.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v24.qf32 = vadd( v30.qf32, v31.qf32 ); v25.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v22.qf32 = vadd( v30.qf32, v31.qf32 ); v23.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v20.qf32 = vadd( v30.qf32, v31.qf32 ); v21.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v18.qf32 = vadd( v30.qf32, v31.qf32 ); v19.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v16.qf32 = vadd( v30.qf32, v31.qf32 ); v17.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v14.qf32 = vadd( v30.qf32, v31.qf32 ); v15.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v12.qf32 = vadd( v30.qf32, v31.qf32 ); v13.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v10.qf32 = vadd( v30.qf32, v31.qf32 ); v11.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v8.qf32  = vadd( v30.qf32, v31.qf32 ); v9.qf32  = vmpy( v30.qf32, v31.qf32 ) }
    { v6.qf32  = vadd( v30.qf32, v31.qf32 ); v7.qf32  = vmpy( v30.qf32, v31.qf32 ) }
    { v4.qf32  = vadd( v30.qf32, v31.qf32 ); v5.qf32  = vmpy( v30.qf32, v31.qf32 ) }
    { v2.qf32  = vadd( v30.qf32, v31.qf32 ); v3.qf32  = vmpy( v30.qf32, v31.qf32 ) }
    { v0.qf32  = vadd( v30.qf32, v31.qf32 ); v1.qf32  = vmpy( v30.qf32, v31.qf32 ) }: endloop0

    { r0 = #32 * 28 }
    { jumpr r31 } // return
    .size	micro_hvx_qf32, .-micro_hvx_qf32


    .p2align 2
    .p2align 4,,15
    .globl gemm_asm_cdsp_192_4_1
    .type gemm_asm_cdsp_192_4_1, @function
gemm_asm_cdsp_192_4_1:

// Load c
    { v0  = vmemu( r2 ++ #1) }
    { v1  = vmemu( r2 ++ #1) }
    { v2  = vmemu( r2 ++ #1) }
    { v3  = vmemu( r2 ++ #1) }
    { v4  = vmemu( r2 ++ #1) }
    { v5  = vmemu( r2 ++ #1) }
    { v6  = vmemu( r2 ++ #1) }
    { v7  = vmemu( r2 ++ #1) }
    { v8  = vmemu( r2 ++ #1) }
    { v9  = vmemu( r2 ++ #1) }
    { v10 = vmemu( r2 ++ #1) }
    { v11 = vmemu( r2 ++ #1) }
    { v12 = vmemu( r2 ++ #1) }
    { v13 = vmemu( r2 ++ #1) }
    { v14 = vmemu( r2 ++ #1) }
    { v15 = vmemu( r2 ++ #1) }
    { v16 = vmemu( r2 ++ #1) }
    { v17 = vmemu( r2 ++ #1) }
    { v18 = vmemu( r2 ++ #1) }
    { v19 = vmemu( r2 ++ #1) }
    { v20 = vmemu( r2 ++ #1) }
    { v21 = vmemu( r2 ++ #1) }
    { v22 = vmemu( r2 ++ #1) }
    { v23 = vmemu( r2 ) }

// Load a
    { v24 = vmemu( r0 ++ #1) }
    { v25 = vmemu( r0 ++ #1) }
    { v26 = vmemu( r0 ++ #1) }
    { v27 = vmemu( r0 ++ #1) }
    { v28 = vmemu( r0 ++ #1) }
    { v29 = vmemu( r0 ++ #1) }

// Load b 0
    r4 = memw(r1)
    v30 = vsplat(r4)

    v31.qf32 = vmpy( v24.sf  v30.sf )
    v0.qf32  = vadd( v31.qf32 v0.sf ) 
    v31.qf32 = vmpy( v25.sf  v30.sf )
    v1.qf32  = vadd( v31.qf32 v1.sf ) 
    v31.qf32 = vmpy( v26.sf  v30.sf )
    v2.qf32  = vadd( v31.qf32 v2.sf ) 
    v31.qf32 = vmpy( v27.sf  v30.sf )
    v3.qf32  = vadd( v31.qf32 v3.sf ) 
    v31.qf32 = vmpy( v28.sf  v30.sf )
    v4.qf32  = vadd( v31.qf32 v4.sf ) 
    v31.qf32 = vmpy( v29.sf  v30.sf )
    v5.qf32  = vadd( v31.qf32 v5.sf ) 

// Load b 1
    r4 = memw(r1 + #4)
    v30 = vsplat(r4)

    v31.qf32 = vmpy( v24.sf  v30.sf )
    v6.qf32  = vadd( v31.qf32 v6.sf ) 
    v31.qf32 = vmpy( v25.sf  v30.sf )
    v7.qf32  = vadd( v31.qf32 v7.sf ) 
    v31.qf32 = vmpy( v26.sf  v30.sf )
    v8.qf32  = vadd( v31.qf32 v8.sf ) 
    v31.qf32 = vmpy( v27.sf  v30.sf )
    v9.qf32  = vadd( v31.qf32 v9.sf ) 
    v31.qf32 = vmpy( v28.sf  v30.sf )
    v10.qf32  = vadd( v31.qf32 v10.sf ) 
    v31.qf32 = vmpy( v29.sf  v30.sf )
    v11.qf32  = vadd( v31.qf32 v11.sf ) 

// Load b 2
    r4 = memw(r1 + #8)
    v30 = vsplat(r4)

    v31.qf32 = vmpy( v24.sf  v30.sf )
    v12.qf32  = vadd( v31.qf32 v12.sf ) 
    v31.qf32 = vmpy( v25.sf  v30.sf )
    v13.qf32  = vadd( v31.qf32 v13.sf ) 
    v31.qf32 = vmpy( v26.sf  v30.sf )
    v14.qf32  = vadd( v31.qf32 v14.sf ) 
    v31.qf32 = vmpy( v27.sf  v30.sf )
    v15.qf32  = vadd( v31.qf32 v15.sf ) 
    v31.qf32 = vmpy( v28.sf  v30.sf )
    v16.qf32  = vadd( v31.qf32 v16.sf ) 
    v31.qf32 = vmpy( v29.sf  v30.sf )
    v17.qf32  = vadd( v31.qf32 v17.sf ) 

// Load b 3
    r4 = memw(r1 + #12)
    v30 = vsplat(r4)

    v31.qf32 = vmpy( v24.sf  v30.sf )
    v18.qf32  = vadd( v31.qf32 v18.sf ) 
    v31.qf32 = vmpy( v25.sf  v30.sf )
    v19.qf32  = vadd( v31.qf32 v19.sf ) 
    v31.qf32 = vmpy( v26.sf  v30.sf )
    v20.qf32  = vadd( v31.qf32 v20.sf ) 
    v31.qf32 = vmpy( v27.sf  v30.sf )
    v21.qf32  = vadd( v31.qf32 v21.sf ) 
    v31.qf32 = vmpy( v28.sf  v30.sf )
    v22.qf32  = vadd( v31.qf32 v22.sf ) 
    v31.qf32 = vmpy( v29.sf  v30.sf )
    v23.qf32  = vadd( v31.qf32 v23.sf ) 

// Store c
                             { v23.sf=v23.qf32 }
    { vmemu( r2 ++ #-1) = v23; v22.sf=v22.qf32 }
    { vmemu( r2 ++ #-1) = v22; v21.sf=v21.qf32 }
    { vmemu( r2 ++ #-1) = v21; v20.sf=v20.qf32 }
    { vmemu( r2 ++ #-1) = v20; v19.sf=v19.qf32 }
    { vmemu( r2 ++ #-1) = v19; v18.sf=v18.qf32 }
    { vmemu( r2 ++ #-1) = v18; v17.sf=v17.qf32 }
    { vmemu( r2 ++ #-1) = v17; v16.sf=v16.qf32 }
    { vmemu( r2 ++ #-1) = v16; v15.sf=v15.qf32 }
    { vmemu( r2 ++ #-1) = v15; v14.sf=v14.qf32 }
    { vmemu( r2 ++ #-1) = v14; v13.sf=v13.qf32 }
    { vmemu( r2 ++ #-1) = v13; v12.sf=v12.qf32 }
    { vmemu( r2 ++ #-1) = v12; v11.sf=v11.qf32 }
    { vmemu( r2 ++ #-1) = v11; v10.sf=v10.qf32 }
    { vmemu( r2 ++ #-1) = v10; v9.sf =v9.qf32  }
    { vmemu( r2 ++ #-1) = v9 ; v8.sf =v8.qf32  }
    { vmemu( r2 ++ #-1) = v8 ; v7.sf =v7.qf32  }
    { vmemu( r2 ++ #-1) = v7 ; v6.sf =v6.qf32  }
    { vmemu( r2 ++ #-1) = v6 ; v5.sf =v5.qf32  }
    { vmemu( r2 ++ #-1) = v5 ; v4.sf =v4.qf32  }
    { vmemu( r2 ++ #-1) = v4 ; v3.sf =v3.qf32  }
    { vmemu( r2 ++ #-1) = v3 ; v2.sf =v2.qf32  }
    { vmemu( r2 ++ #-1) = v2 ; v1.sf =v1.qf32  }
    { vmemu( r2 ++ #-1) = v1 ; v0.sf =v0.qf32  }
    { vmemu( r2 ++ #-1) = v0 }

    { jumpr r31 } // return
    .size	gemm_asm_cdsp_192_4_1, .-gemm_asm_cdsp_192_4_1

    .p2align 2
    .p2align 4,,15
    .globl gemm_asm_cdsp_192_4_128
    .type gemm_asm_cdsp_192_4_128, @function
gemm_asm_cdsp_192_4_128:
    // TODO: finished implementation

    { jumpr r31 } // return
    .size	gemm_asm_cdsp_192_4_128, .-gemm_asm_cdsp_192_4_128
