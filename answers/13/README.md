# 13

## 12.4

### 1., 2.

```
    .text
    .p2align 2
    .p2align 4,,15
    .globl micro_hvx_qf32
    .type micro_hvx_qf32, @function
micro_hvx_qf32:

    loop0( loop_micro_hvx_qf32, r0 )
loop_micro_hvx_qf32:
    { v28.qf32 = vadd( v30.qf32, v31.qf32 ); v29.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v26.qf32 = vadd( v30.qf32, v31.qf32 ); v27.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v24.qf32 = vadd( v30.qf32, v31.qf32 ); v25.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v22.qf32 = vadd( v30.qf32, v31.qf32 ); v23.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v20.qf32 = vadd( v30.qf32, v31.qf32 ); v21.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v18.qf32 = vadd( v30.qf32, v31.qf32 ); v19.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v16.qf32 = vadd( v30.qf32, v31.qf32 ); v17.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v14.qf32 = vadd( v30.qf32, v31.qf32 ); v15.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v12.qf32 = vadd( v30.qf32, v31.qf32 ); v13.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v10.qf32 = vadd( v30.qf32, v31.qf32 ); v11.qf32 = vmpy( v30.qf32, v31.qf32 ) }
    { v8.qf32  = vadd( v30.qf32, v31.qf32 ); v9.qf32  = vmpy( v30.qf32, v31.qf32 ) }
    { v6.qf32  = vadd( v30.qf32, v31.qf32 ); v7.qf32  = vmpy( v30.qf32, v31.qf32 ) }
    { v4.qf32  = vadd( v30.qf32, v31.qf32 ); v5.qf32  = vmpy( v30.qf32, v31.qf32 ) }
    { v2.qf32  = vadd( v30.qf32, v31.qf32 ); v3.qf32  = vmpy( v30.qf32, v31.qf32 ) }
    { v0.qf32  = vadd( v30.qf32, v31.qf32 ); v1.qf32  = vmpy( v30.qf32, v31.qf32 ) }: endloop0

    { r0 = #32 * 28 }
    { jumpr r31 } // return
    .size       micro_hvx_qf32, .-micro_hvx_qf32
```

```
Reset loading vote for libmini_hexa_skel.so
=============== running mini_hexa_micro ===============
  duration micro_hvx_qf32: 2.048620
  GFLOPS micro_hvx_qf32: 43.736759
  spawning thread 0
  spawning thread 1
  spawning thread 2
  spawning thread 3
  finished spawning 4 threads
  waiting for thread 0
  waiting for thread 1
  waiting for thread 2
  waiting for thread 3
  duration micro_hvx_qf32 (threaded): 2.167772
  GFLOPS micro_hvx_qf32 (threaded): 165.331040
=============== finished mini_hexa_micro ==============
```

```
=============== running mini_hexa_micro ===============
  duration micro_hvx_qf32: 2.048573
  GFLOPS micro_hvx_qf32: 43.737763
  spawning thread 0
  spawning thread 1
  spawning thread 2
  spawning thread 3
  spawning thread 4
  finished spawning 5 threads
  waiting for thread 0
  waiting for thread 1
  waiting for thread 2
  waiting for thread 3
  waiting for thread 4
  duration micro_hvx_qf32 (threaded): 4.061300
  GFLOPS micro_hvx_qf32 (threaded): 110.309507
=============== finished mini_hexa_micro ==============
```

```
=============== running mini_hexa_micro ===============
  duration micro_hvx_qf32: 2.048337
  GFLOPS micro_hvx_qf32: 43.742802
  spawning thread 0
  spawning thread 1
  spawning thread 2
  finished spawning 3 threads
  waiting for thread 0
  waiting for thread 1
  waiting for thread 2
  duration micro_hvx_qf32 (threaded): 2.031192
  GFLOPS micro_hvx_qf32 (threaded): 132.336086
=============== finished mini_hexa_micro ==============
```

```
=============== running mini_hexa_micro ===============
  duration micro_hvx_qf32: 2.048630
  GFLOPS micro_hvx_qf32: 43.736546
  spawning thread 0
  spawning thread 1
  spawning thread 2
  spawning thread 3
  spawning thread 4
  spawning thread 5
  spawning thread 6
  spawning thread 7
  finished spawning 8 threads
  waiting for thread 0
  waiting for thread 1
  waiting for thread 2
  waiting for thread 3
  waiting for thread 4
  waiting for thread 5
  waiting for thread 6
  waiting for thread 7
  duration micro_hvx_qf32 (threaded): 4.335474
  GFLOPS micro_hvx_qf32 (threaded): 165.333710
=============== finished mini_hexa_micro ==============
```

cDSP has 4 hardware threads, so a muiltible of 4 is the fastest configuration
and achieves 165.33 GFLOPS. The calculation is a bit of, it should be $32*30$
FLOP per Iteration, instead of $32*28$.

### 3
#### `my_asm_2.s`

```
=============== running mini_hexa_sgemm ===============
testing gemm_asm_cdsp_192_4_1 kernel
  calling gemm_asm_cdsp_192_4_1
  max error: 0.000061
  duration gemm_asm_cdsp_192_4_1: 0.032448 seconds
  GFLOPS gemm_asm_cdsp_192_4_1: 4.733728
...
=============== finished mini_hexa_sgemm ===============
```

#### `my_asm.s`

```
=============== running mini_hexa_sgemm ===============
testing gemm_asm_cdsp_192_4_1 kernel
  calling gemm_asm_cdsp_192_4_1
  max error: 0.000061
  duration gemm_asm_cdsp_192_4_1: 0.029539 seconds
  GFLOPS gemm_asm_cdsp_192_4_1: 5.199905
...
=============== finished mini_hexa_sgemm ===============
```

### 4

```
=============== running mini_hexa_sgemm ===============
....
testing gemm_asm_cdsp_192_4_128 kernel
  calling gemm_asm_cdsp_192_4_128
  max error: 0.000025
  duration gemm_asm_cdsp_192_4_128: 1.272179 seconds
  GFLOPS gemm_asm_cdsp_192_4_128: 15.454429
=============== finished mini_hexa_sgemm ===============
```
