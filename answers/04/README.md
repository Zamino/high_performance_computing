# 04

## 5. Writing Assembly Code: AArch64

### 5.1. A New World


`./aarch64_asm/driver_data_processing_6.cpp`
```cpp
#include <bitset>
#include <iostream>

extern "C" {
  uint64_t data_processing_6( uint32_t i_a,
      uint32_t i_b );
}

int main() {
  uint32_t l_a6 = 17;
  uint32_t l_b6 = 18;
  uint64_t l_nzcv6 = data_processing_6( l_a6,
      l_b6 );

  std::cout << "data_processing_6("
    << l_a6 << ", "
    << l_b6 << ") #1: "
    << std::bitset< 64 >( l_nzcv6 )
    << std::endl;

  l_a6 = 17;
  l_b6 = 17;
  l_nzcv6 = data_processing_6( l_a6,
      l_b6 );

  std::cout << "data_processing_6("
    << l_a6 << ", "
    << l_b6 << ") #2: "
    << std::bitset< 64 >( l_nzcv6 )
    << std::endl;

    return EXIT_SUCCESS;
}
```

`./aarch64_asm/kernels/data_processing_6.s`
```assembly
        .text
        .type data_processing_6, %function
        .global data_processing_6
        /*
         * Compares two 32-bit values.
         *
         * @param x0: first 32-bit value.
         * @param x1: second 32-bit value.
         * @return x0: NZCV after the compare-operation.
         */
data_processing_6:
        cmp w0, w1
        mrs x0, nzcv

        ret
        .size data_processing_6, (. - data_processing_6)
```

`./aarch64_asm/kernels/data_processing_6_alias.s`
```assembly
        .text
        .type data_processing_6, %function
        .global data_processing_6
        /*
         * Compares two 32-bit values.
         *
         * @param x0: first 32-bit value.
         * @param x1: second 32-bit value.
         * @return x0: NZCV after the compare-operation.
         */
data_processing_6:
        subs wzr, w0, w1
        mrs x0, nzcv

        ret
        .size data_processing_6, (. - data_processing_6)
```

`./aarch64_asm/kernels/data_processing_6_word.s`
```assembly
        .text
        .type data_processing_6, %function
        .global data_processing_6
        /*
         * Compares two 32-bit values.
         *
         * @param x0: first 32-bit value.
         * @param x1: second 32-bit value.
         * @return x0: NZCV after the compare-operation.
         */
data_processing_6:
        .word 0x6b01001f
        mrs x0, nzcv

        ret
        .size data_processing_6, (. - data_processing_6)
```

```
$ make data_processing_6
gcc -r -pedantic -Wall -Wextra -Werror -c kernels/data_processing_6.s -o ./build/data_processing_6.o
g++ -pedantic -Wall -Wextra -Werror -O2 driver_data_processing_6.cpp ./build/data_processing_6.o -o ./build/data_processing_6
$ ls build/
data_processing_6  data_processing_6.o
$ ./build/data_processing_6
data_processing_6(17, 18) #1: 0000000000000000000000000000000010000000000000000000000000000000
data_processing_6(17, 17) #2: 0000000000000000000000000000000001100000000000000000000000000000
$ hexdump -v build/data_processing_6.o >> hexdump.txt
$ cat hexdump.txt
0000000 457f 464c 0102 0001 0000 0000 0000 0000
0000010 0001 00b7 0001 0000 0000 0000 0000 0000
0000020 0000 0000 0000 0000 0148 0000 0000 0000
0000030 0000 0000 0040 0000 0000 0040 0007 0006
0000040 001f 6b01 4200 d53b 03c0 d65f 0000 0000
0000050 0000 0000 0000 0000 0000 0000 0000 0000
0000060 0000 0000 0000 0000 0000 0000 0003 0001
0000070 0000 0000 0000 0000 0000 0000 0000 0000
0000080 0000 0000 0003 0002 0000 0000 0000 0000
0000090 0000 0000 0000 0000 0000 0000 0003 0003
00000a0 0000 0000 0000 0000 0000 0000 0000 0000
00000b0 0001 0000 0004 fff1 0000 0000 0000 0000
00000c0 0000 0000 0000 0000 000c 0000 0000 0001
00000d0 0000 0000 0000 0000 0000 0000 0000 0000
00000e0 000f 0000 0012 0001 0000 0000 0000 0000
00000f0 000c 0000 0000 0000 6300 3863 4251 5852
0000100 2e51 006f 7824 6400 7461 5f61 7270 636f
0000110 7365 6973 676e 365f 0000 732e 6d79 6174
0000120 0062 732e 7274 6174 0062 732e 7368 7274
0000130 6174 0062 742e 7865 0074 642e 7461 0061
0000140 622e 7373 0000 0000 0000 0000 0000 0000
0000150 0000 0000 0000 0000 0000 0000 0000 0000
0000160 0000 0000 0000 0000 0000 0000 0000 0000
0000170 0000 0000 0000 0000 0000 0000 0000 0000
0000180 0000 0000 0000 0000 001b 0000 0001 0000
0000190 0006 0000 0000 0000 0000 0000 0000 0000
00001a0 0040 0000 0000 0000 000c 0000 0000 0000
00001b0 0000 0000 0000 0000 0004 0000 0000 0000
00001c0 0000 0000 0000 0000 0021 0000 0001 0000
00001d0 0003 0000 0000 0000 0000 0000 0000 0000
00001e0 004c 0000 0000 0000 0000 0000 0000 0000
00001f0 0000 0000 0000 0000 0001 0000 0000 0000
0000200 0000 0000 0000 0000 0027 0000 0008 0000
0000210 0003 0000 0000 0000 0000 0000 0000 0000
0000220 004c 0000 0000 0000 0000 0000 0000 0000
0000230 0000 0000 0000 0000 0001 0000 0000 0000
0000240 0000 0000 0000 0000 0001 0000 0002 0000
0000250 0000 0000 0000 0000 0000 0000 0000 0000
0000260 0050 0000 0000 0000 00a8 0000 0000 0000
0000270 0005 0000 0006 0000 0008 0000 0000 0000
0000280 0018 0000 0000 0000 0009 0000 0003 0000
0000290 0000 0000 0000 0000 0000 0000 0000 0000
00002a0 00f8 0000 0000 0000 0021 0000 0000 0000
00002b0 0000 0000 0000 0000 0001 0000 0000 0000
00002c0 0000 0000 0000 0000 0011 0000 0003 0000
00002d0 0000 0000 0000 0000 0000 0000 0000 0000
00002e0 0119 0000 0000 0000 002c 0000 0000 0000
00002f0 0000 0000 0000 0000 0001 0000 0000 0000
0000300 0000 0000 0000 0000
0000308
$ objdump --disassemble-all build/data_processing_6.o

data_processing_6.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <data_processing_6>:
   0:   6b01001f        cmp     w0, w1
   4:   d53b4200        mrs     x0, nzcv
   8:   d65f03c0        ret
$ make data_processing_6_alias
gcc -r -pedantic -Wall -Wextra -Werror -c kernels/data_processing_6_alias.s -o ./build/data_processing_6_alias.o
$ objdump --disassemble-all build/data_processing_6_alias.o

build/data_processing_6_alias.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <data_processing_6>:
   0:   6b01001f        cmp     w0, w1
   4:   d53b4200        mrs     x0, nzcv
   8:   d65f03c0        ret
$ make data_processing_6_word
gcc -r -pedantic -Wall -Wextra -Werror -c kernels/data_processing_6_word.s -o ./build/data_processing_6_word.o
$ objdump --disassemble-all build/data_processing_6_word.o

build/data_processing_6_word.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <data_processing_6>:
   0:   6b01001f        cmp     w0, w1
   4:   d53b4200        mrs     x0, nzcv
   8:   d65f03c0        ret
```

### 5.2 GDB and Valgrind

#### 1

The function is called with the memory address of the 3rd element of the array.
The call is equivalent to `load_asm( &l_a[2] )`. When called, this value is in
`x0`. The first command increments the value in `x0` by 8, so the value in `x0`
now corresponds to `&l_a[3]` and the value after this memory address is loaded
into `x1`, this value is 400. The next command loads the next two values after
`x0` into `x2` and `x3`. The last load command loads the next two values after
`x0` with an offset of 16 in `x4` and `x5`, i.e. `l_a[5]` in `x4` and `l_a[6]`
in `x5`. The value in `x0` is not changed.

| register | end value |
|----------|-----------|
| `x0`     | `&l_a[3]` |
| `x1`     | 400       |
| `x2`     | 400       |
| `x3`     | 500       |
| `x4`     | 600       |
| `x5`     | 700       |

#### 2

```
$ make load
gcc -g -pedantic -Wall -Wextra -Werror -c kernels/load.s -o ./build/load.o
g++ -g -pedantic -Wall -Wextra -Werror -O2 driver_load.cpp ./build/load.o -o ./build/load
$ gdb ./build/load
...
(gdb) break load_asm
Breakpoint 1 at 0x400bf0: file kernels/load.s, line 6.
(gdb) run
...
Breakpoint 1, load_asm () at kernels/load.s:6
6               ldr x1,     [x0, #8]!
Missing separate debuginfos, use: dnf debuginfo-install glibc-2.36-9.fc37.aarch64 libgcc-12.2.1-4.fc37.aarch64 libstdc++-12.2.1-4.fc37.aarch64
(gdb) info reg
x0             0x432ec0            4402880
x1             0x8                 8
x2             0x432f10            4402960
x3             0x1                 1
x4             0xffffffff          4294967295
x5             0xfbad2a84          4222429828
...
(gdb) step
7               ldp x2, x3, [x0]
(gdb) info reg
x0             0x432ec8            4402888
x1             0x190               400
x2             0x432f10            4402960
x3             0x1                 1
x4             0xffffffff          4294967295
x5             0xfbad2a84          4222429828
...
(gdb) step
8               ldp x4, x5, [x0, #16]
(gdb) info reg
x0             0x432ec8            4402888
x1             0x190               400
x2             0x190               400
x3             0x1f4               500
x4             0xffffffff          4294967295
x5             0xfbad2a84          4222429828
....
(gdb) step
10              ret
(gdb) info reg
x0             0x432ec8            4402888
x1             0x190               400
x2             0x190               400
x3             0x1f4               500
x4             0x258               600
x5             0x2bc               700
...
(gdb)
```

#### 3

These calls would access unallocated memory, which is not allowed. It may be
that the Linux kernel detects this and terminates the program without warning.

##### not ok #1

```
$ make load
gcc -g -pedantic -Wall -Wextra -Werror -c kernels/load.s -o ./build/load.o
g++ -g -pedantic -Wall -Wextra -Werror -O2 driver_load.cpp ./build/load.o -o ./build/load
$ valgrind ./build/load
==578946== Memcheck, a memory error detector
==578946== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==578946== Using Valgrind-3.20.0 and LibVEX; rerun with -h for copyright info
==578946== Command: ./build/load
==578946==
2
==578946== Invalid read of size 8
==578946==    at 0x4009FC: main (driver_load.cpp:23)
==578946==  Address 0x4dcace8 is 24 bytes after a block of size 80 in arena "client"
==578946==
==578946== Invalid read of size 8
==578946==    at 0x400BF4: load_asm (load.s:7)
==578946==  Address 0x4dcace8 is 24 bytes after a block of size 80 in arena "client"
==578946==
==578946== Invalid read of size 8
==578946==    at 0x400BF8: load_asm (load.s:8)
==578946==  Address 0x4dcacf8 is 24 bytes before a block of size 1,024 alloc'd
==578946==    at 0x48852DC: malloc (vg_replace_malloc.c:393)
==578946==    by 0x4C6F58B: _IO_file_doallocate (filedoalloc.c:101)
==578946==    by 0x4C7E5C3: _IO_doallocbuf (genops.c:347)
==578946==    by 0x4C7E5C3: _IO_doallocbuf (genops.c:342)
==578946==    by 0x4C7D923: _IO_file_overflow@@GLIBC_2.17 (fileops.c:744)
==578946==    by 0x4C7CA37: _IO_new_file_xsputn (fileops.c:1243)
==578946==    by 0x4C7CA37: _IO_file_xsputn@@GLIBC_2.17 (fileops.c:1196)
==578946==    by 0x4C70BC3: fwrite (iofwrite.c:39)
==578946==    by 0x49E8B87: sputn (streambuf:456)
==578946==    by 0x49E8B87: _M_put (streambuf_iterator.h:326)
==578946==    by 0x49E8B87: __write<char> (locale_facets.h:124)
==578946==    by 0x49E8B87: std::ostreambuf_iterator<char, std::char_traits<char> > std::num_put<char, std::ostreambuf_iterator<char, std::char_traits<char> > >::_M_insert_int<unsigned long>(std::ostreambuf_iterator<char, std::char_traits<char> >, std::ios_base&, char, unsigned long) const (locale_facets.tcc:951)
==578946==    by 0x49F6507: put (locale_facets.h:2405)
==578946==    by 0x49F6507: std::ostream& std::ostream::_M_insert<unsigned long>(unsigned long) (ostream.tcc:73)
==578946==    by 0x4009C7: operator<< (ostream:171)
==578946==    by 0x4009C7: main (driver_load.cpp:17)
==578946==
==578946==
==578946== HEAP SUMMARY:
==578946==     in use at exit: 0 bytes in 0 blocks
==578946==   total heap usage: 3 allocs, 3 frees, 73,808 bytes allocated
==578946==
==578946== All heap blocks were freed -- no leaks are possible
==578946==
==578946== For lists of detected and suppressed errors, rerun with: -s
==578946== ERROR SUMMARY: 5 errors from 3 contexts (suppressed: 0 from 0)
```

All three memory accesses are outside the allocated memory. The last two read
accesses have size 16, but Valgrind sees them only as size 8.

##### not ok #2

```
$ make load
gcc -g -pedantic -Wall -Wextra -Werror -c kernels/load.s -o ./build/load.o
g++ -g -pedantic -Wall -Wextra -Werror -O2 driver_load.cpp ./build/load.o -o ./build/load
$ valgrind ./build/load
==578981== Memcheck, a memory error detector
==578981== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==578981== Using Valgrind-3.20.0 and LibVEX; rerun with -h for copyright info
==578981== Command: ./build/load
==578981==
2
==578981== Invalid read of size 8
==578981==    at 0x400BF4: load_asm (load.s:7)
==578981==  Address 0x4dcacd0 is 0 bytes after a block of size 80 alloc'd
==578981==    at 0x4886DF4: operator new[](unsigned long) (vg_replace_malloc.c:652)
==578981==    by 0x400997: main (driver_load.cpp:12)
==578981==
==578981== Invalid read of size 8
==578981==    at 0x400BF8: load_asm (load.s:8)
==578981==  Address 0x4dcacd8 is 8 bytes after a block of size 80 alloc'd
==578981==    at 0x4886DF4: operator new[](unsigned long) (vg_replace_malloc.c:652)
==578981==    by 0x400997: main (driver_load.cpp:12)
==578981==
==578981==
==578981== HEAP SUMMARY:
==578981==     in use at exit: 0 bytes in 0 blocks
==578981==   total heap usage: 3 allocs, 3 frees, 73,808 bytes allocated
==578981==
==578981== All heap blocks were freed -- no leaks are possible
==578981==
==578981== For lists of detected and suppressed errors, rerun with: -s
==578981== ERROR SUMMARY: 3 errors from 2 contexts (suppressed: 0 from 0)
```

Here, only the second half of the second load and the third load are out of
bounds. Valgrind sees them each as size 8, although the third is 16 bytes.

##### not ok #3

```
$ make load
gcc -g -pedantic -Wall -Wextra -Werror -c kernels/load.s -o ./build/load.o
g++ -g -pedantic -Wall -Wextra -Werror -O2 driver_load.cpp ./build/load.o -o ./build/load
$ valgrind ./build/load
==579000== Memcheck, a memory error detector
==579000== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==579000== Using Valgrind-3.20.0 and LibVEX; rerun with -h for copyright info
==579000== Command: ./build/load
==579000==
2
==579000== Invalid read of size 8
==579000==    at 0x400BF8: load_asm (load.s:8)
==579000==  Address 0x4dcacd0 is 0 bytes after a block of size 80 alloc'd
==579000==    at 0x4886DF4: operator new[](unsigned long) (vg_replace_malloc.c:652)
==579000==    by 0x400997: main (driver_load.cpp:12)
==579000==
==579000==
==579000== HEAP SUMMARY:
==579000==     in use at exit: 0 bytes in 0 blocks
==579000==   total heap usage: 3 allocs, 3 frees, 73,808 bytes allocated
==579000==
==579000== All heap blocks were freed -- no leaks are possible
==579000==
==579000== For lists of detected and suppressed errors, rerun with: -s
==579000== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)
```

Here, only the second half of the last loading instruction is invalid, which
would access `l_a[10]`.

### 5.3 Copying Data

#### 1

`./aarch64_asm/kernels/copy.s`
```
        .text
        .type   copy_asm, %function
        .global copy_asm
copy_asm:
        ldr w2, [x0], #4
        str x2, [x1], #8
        ldr w2, [x0], #4
        str x2, [x1], #8
        ldr w2, [x0], #4
        str x2, [x1], #8
        ldr w2, [x0], #4
        str x2, [x1], #8
        ldr w2, [x0], #4
        str x2, [x1], #8
        ldr w2, [x0], #4
        str x2, [x1], #8
        ldr w2, [x0]
        str x2, [x1]

        ret
        .size copy_asm, (. - copy_asm)
```

#### 2

`./aarch64_asm/kernels/copy_c.c`
```
#include <stdint.h>

void copy_c( uint32_t const * i_a,
             uint64_t       * o_b) {
  uint32_t tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a;
  *o_b   = tmp;
}
```

#### 3

```
$ make copy
gcc -g -r -pedantic -Wall -Wextra -Werror -c kernels/copy.s -o ./build/copy.o
gcc -S -pedantic -Wall -Wextra -Werror -c kernels/copy_c.c -o ./build/copy_c.s
gcc -r -pedantic -Wall -Wextra -Werror -c kernels/copy_c.c -o ./build/copy_c.o
g++ -pedantic -Wall -Wextra -Werror -O2 driver_copy.cpp ./build/copy.o ./build/copy_c.o -o ./build/copy
$ cat ./build/copy_c.s
        .arch armv8-a
        .file   "copy_c.c"
        .text
        .align  2
        .global copy_c
        .type   copy_c, %function
copy_c:
.LFB0:
        .cfi_startproc
        sub     sp, sp, #32
        .cfi_def_cfa_offset 32
        str     x0, [sp, 8]
        str     x1, [sp]
        ldr     x0, [sp, 8]
        add     x1, x0, 4
        str     x1, [sp, 8]
        ldr     w0, [x0]
        str     w0, [sp, 28]
        ldr     x0, [sp]
        add     x1, x0, 8
        str     x1, [sp]
        ldr     w1, [sp, 28]
        str     x1, [x0]
        ldr     x0, [sp, 8]
        add     x1, x0, 4
        str     x1, [sp, 8]
        ldr     w0, [x0]
        str     w0, [sp, 28]
        ldr     x0, [sp]
        add     x1, x0, 8
        str     x1, [sp]
        ldr     w1, [sp, 28]
        str     x1, [x0]
        ldr     x0, [sp, 8]
        add     x1, x0, 4
        str     x1, [sp, 8]
        ldr     w0, [x0]
        str     w0, [sp, 28]
        ldr     x0, [sp]
        add     x1, x0, 8
        str     x1, [sp]
        ldr     w1, [sp, 28]
        str     x1, [x0]
        ldr     x0, [sp, 8]
        add     x1, x0, 4
        str     x1, [sp, 8]
        ldr     w0, [x0]
        str     w0, [sp, 28]
        ldr     x0, [sp]
        add     x1, x0, 8
        str     x1, [sp]
        ldr     w1, [sp, 28]
        str     x1, [x0]
        ldr     x0, [sp, 8]
        add     x1, x0, 4
        str     x1, [sp, 8]
        ldr     w0, [x0]
        str     w0, [sp, 28]
        ldr     x0, [sp]
        add     x1, x0, 8
        str     x1, [sp]
        ldr     w1, [sp, 28]
        str     x1, [x0]
        ldr     x0, [sp, 8]
        add     x1, x0, 4
        str     x1, [sp, 8]
        ldr     w0, [x0]
        str     w0, [sp, 28]
        ldr     x0, [sp]
        add     x1, x0, 8
        str     x1, [sp]
        ldr     w1, [sp, 28]
        str     x1, [x0]
        ldr     x0, [sp, 8]
        ldr     w0, [x0]
        str     w0, [sp, 28]
        ldr     w1, [sp, 28]
        ldr     x0, [sp]
        str     x1, [x0]
        nop
        add     sp, sp, 32
        .cfi_def_cfa_offset 0
        ret
        .cfi_endproc
.LFE0:
        .size   copy_c, .-copy_c
        .ident  "GCC: (GNU) 12.2.1 20221121 (Red Hat 12.2.1-4)"
        .section        .note.GNU-stack,"",@progbits
$ objdump --disassemble-all build/copy_c.o

build/copy_c.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <copy_c>:
   0:   d10083ff        sub     sp, sp, #0x20
   4:   f90007e0        str     x0, [sp, #8]
   8:   f90003e1        str     x1, [sp]
   c:   f94007e0        ldr     x0, [sp, #8]
  10:   91001001        add     x1, x0, #0x4
  14:   f90007e1        str     x1, [sp, #8]
  18:   b9400000        ldr     w0, [x0]
  1c:   b9001fe0        str     w0, [sp, #28]
  20:   f94003e0        ldr     x0, [sp]
  24:   91002001        add     x1, x0, #0x8
  28:   f90003e1        str     x1, [sp]
  2c:   b9401fe1        ldr     w1, [sp, #28]
  30:   f9000001        str     x1, [x0]
  34:   f94007e0        ldr     x0, [sp, #8]
  38:   91001001        add     x1, x0, #0x4
  3c:   f90007e1        str     x1, [sp, #8]
  40:   b9400000        ldr     w0, [x0]
  44:   b9001fe0        str     w0, [sp, #28]
  48:   f94003e0        ldr     x0, [sp]
  4c:   91002001        add     x1, x0, #0x8
  50:   f90003e1        str     x1, [sp]
  54:   b9401fe1        ldr     w1, [sp, #28]
  58:   f9000001        str     x1, [x0]
  5c:   f94007e0        ldr     x0, [sp, #8]
  60:   91001001        add     x1, x0, #0x4
  64:   f90007e1        str     x1, [sp, #8]
  68:   b9400000        ldr     w0, [x0]
  6c:   b9001fe0        str     w0, [sp, #28]
  70:   f94003e0        ldr     x0, [sp]
  74:   91002001        add     x1, x0, #0x8
  78:   f90003e1        str     x1, [sp]
  7c:   b9401fe1        ldr     w1, [sp, #28]
  80:   f9000001        str     x1, [x0]
  84:   f94007e0        ldr     x0, [sp, #8]
  88:   91001001        add     x1, x0, #0x4
  8c:   f90007e1        str     x1, [sp, #8]
  90:   b9400000        ldr     w0, [x0]
  94:   b9001fe0        str     w0, [sp, #28]
  98:   f94003e0        ldr     x0, [sp]
  9c:   91002001        add     x1, x0, #0x8
  a0:   f90003e1        str     x1, [sp]
  a4:   b9401fe1        ldr     w1, [sp, #28]
  a8:   f9000001        str     x1, [x0]
  ac:   f94007e0        ldr     x0, [sp, #8]
  b0:   91001001        add     x1, x0, #0x4
  b4:   f90007e1        str     x1, [sp, #8]
  b8:   b9400000        ldr     w0, [x0]
  bc:   b9001fe0        str     w0, [sp, #28]
  c0:   f94003e0        ldr     x0, [sp]
  c4:   91002001        add     x1, x0, #0x8
  c8:   f90003e1        str     x1, [sp]
  cc:   b9401fe1        ldr     w1, [sp, #28]
  d0:   f9000001        str     x1, [x0]
  d4:   f94007e0        ldr     x0, [sp, #8]
  d8:   91001001        add     x1, x0, #0x4
  dc:   f90007e1        str     x1, [sp, #8]
  e0:   b9400000        ldr     w0, [x0]
  e4:   b9001fe0        str     w0, [sp, #28]
  e8:   f94003e0        ldr     x0, [sp]
  ec:   91002001        add     x1, x0, #0x8
  f0:   f90003e1        str     x1, [sp]
  f4:   b9401fe1        ldr     w1, [sp, #28]
  f8:   f9000001        str     x1, [x0]
  fc:   f94007e0        ldr     x0, [sp, #8]
 100:   b9400000        ldr     w0, [x0]
 104:   b9001fe0        str     w0, [sp, #28]
 108:   b9401fe1        ldr     w1, [sp, #28]
 10c:   f94003e0        ldr     x0, [sp]
 110:   f9000001        str     x1, [x0]
 114:   d503201f        nop
 118:   910083ff        add     sp, sp, #0x20
 11c:   d65f03c0        ret

Disassembly of section .comment:

0000000000000000 <.comment>:
   0:   43434700        .inst   0x43434700 ; undefined
   4:   4728203a        .inst   0x4728203a ; undefined
   8:   2029554e        .inst   0x2029554e ; undefined
   c:   322e3231        orr     w17, w17, #0x7ffc0000
  10:   3220312e        orr     w14, w9, #0x1fff
  14:   31323230        adds    w16, w17, #0xc8c
  18:   20313231        .inst   0x20313231 ; undefined
  1c:   64655228        .inst   0x64655228 ; undefined
  20:   74614820        .inst   0x74614820 ; undefined
  24:   2e323120        usubw   v0.8h, v9.8h, v18.8b
  28:   2d312e32        stp     s18, s11, [x17, #-120]
  2c:   Address 0x000000000000002c is out of bounds.


Disassembly of section .eh_frame:

0000000000000000 <.eh_frame>:
   0:   00000010        udf     #16
   4:   00000000        udf     #0
   8:   00527a01        .inst   0x00527a01 ; undefined
   c:   011e7804        .inst   0x011e7804 ; undefined
  10:   001f0c1b        .inst   0x001f0c1b ; undefined
  14:   00000018        udf     #24
  18:   00000018        udf     #24
  1c:   00000000        udf     #0
  20:   00000120        udf     #288
  24:   200e4100        .inst   0x200e4100 ; undefined
  28:   000e4602        .inst   0x000e4602 ; undefined
  2c:   00000000        udf     #0
```

### 5.4 A Mini Matrix Kernel

#### 2

`./aarch64_asm/kernels/gemm_asm_gp.s`
```assembly
    .text
        .type gemm_asm_gp, %function
        .global gemm_asm_gp
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (4x2) = (4x2) * (2x2).
         * The input-data is of type uint32_t.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */ 
gemm_asm_gp:
        // store
        stp x19, x20, [sp, #-16]!
        stp x21, x22, [sp, #-16]!
        stp x23, x24, [sp, #-16]!
        stp x25, x26, [sp, #-16]!
        stp x27, x28, [sp, #-16]!
        stp x29, x30, [sp, #-16]!

        // Load
        // A
        ldp  w4,  w5, [x0], #8
        ldp  w6,  w7, [x0], #8
        ldp  w8,  w9, [x0], #8
        ldp w10, w11, [x0], #8
        // B
        ldp w12, w13, [x1], #8
        ldp w14, w15, [x1], #8
        // C
        ldp w16, w17, [x2], #8
        ldp w18, w19, [x2], #8
        ldp w20, w21, [x2], #8
        ldp w22, w23, [x2], #8

        // Calc [m k n]
        // 0 0 0
        madd x16,  x4, x12, x16
        // 1 0 0
        madd x17,  x5, x12, x17
        // 2 0 0
        madd x18,  x6, x12, x18
        // 3 0 0
        madd x19,  x7, x12, x19

        // 0 1 0
        madd x16,  x8, x13, x16
        // 1 1 0
        madd x17,  x9, x13, x17
        // 2 1 0
        madd x18, x10, x13, x18
        // 3 1 0
        madd x19, x11, x13, x19

        // m k n
        // 0 0 1
        madd x20,  x4, x14, x20
        // 1 0 1
        madd x21,  x5, x14, x21
        // 2 0 1
        madd x22,  x6, x14, x22
        // 3 0 1
        madd x23,  x7, x14, x23

        // 0 1 1
        madd x20,  x8, x15, x20
        // 1 1 1
        madd x21,  x9, x15, x21
        // 2 1 1
        madd x22, x10, x15, x22
        // 3 1 1
        madd x23, x11, x15, x23

        // Store C
        stp w22, w23, [x2, #-8]!
        stp w20, w21, [x2, #-8]!
        stp w18, w19, [x2, #-8]!
        stp w16, w17, [x2, #-8]!

        // restore
        ldp x29, x30, [sp], #16
        ldp x27, x28, [sp], #16
        ldp x25, x26, [sp], #16
        ldp x23, x24, [sp], #16
        ldp x21, x22, [sp], #16
        ldp x19, x20, [sp], #16

        ret
        .size gemm_asm_gp, (. - gemm_asm_gp)
```

#### 3

`./aarch64_asm/driver_gemm_asm_gp.cpp`
```cpp
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <assert.h>

extern "C" {
  void gemm_asm_gp( uint32_t const * i_a,
                    uint32_t const * i_b,
                    uint32_t       * i_c );
}

void gemm_ref( // C^MxN += A^MxK * B^KxN
    uint32_t* const i_a,   uint32_t* const i_b,   uint32_t* io_c, // data
    int64_t         i_m,   int64_t         i_n,   int64_t   i_k,  // dimensions
    int64_t         i_lda, int64_t         i_ldb, int64_t   i_ldc // data_row lengths
    ) {
  for (int m=0; m<i_m; m++) {
    for (int n=0; n<i_n; n++) {
      for (int k=0; k<i_k; k++) {
        io_c[m + n * i_ldc] += i_a[m + k * i_lda] * i_b[k + n * i_ldb];
      }
    }
  }
}

int main() {
  uint32_t l_a[8] = {
    1,
    3,
    5,
    7,
       2,
       4,
       6,
       8
  };
  uint32_t l_b[4] = {
    1,
    2,
       3,
       4
  };
  uint32_t l_c[8] = {
    1,
    3,
    5,
    7,
       2,
       4,
       6,
       8
  };

  uint32_t l_d[8] = {
    1,
    3,
    5,
    7,
       2,
       4,
       6,
       8
  };

  gemm_asm_gp( l_a, l_b, l_c);
  gemm_ref(l_a, l_b, l_d, 4, 2, 2, 4, 2 ,4);

  for (int i=0; i<4; i++) {
    std::cout << l_c[i] << " " << l_c[i+4];
    std::cout << std::endl;
  }

  for (int i=0; i<8; i++) {
    assert(l_c[i] == l_d[i]);
  }

  return EXIT_SUCCESS;
}
```

```
$ make gemm_asm_gp
gcc -g -pedantic -Wall -Wextra -Werror -c kernels/gemm_asm_gp.s -o ./build/gemm_asm_gp.o
g++ -g -pedantic -Wall -Wextra -Werror -O2 driver_gemm_asm_gp.cpp ./build/gemm_asm_gp.o -o ./build/gemm_asm_gp
$ ./build/gemm_asm_gp
6 13
14 29
22 45
30 61
```
