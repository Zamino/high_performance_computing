#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <assert.h>

extern "C" {
  void gemm_asm_gp( uint32_t const * i_a,
                    uint32_t const * i_b,
                    uint32_t       * i_c );
}

void gemm_ref( // C^MxN += A^MxK * B^KxN
    uint32_t* const i_a,   uint32_t* const i_b,   uint32_t* io_c, // data
    int64_t         i_m,   int64_t         i_n,   int64_t   i_k,  // dimensions
    int64_t         i_lda, int64_t         i_ldb, int64_t   i_ldc // data_row lengths
    ) {
  for (int m=0; m<i_m; m++) {
    for (int n=0; n<i_n; n++) {
      for (int k=0; k<i_k; k++) {
        io_c[m + n * i_ldc] += i_a[m + k * i_lda] * i_b[k + n * i_ldb];
      }
    }
  }
}

int main() {
  uint32_t l_a[8] = {
    1,
    3,
    5,
    7,
       2,
       4,
       6,
       8
  };
  uint32_t l_b[4] = {
    1,
    2,
       3,
       4
  };
  uint32_t l_c[8] = {
    1,
    3,
    5,
    7,
       2,
       4,
       6,
       8
  };

  uint32_t l_d[8] = {
    1,
    3,
    5,
    7,
       2,
       4,
       6,
       8
  };

  gemm_asm_gp( l_a, l_b, l_c);
  gemm_ref(l_a, l_b, l_d, 4, 2, 2, 4, 2 ,4);

  for (int i=0; i<4; i++) {
    std::cout << l_c[i] << " " << l_c[i+4];
    std::cout << std::endl;
  }

  for (int i=0; i<8; i++) {
    assert(l_c[i] == l_d[i]);
  }

  return EXIT_SUCCESS;
}
