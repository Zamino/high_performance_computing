    .text
        .type gemm_asm_gp, %function
        .global gemm_asm_gp
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (4x2) = (4x2) * (2x2).
         * The input-data is of type uint32_t.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */ 
gemm_asm_gp:
        // store
        stp x19, x20, [sp, #-16]!
        stp x21, x22, [sp, #-16]!
        stp x23, x24, [sp, #-16]!
        stp x25, x26, [sp, #-16]!
        stp x27, x28, [sp, #-16]!
        stp x29, x30, [sp, #-16]!

        // Load
        // A
        ldp  w4,  w5, [x0], #8
        ldp  w6,  w7, [x0], #8
        ldp  w8,  w9, [x0], #8
        ldp w10, w11, [x0], #8
        // B
        ldp w12, w13, [x1], #8
        ldp w14, w15, [x1], #8
        // C
        ldp w16, w17, [x2], #8
        ldp w18, w19, [x2], #8
        ldp w20, w21, [x2], #8
        ldp w22, w23, [x2], #8

        // Calc [m k n]
        // 0 0 0
        madd x16,  x4, x12, x16
        // 1 0 0
        madd x17,  x5, x12, x17
        // 2 0 0
        madd x18,  x6, x12, x18
        // 3 0 0
        madd x19,  x7, x12, x19

        // 0 1 0
        madd x16,  x8, x13, x16
        // 1 1 0
        madd x17,  x9, x13, x17
        // 2 1 0
        madd x18, x10, x13, x18
        // 3 1 0
        madd x19, x11, x13, x19

        // m k n
        // 0 0 1
        madd x20,  x4, x14, x20
        // 1 0 1
        madd x21,  x5, x14, x21
        // 2 0 1
        madd x22,  x6, x14, x22
        // 3 0 1
        madd x23,  x7, x14, x23

        // 0 1 1
        madd x20,  x8, x15, x20
        // 1 1 1
        madd x21,  x9, x15, x21
        // 2 1 1
        madd x22, x10, x15, x22
        // 3 1 1
        madd x23, x11, x15, x23

        // Store C
        stp w22, w23, [x2, #-8]!
        stp w20, w21, [x2, #-8]!
        stp w18, w19, [x2, #-8]!
        stp w16, w17, [x2, #-8]!

        // restore
        ldp x29, x30, [sp], #16
        ldp x27, x28, [sp], #16
        ldp x25, x26, [sp], #16
        ldp x23, x24, [sp], #16
        ldp x21, x22, [sp], #16
        ldp x19, x20, [sp], #16

        ret
        .size gemm_asm_gp, (. - gemm_asm_gp)
