#include <stdint.h>

void copy_c( uint32_t const * i_a,
             uint64_t       * o_b) {
  uint32_t tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a++;
  *o_b++ = tmp;
  tmp    = *i_a;
  *o_b   = tmp;
}
