        .text
        .type data_processing_6, %function
        .global data_processing_6
        /*
         * Compares two 32-bit values.
         *
         * @param x0: first 32-bit value.
         * @param x1: second 32-bit value.
         * @return x0: NZCV after the compare-operation.
         */
data_processing_6:
        subs wzr, w0, w1
        mrs x0, nzcv

        ret
        .size data_processing_6, (. - data_processing_6)
