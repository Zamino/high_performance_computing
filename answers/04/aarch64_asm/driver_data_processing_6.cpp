#include <bitset>
#include <iostream>

extern "C" {
  uint64_t data_processing_6( uint32_t i_a,
      uint32_t i_b );
}

int main() {
  uint32_t l_a6 = 17;
  uint32_t l_b6 = 18;
  uint64_t l_nzcv6 = data_processing_6( l_a6,
      l_b6 );

  std::cout << "data_processing_6("
    << l_a6 << ", "
    << l_b6 << ") #1: "
    << std::bitset< 64 >( l_nzcv6 )
    << std::endl;

  l_a6 = 17;
  l_b6 = 17;
  l_nzcv6 = data_processing_6( l_a6,
      l_b6 );

  std::cout << "data_processing_6("
    << l_a6 << ", "
    << l_b6 << ") #2: "
    << std::bitset< 64 >( l_nzcv6 )
    << std::endl;

    return EXIT_SUCCESS;
}
