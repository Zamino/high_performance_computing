# 10

## 11. Back to the Compiler

### 1.

### 2.

```
$ CXX=g++ make
g++ -g -pedantic -Wall -Wextra -Werror -O2 -fopenmp -ftree-vectorize -fopt-info-vec-all -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:32: optimized: loop vectorized using 16 byte vectors
kernels/triad.cpp:7:32: optimized:  loop versioned for vectorization because of possible aliasing
kernels/triad.cpp:3:6: note: vectorized 1 loops in function.
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V4SF
kernels/triad.cpp:3:6: note: ***** The result for vector mode V16QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode V8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode V4HI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V2SI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V2SI
g++ -g -pedantic -Wall -Wextra -Werror -O2 -fopenmp driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
$ objdump -d build/kernels/triad.o

build/kernels/triad.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <_Z12triad_simplemPKfS0_Pf>:
   0:   b40002a0        cbz     x0, 54 <_Z12triad_simplemPKfS0_Pf+0x54>
   4:   d1000404        sub     x4, x0, #0x1
   8:   f1000c9f        cmp     x4, #0x3
   c:   54000569        b.ls    b8 <_Z12triad_simplemPKfS0_Pf+0xb8>  // b.plast
  10:   91001024        add     x4, x1, #0x4
  14:   91001045        add     x5, x2, #0x4
  18:   cb040064        sub     x4, x3, x4
  1c:   cb050065        sub     x5, x3, x5
  20:   f100209f        cmp     x4, #0x8
  24:   d2800004        mov     x4, #0x0                        // #0
  28:   fa4888a0        ccmp    x5, #0x8, #0x0, hi  // hi = pmore
  2c:   54000168        b.hi    58 <_Z12triad_simplemPKfS0_Pf+0x58>  // b.pmore
  30:   1e201002        fmov    s2, #2.000000000000000000e+00
  34:   d503201f        nop
  38:   bc647841        ldr     s1, [x2, x4, lsl #2]
  3c:   bc647820        ldr     s0, [x1, x4, lsl #2]
  40:   1f020020        fmadd   s0, s1, s2, s0
  44:   bc247860        str     s0, [x3, x4, lsl #2]
  48:   91000484        add     x4, x4, #0x1
  4c:   eb04001f        cmp     x0, x4
  50:   54ffff41        b.ne    38 <_Z12triad_simplemPKfS0_Pf+0x38>  // b.any
  54:   d65f03c0        ret
  58:   4f00f402        fmov    v2.4s, #2.000000000000000000e+00
  5c:   d342fc05        lsr     x5, x0, #2
  60:   d37ceca5        lsl     x5, x5, #4
  64:   d503201f        nop
  68:   3ce46841        ldr     q1, [x2, x4]
  6c:   3ce46820        ldr     q0, [x1, x4]
  70:   4e22cc20        fmla    v0.4s, v1.4s, v2.4s
  74:   3ca46860        str     q0, [x3, x4]
  78:   91004084        add     x4, x4, #0x10
  7c:   eb0400bf        cmp     x5, x4
  80:   54ffff41        b.ne    68 <_Z12triad_simplemPKfS0_Pf+0x68>  // b.any
  84:   927ef404        and     x4, x0, #0xfffffffffffffffc
  88:   f240041f        tst     x0, #0x3
  8c:   54fffe40        b.eq    54 <_Z12triad_simplemPKfS0_Pf+0x54>  // b.none
  90:   1e201002        fmov    s2, #2.000000000000000000e+00
  94:   d503201f        nop
  98:   bc647841        ldr     s1, [x2, x4, lsl #2]
  9c:   bc647820        ldr     s0, [x1, x4, lsl #2]
  a0:   1f020020        fmadd   s0, s1, s2, s0
  a4:   bc247860        str     s0, [x3, x4, lsl #2]
  a8:   91000484        add     x4, x4, #0x1
  ac:   eb04001f        cmp     x0, x4
  b0:   54ffff48        b.hi    98 <_Z12triad_simplemPKfS0_Pf+0x98>  // b.pmore
  b4:   d65f03c0        ret
  b8:   d2800004        mov     x4, #0x0                        // #0
  bc:   17ffffdd        b       30 <_Z12triad_simplemPKfS0_Pf+0x30>
```

```
$ CXX=clang++ make
clang++ -g -pedantic -Wall -Wextra -Werror -O2 -fopenmp -Rpass=loop-vectorize -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:3: remark: vectorized loop (vectorization width: 4, interleaved count: 2) [-Rpass=loop-vectorize]
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
  ^
clang++ -g -pedantic -Wall -Wextra -Werror -O2 -fopenmp driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
$ objdump -d build/kernels/triad.o

build/kernels/triad.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <_Z12triad_simplemPKfS0_Pf>:
   0:   b4000540        cbz     x0, a8 <_Z12triad_simplemPKfS0_Pf+0xa8>
   4:   f100201f        cmp     x0, #0x8
   8:   54000062        b.cs    14 <_Z12triad_simplemPKfS0_Pf+0x14>  // b.hs, b.nlast
   c:   aa1f03e8        mov     x8, xzr
  10:   1400001a        b       78 <_Z12triad_simplemPKfS0_Pf+0x78>
  14:   aa1f03e8        mov     x8, xzr
  18:   cb010069        sub     x9, x3, x1
  1c:   f100813f        cmp     x9, #0x20
  20:   540002c3        b.cc    78 <_Z12triad_simplemPKfS0_Pf+0x78>  // b.lo, b.ul, b.last
  24:   cb020069        sub     x9, x3, x2
  28:   f100813f        cmp     x9, #0x20
  2c:   54000263        b.cc    78 <_Z12triad_simplemPKfS0_Pf+0x78>  // b.lo, b.ul, b.last
  30:   927df008        and     x8, x0, #0xfffffffffffffff8
  34:   91004029        add     x9, x1, #0x10
  38:   4f026400        movi    v0.4s, #0x40, lsl #24
  3c:   9100404a        add     x10, x2, #0x10
  40:   9100406b        add     x11, x3, #0x10
  44:   aa0803ec        mov     x12, x8
  48:   ad7f8921        ldp     q1, q2, [x9, #-16]
  4c:   91008129        add     x9, x9, #0x20
  50:   f100218c        subs    x12, x12, #0x8
  54:   ad7f9143        ldp     q3, q4, [x10, #-16]
  58:   9100814a        add     x10, x10, #0x20
  5c:   4e23cc01        fmla    v1.4s, v0.4s, v3.4s
  60:   4e24cc02        fmla    v2.4s, v0.4s, v4.4s
  64:   ad3f8961        stp     q1, q2, [x11, #-16]
  68:   9100816b        add     x11, x11, #0x20
  6c:   54fffee1        b.ne    48 <_Z12triad_simplemPKfS0_Pf+0x48>  // b.any
  70:   eb00011f        cmp     x8, x0
  74:   540001a0        b.eq    a8 <_Z12triad_simplemPKfS0_Pf+0xa8>  // b.none
  78:   d37ef50b        lsl     x11, x8, #2
  7c:   cb080009        sub     x9, x0, x8
  80:   8b0b0068        add     x8, x3, x11
  84:   8b0b004a        add     x10, x2, x11
  88:   8b0b002b        add     x11, x1, x11
  8c:   1e201000        fmov    s0, #2.000000000000000000e+00
  90:   bc404561        ldr     s1, [x11], #4
  94:   bc404542        ldr     s2, [x10], #4
  98:   f1000529        subs    x9, x9, #0x1
  9c:   1f000441        fmadd   s1, s2, s0, s1
  a0:   bc004501        str     s1, [x8], #4
  a4:   54ffff61        b.ne    90 <_Z12triad_simplemPKfS0_Pf+0x90>  // b.any
  a8:   d65f03c0        ret
```

Both have vectorized the code but only used asimd.

```
$ CXX=g++ make
g++ -g -pedantic -Wall -Wextra -Werror -O2 -fopenmp -march=armv8-a+sve -ftree-vectorize -fopt-info-vec-all -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:32: optimized: loop vectorized using variable length vectors
kernels/triad.cpp:7:32: optimized:  loop versioned for vectorization because of possible aliasing
kernels/triad.cpp:3:6: note: vectorized 1 loops in function.
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode VNx4SF
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx16QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx4QI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode VNx2QI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode VNx2QI
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V16QI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V16QI
kernels/triad.cpp:3:6: note: ***** The result for vector mode V8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode V4HI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V2SI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V2SI
g++ -g -pedantic -Wall -Wextra -Werror -O2 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
$ objdump -d build/kernels/triad.o

build/kernels/triad.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <_Z12triad_simplemPKfS0_Pf>:
   0:   b40002e0        cbz     x0, 5c <_Z12triad_simplemPKfS0_Pf+0x5c>
   4:   91001044        add     x4, x2, #0x4
   8:   91001025        add     x5, x1, #0x4
   c:   cb050065        sub     x5, x3, x5
  10:   cb040064        sub     x4, x3, x4
  14:   eb05009f        cmp     x4, x5
  18:   0420e3e6        cntb    x6
  1c:   9a859084        csel    x4, x4, x5, ls  // ls = plast
  20:   d10020c5        sub     x5, x6, #0x8
  24:   eb05009f        cmp     x4, x5
  28:   d2800004        mov     x4, #0x0                        // #0
  2c:   540001a9        b.ls    60 <_Z12triad_simplemPKfS0_Pf+0x60>  // b.plast
  30:   04a0e3e5        cntw    x5
  34:   25a01fe0        whilelo p0.s, xzr, x0
  38:   25b9c002        fmov    z2.s, #2.000000000000000000e+00
  3c:   2518e3e1        ptrue   p1.b
  40:   a5444021        ld1w    {z1.s}, p0/z, [x1, x4, lsl #2]
  44:   a5444040        ld1w    {z0.s}, p0/z, [x2, x4, lsl #2]
  48:   65a18440        fmad    z0.s, p1/m, z2.s, z1.s
  4c:   e5444060        st1w    {z0.s}, p0, [x3, x4, lsl #2]
  50:   8b050084        add     x4, x4, x5
  54:   25a01c80        whilelo p0.s, x4, x0
  58:   54ffff41        b.ne    40 <_Z12triad_simplemPKfS0_Pf+0x40>  // b.any
  5c:   d65f03c0        ret
  60:   1e201002        fmov    s2, #2.000000000000000000e+00
  64:   d503201f        nop
  68:   bc647841        ldr     s1, [x2, x4, lsl #2]
  6c:   bc647820        ldr     s0, [x1, x4, lsl #2]
  70:   1f020020        fmadd   s0, s1, s2, s0
  74:   bc247860        str     s0, [x3, x4, lsl #2]
  78:   91000484        add     x4, x4, #0x1
  7c:   eb04001f        cmp     x0, x4
  80:   54ffff41        b.ne    68 <_Z12triad_simplemPKfS0_Pf+0x68>  // b.any
  84:   d65f03c0        ret
```

```
$ CXX=clang++ make
clang++ -g -pedantic -Wall -Wextra -Werror -O2 -fopenmp -march=armv8-a+sve -Rpass=loop-vectorize -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:8:17: remark: Instruction with invalid costs prevented vectorization at VF=(vscale x 1): load [-Rpass-analysis=loop-vectorize]
    o_c[l_va] = i_a[l_va] + 2.0f * i_b[l_va];
                ^
kernels/triad.cpp:8:36: remark: Instruction with invalid costs prevented vectorization at VF=(vscale x 1): load [-Rpass-analysis=loop-vectorize]
    o_c[l_va] = i_a[l_va] + 2.0f * i_b[l_va];
                                   ^
kernels/triad.cpp:8:15: remark: Instruction with invalid costs prevented vectorization at VF=(vscale x 1): store [-Rpass-analysis=loop-vectorize]
    o_c[l_va] = i_a[l_va] + 2.0f * i_b[l_va];
              ^
kernels/triad.cpp:7:3: remark: vectorized loop (vectorization width: vscale x 4, interleaved count: 2) [-Rpass=loop-vectorize]
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
  ^
clang++ -g -pedantic -Wall -Wextra -Werror -O2 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
$ objdump -d build/kernels/triad.o

build/kernels/triad.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <_Z12triad_simplemPKfS0_Pf>:
   0:   b40006e0        cbz     x0, dc <_Z12triad_simplemPKfS0_Pf+0xdc>
   4:   0460e3e8        cnth    x8
   8:   52800209        mov     w9, #0x10                       // #16
   c:   f100411f        cmp     x8, #0x10
  10:   9a898109        csel    x9, x8, x9, hi  // hi = pmore
  14:   eb00013f        cmp     x9, x0
  18:   54000069        b.ls    24 <_Z12triad_simplemPKfS0_Pf+0x24>  // b.plast
  1c:   aa1f03e9        mov     x9, xzr
  20:   14000023        b       ac <_Z12triad_simplemPKfS0_Pf+0xac>
  24:   aa1f03e9        mov     x9, xzr
  28:   04bf504a        rdvl    x10, #2
  2c:   cb01006b        sub     x11, x3, x1
  30:   eb0a017f        cmp     x11, x10
  34:   540003c3        b.cc    ac <_Z12triad_simplemPKfS0_Pf+0xac>  // b.lo, b.ul, b.last
  38:   cb02006b        sub     x11, x3, x2
  3c:   eb0a017f        cmp     x11, x10
  40:   54000363        b.cc    ac <_Z12triad_simplemPKfS0_Pf+0xac>  // b.lo, b.ul, b.last
  44:   9ac80809        udiv    x9, x0, x8
  48:   04bf502b        rdvl    x11, #1
  4c:   d344fd6b        lsr     x11, x11, #4
  50:   aa1f03ea        mov     x10, xzr
  54:   92407d6b        and     x11, x11, #0xffffffff
  58:   2598e3e0        ptrue   p0.s
  5c:   d37ced6e        lsl     x14, x11, #4
  60:   25b9c000        fmov    z0.s, #2.000000000000000000e+00
  64:   8b0e002c        add     x12, x1, x14
  68:   8b0e004d        add     x13, x2, x14
  6c:   8b0e006e        add     x14, x3, x14
  70:   9b087d29        mul     x9, x9, x8
  74:   cb09000b        sub     x11, x0, x9
  78:   a54a4021        ld1w    {z1.s}, p0/z, [x1, x10, lsl #2]
  7c:   a54a4042        ld1w    {z2.s}, p0/z, [x2, x10, lsl #2]
  80:   a54a4183        ld1w    {z3.s}, p0/z, [x12, x10, lsl #2]
  84:   a54a41a4        ld1w    {z4.s}, p0/z, [x13, x10, lsl #2]
  88:   65a00041        fmla    z1.s, p0/m, z2.s, z0.s
  8c:   0420bc62        movprfx z2, z3
  90:   65a00082        fmla    z2.s, p0/m, z4.s, z0.s
  94:   e54a4061        st1w    {z1.s}, p0, [x3, x10, lsl #2]
  98:   e54a41c2        st1w    {z2.s}, p0, [x14, x10, lsl #2]
  9c:   8b08014a        add     x10, x10, x8
  a0:   eb0a013f        cmp     x9, x10
  a4:   54fffea1        b.ne    78 <_Z12triad_simplemPKfS0_Pf+0x78>  // b.any
  a8:   b40001ab        cbz     x11, dc <_Z12triad_simplemPKfS0_Pf+0xdc>
  ac:   d37ef52b        lsl     x11, x9, #2
  b0:   cb090008        sub     x8, x0, x9
  b4:   8b0b0069        add     x9, x3, x11
  b8:   8b0b004a        add     x10, x2, x11
  bc:   8b0b002b        add     x11, x1, x11
  c0:   1e201000        fmov    s0, #2.000000000000000000e+00
  c4:   bc404561        ldr     s1, [x11], #4
  c8:   bc404542        ldr     s2, [x10], #4
  cc:   f1000508        subs    x8, x8, #0x1
  d0:   1f000441        fmadd   s1, s2, s0, s1
  d4:   bc004521        str     s1, [x9], #4
  d8:   54ffff61        b.ne    c4 <_Z12triad_simplemPKfS0_Pf+0xc4>  // b.any
  dc:   d65f03c0        ret
```

With the additional argument `-march=armv8-a+sve` both compiler produced
code with SVE instructions.

### 3.

```
$ CXX=clang++ make
clang++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve -Rpass=loop-vectorize -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:3: remark: loop not vectorized [-Rpass-missed=loop-vectorize]
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
  ^
clang++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
$ objdump -d build/kernels/triad.o

build/kernels/triad.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <_Z12triad_simplemPKfS0_Pf>:
   0:   b4000100        cbz     x0, 20 <_Z12triad_simplemPKfS0_Pf+0x20>
   4:   1e201000        fmov    s0, #2.000000000000000000e+00
   8:   bc404421        ldr     s1, [x1], #4
   c:   bc404442        ldr     s2, [x2], #4
  10:   f1000400        subs    x0, x0, #0x1
  14:   1f000441        fmadd   s1, s2, s0, s1
  18:   bc004461        str     s1, [x3], #4
  1c:   54ffff61        b.ne    8 <_Z12triad_simplemPKfS0_Pf+0x8>  // b.any
  20:   d65f03c0        ret
$ CXX=g++ make
g++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve -ftree-vectorize -fopt-info-vec-all -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:32: optimized: loop vectorized using variable length vectors
kernels/triad.cpp:7:32: optimized:  loop versioned for vectorization because of possible aliasing
kernels/triad.cpp:3:6: note: vectorized 1 loops in function.
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode VNx4SF
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx16QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx4QI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode VNx2QI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode VNx2QI
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V16QI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V16QI
kernels/triad.cpp:3:6: note: ***** The result for vector mode V8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode V4HI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V2SI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V2SI
g++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
$ objdump -d build/kernels/triad.o

build/kernels/triad.o:     file format elf64-littleaarch64


Disassembly of section .text:

0000000000000000 <_Z12triad_simplemPKfS0_Pf>:
   0:   b40002c0        cbz     x0, 58 <_Z12triad_simplemPKfS0_Pf+0x58>
   4:   91001024        add     x4, x1, #0x4
   8:   cb040064        sub     x4, x3, x4
   c:   91001045        add     x5, x2, #0x4
  10:   cb050065        sub     x5, x3, x5
  14:   eb05009f        cmp     x4, x5
  18:   9a859084        csel    x4, x4, x5, ls  // ls = plast
  1c:   0420e3e5        cntb    x5
  20:   d10020a5        sub     x5, x5, #0x8
  24:   eb05009f        cmp     x4, x5
  28:   540001a9        b.ls    5c <_Z12triad_simplemPKfS0_Pf+0x5c>  // b.plast
  2c:   25a01fe0        whilelo p0.s, xzr, x0
  30:   d2800004        mov     x4, #0x0                        // #0
  34:   04a0e3e5        cntw    x5
  38:   a5444021        ld1w    {z1.s}, p0/z, [x1, x4, lsl #2]
  3c:   a5444040        ld1w    {z0.s}, p0/z, [x2, x4, lsl #2]
  40:   65800000        fadd    z0.s, z0.s, z0.s
  44:   65810000        fadd    z0.s, z0.s, z1.s
  48:   e5444060        st1w    {z0.s}, p0, [x3, x4, lsl #2]
  4c:   8b050084        add     x4, x4, x5
  50:   25a01c80        whilelo p0.s, x4, x0
  54:   54ffff21        b.ne    38 <_Z12triad_simplemPKfS0_Pf+0x38>  // b.any
  58:   d65f03c0        ret
  5c:   d2800004        mov     x4, #0x0                        // #0
  60:   bc647840        ldr     s0, [x2, x4, lsl #2]
  64:   1e202800        fadd    s0, s0, s0
  68:   bc647821        ldr     s1, [x1, x4, lsl #2]
  6c:   1e212800        fadd    s0, s0, s1
  70:   bc247860        str     s0, [x3, x4, lsl #2]
  74:   91000484        add     x4, x4, #0x1
  78:   eb04001f        cmp     x0, x4
  7c:   54ffff21        b.ne    60 <_Z12triad_simplemPKfS0_Pf+0x60>  // b.any
  80:   17fffff6        b       58 <_Z12triad_simplemPKfS0_Pf+0x58>
```

With flag `-O1` `g++` still uses SVE instructions, where as `clang++` only uses
scalar instructions.

### 4.

```
$ CXX=g++ make
g++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve -ftree-vectorize -fopt-info-vec-all -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:32: optimized: loop vectorized using variable length vectors
kernels/triad.cpp:7:32: optimized:  loop versioned for vectorization because of possible aliasing
kernels/triad.cpp:3:6: note: vectorized 1 loops in function.
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode VNx4SF
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx16QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx4QI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode VNx2QI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode VNx2QI
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V16QI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V16QI
kernels/triad.cpp:3:6: note: ***** The result for vector mode V8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode V4HI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V2SI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V2SI
g++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
$ ./build/auto_vec 1024 10000000
working with:
  N_VALUES: 1024; this means 0.00390625 MiB per array
  N_REPEATS: 10000000
benchmarking triad_simple
  performance:
    duration: 1.01241 seconds
    GFLOPS: 20.229
    GiB/s: 113.038
```

```
$ CXX=clang++ make
clang++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve -Rpass=loop-vectorize -Rpass-missed=loop-vectorize -Rpass-analysis=loop-vectorize -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:3: remark: loop not vectorized [-Rpass-missed=loop-vectorize]
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
  ^
clang++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
$ ./build/auto_vec 1024 10000000
working with:
  N_VALUES: 1024; this means 0.00390625 MiB per array
  N_REPEATS: 10000000
benchmarking triad_simple
  performance:
    duration: 6.08355 seconds
    GFLOPS: 3.36646
    GiB/s: 18.8115
```

The vectorized machine code compiled with `g++` achieves 10,000,000 repeats in
close to 1 second and the scalar machine code compiled with `clang++` needs 6
seconds to do the same work.

### 5.

**Version 1.0**
```cpp
#include "triad.h"

void triad_simple( uint64_t         i_nValues,
                   float    const * i_a,
                   float    const * i_b,
                   float          * o_c ) {
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
    o_c[l_va] = i_a[l_va] + 2.0f * i_b[l_va];
    if (l_va == i_nValues)
            break;
  }
}
```

```
$ CXX=g++ make
g++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve -ftree-vectorize -fopt-info-vec-all -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:32: optimized: loop vectorized using variable length vectors
kernels/triad.cpp:7:32: optimized:  loop versioned for vectorization because of possible aliasing
kernels/triad.cpp:3:6: note: vectorized 1 loops in function.
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode VNx4SF
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx16QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode VNx4QI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode VNx2QI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode VNx2QI
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V16QI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V16QI
kernels/triad.cpp:3:6: note: ***** The result for vector mode V8QI would be the same
kernels/triad.cpp:3:6: note: ***** The result for vector mode V4HI would be the same
kernels/triad.cpp:3:6: note: ***** Re-trying analysis with vector mode V2SI
kernels/triad.cpp:3:6: note: ***** Analysis failed with vector mode V2SI
g++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
```

**Version 1.1**
```cpp
#include "triad.h"

void triad_simple( uint64_t         i_nValues,
                   float    const * i_a,
                   float    const * i_b,
                   float          * o_c ) {
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
    o_c[l_va] = i_a[l_va] + 2.0f * i_b[l_va];
    if (l_va == i_nValues-1)
            break;
  }
}
```


```
$ CXX=g++ make
g++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve -ftree-vectorize -fopt-info-vec-all -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp:7:3: missed: couldn't vectorize loop
kernels/triad.cpp:7:3: missed: not vectorized: control flow in loop.
kernels/triad.cpp:3:6: note: vectorized 0 loops in function.
kernels/triad.cpp:12:1: note: ***** Analysis failed with vector mode VNx4SF
kernels/triad.cpp:12:1: note: ***** Skipping vector mode VNx16QI, which would repeat the analysis for VNx4SF
g++ -g -pedantic -Wall -Wextra -Werror -O1 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
```

Both changes doesn't effect the computation, but in the version 1.1 the `break`
will be triggered and so no vectorization happens.

**Version 2**
```cpp
#include "triad.h"
#include <chrono>

void triad_simple( uint64_t         i_nValues,
                   float    const * i_a,
                   float    const * i_b,
                   float          * o_c ) {
  for( uint64_t l_va = 0; l_va < i_nValues; l_va++ ) {
    auto time = std::chrono::system_clock::now();
    o_c[l_va] = i_a[l_va] + 2.0f * i_b[l_va];
  }
}
```

```
$ CXX=g++ make
g++ -g -pedantic -Wall -Wextra -O1 -fopenmp -march=armv8-a+sve -ftree-vectorize -fopt-info-vec-all -c kernels/triad.cpp -o ./build/kernels/triad.o
kernels/triad.cpp: In function ‘void triad_simple(uint64_t, const float*, const float*, float*)’:
kernels/triad.cpp:9:10: warning: variable ‘time’ set but not used [-Wunused-but-set-variable]
    9 |     auto time = std::chrono::system_clock::now();
      |          ^~~~
kernels/triad.cpp:8:32: missed: couldn't vectorize loop
kernels/triad.cpp:9:47: missed: statement clobbers memory: std::chrono::_V2::system_clock::now ();
kernels/triad.cpp:4:6: note: vectorized 0 loops in function.
kernels/triad.cpp:9:47: missed: statement clobbers memory: std::chrono::_V2::system_clock::now ();
kernels/triad.cpp:12:1: note: ***** Analysis failed with vector mode VNx4SF
kernels/triad.cpp:12:1: note: ***** Skipping vector mode VNx16QI, which would repeat the analysis for VNx4SF
g++ -g -pedantic -Wall -Wextra -O1 -fopenmp -march=armv8-a+sve driver.cpp ./build/kernels/triad.o -o ./build/auto_vec
```

The compiler flag `-Werror` is excluded and a call to `system_clock` is added,
which prevents vectorization.
