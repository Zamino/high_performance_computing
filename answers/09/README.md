# 09

## 10. JITed Kernels in Praxis

### 10.1. Getting Started

#### 1

```
$ tar -xf mini_jit.tar.xz
$ cd mini_jit
$ make test
...
$ ./build/test
===============================================================================
All tests passed (20 assertions in 12 test cases)

```

#### 2

```
$ make all
...
$ ./build/mini_jit
...
$ objdump -m aarch64 -b binary -D small_gemm_sve.bin

small_gemm_sve.bin:     file format binary


Disassembly of section .data:

0000000000000000 <.data>:
   0:   a9bf53f3        stp     x19, x20, [sp, #-16]!
   4:   a9bf5bf5        stp     x21, x22, [sp, #-16]!
   8:   a9bf63f7        stp     x23, x24, [sp, #-16]!
   c:   a9bf6bf9        stp     x25, x26, [sp, #-16]!
  10:   a9bf73fb        stp     x27, x28, [sp, #-16]!
  14:   a9bf7bfd        stp     x29, x30, [sp, #-16]!
  18:   6dbf27e8        stp     d8, d9, [sp, #-16]!
  1c:   6dbf2fea        stp     d10, d11, [sp, #-16]!
  20:   6dbf37ec        stp     d12, d13, [sp, #-16]!
  24:   6dbf3fee        stp     d14, d15, [sp, #-16]!
  28:   6cc13fee        ldp     d14, d15, [sp], #16
  2c:   6cc137ec        ldp     d12, d13, [sp], #16
  30:   6cc12fea        ldp     d10, d11, [sp], #16
  34:   6cc127e8        ldp     d8, d9, [sp], #16
  38:   a8c17bfd        ldp     x29, x30, [sp], #16
  3c:   a8c173fb        ldp     x27, x28, [sp], #16
  40:   a8c16bf9        ldp     x25, x26, [sp], #16
  44:   a8c163f7        ldp     x23, x24, [sp], #16
  48:   a8c15bf5        ldp     x21, x22, [sp], #16
  4c:   a8c153f3        ldp     x19, x20, [sp], #16
  50:   d65f03c0        ret
```

#### 3

`./src/instructions/Base.h`
```cpp
#ifndef MINI_JIT_INSTRUCTIONS_BASE_H
#define MINI_JIT_INSTRUCTIONS_BASE_H

#include <cstdint>

namespace mini_jit {
  namespace instructions {
    class Base;
  }
}

class mini_jit::instructions::Base {
  public:
    //! shift type
    enum shift_t: char {
      lsl = 0,
      lsr = 1,
      asr = 3,
    };


    ...

    /**
     * Gets the machine code for and shifted register.
     *
     * @param i_regGpDes general purpose destination register.
     * @param i_regGpSrc0 first general purpose source register.
     * @param i_regGpSrc1 second general purpose source register.
     * @param i_shift type of the shift.
     * @param i_imm6 amount of the shift type of the shift.
     * @param i_size 32-bit version if 0, 64-bit version if 1.
     *
     * @return instruction.
     **/
    static uint32_t dpAndSr( uint8_t i_regGpDes,
                             uint8_t i_regGpSrc0,
                             uint8_t i_regGpSrc1,
                             uint8_t i_imm6,
                             shift_t i_shift,
                             uint8_t i_size );

    ...

};

#endif
```

`./src/instructions/Base.cpp`
```cpp
#include "Base.h"

...

uint32_t mini_jit::instructions::Base::dpAndSr( uint8_t i_regGpDes,
                                                uint8_t i_regGpSrc0,
                                                uint8_t i_regGpSrc1,
                                                uint8_t i_imm6,
                                                shift_t i_shift,
                                                uint8_t i_size  ) {
  uint32_t l_ins = 0x0a000000;

  l_ins |= 0x1f & i_regGpDes;
  l_ins |= (0x1f & i_regGpSrc0) << 5;
  l_ins |= (0x3f & i_imm6) << 10;
  l_ins |= (0x1f & i_regGpSrc1) << 16;
  l_ins |= (0x3 & i_shift) << 22;
  l_ins |= (0x1 & i_size) << 31;

  return l_ins;
}

...
```

`./src/instructions/Base.test.cpp`
```cpp
#include <catch2/catch.hpp>
#include "Base.h"

TEST_CASE( "Tests dpAndSr.", "[Base][dpAndSr]" ) {
  uint32_t l_ins = 0;
  mini_jit::instructions::Base::shift_t l_shift = mini_jit::instructions::Base::shift_t::lsl;

  // and x12, x22, x4, lsl #17
  l_ins = mini_jit::instructions::Base::dpAndSr( 12,
                                                 22,
                                                 4,
                                                 17,
                                                 l_shift,
                                                 1 );
  REQUIRE( l_ins == 0x8a0446cc );
}

...
```

`./src/generators/MyExample.h`
```cpp
#ifndef MINI_JIT_GENERATORS_MY_EXAMPLE_H
#define MINI_JIT_GENERATORS_MY_EXAMPLE_H

#include "../backend/Kernel.h"
#include "../instructions/Base.h"

namespace mini_jit {
  namespace generators {
    class MyExample;
  }
}

class mini_jit::generators::MyExample {
  private:
    //! kernel backend
    backend::Kernel m_kernel;

  public:
    /**
     * Generates a simple kernel which calculates the bitwise and of register x0
     * and x1 and stores it in x0.
     *
     * @return function pointer to kernel.
     **/
    uint64_t ( *generate() )( uint64_t i_a, uint64_t i_b );
};

#endif
```

`./src/generators/MyExample.cpp`
```cpp
#include "MyExample.h"

uint64_t ( *mini_jit::generators::MyExample::generate() )(uint64_t, uint64_t) {
  uint32_t l_ins = 0;
  mini_jit::instructions::Base::shift_t l_shift =
    mini_jit::instructions::Base::shift_t::lsl;

  // and x0, x0, x1
  l_ins = mini_jit::instructions::Base::dpAndSr( 0,
                                                 0,
                                                 1,
                                                 0,
                                                 l_shift,
                                                 1 );
  m_kernel.addInstruction( l_ins );

  // ret
  l_ins = instructions::Base::bRet();
  m_kernel.addInstruction( l_ins );


  // we might debug through file-io
  std::string l_file = "my_example.bin";
  m_kernel.write( l_file.c_str() );

  m_kernel.setKernel();

  return (uint64_t (*)(uint64_t, uint64_t)) m_kernel.getKernel();
}
```

```
$ ./build/test
===============================================================================
All tests passed (21 assertions in 13 test cases)
$ ./build/mini_jit
###########################
### welcome to mini_jit ###
###########################
my example:
  generating
  running
    result (7 / 9): 1
...
```

#### 4

```
$ make all
...
$ ./build/test
===============================================================================
All tests passed (21 assertions in 13 test cases)

$ ./build/mini_jit
###########################
### welcome to mini_jit ###
###########################
simple:
  generating simple kernel
  running
zsh: illegal hardware instruction (core dumped)  ./build/mini_jit
```

### 10.2 ASIMD: First Steps

#### 1

`./mini_jit/src/instructions/Asimd.h`
```cpp
#ifndef MINI_JIT_INSTRUCTIONS_ASIMD_H
#define MINI_JIT_INSTRUCTIONS_ASIMD_H

#include <cstdint>

namespace mini_jit {
  namespace instructions {
    class Asimd;
  }
}

class mini_jit::instructions::Asimd {
  public:
    //! arrangement specifiers
    enum arrspec_t: char {
      s1 = 0,
      s2 = 1,
      s4 = 2,
      d1 = 3,
      d2 = 4
    };

    //! register sizes
    enum regsize_t: char {
      b = 0,
      h = 1,
      s = 2,
      d = 3,
      q = 4
    };

    ...

     * Gets the machine code for floating-point fused multiply-add (scalar).
     *
     * @param i_regSimdDes SIMD&FP destination register.
     * @param i_regSimdSrc1 first SIMD&FP source register.
     * @param i_regSimdSrc2 second SIMD&FP source register.
     * @param i_regSimdSrc3 third SIMD&FP source register.
     * @param i_arrSpec arrangement specifier.
     *
     * @return instruction.
     **/
    static uint32_t dpFmaddScalar( uint8_t   i_regSimdDes,
                                   uint8_t   i_regSimdSrc1,
                                   uint8_t   i_regSimdSrc2,
                                   uint8_t   i_regSimdSrc3,
                                   regsize_t i_regSize );
};

#endif
```

`./mini_jit/src/instructions/Asimd.cpp`
```cpp
#include "Asimd.h"

...

uint32_t mini_jit::instructions::Asimd::dpFmaddScalar( uint8_t   i_regSimdDes,
                                                       uint8_t   i_regSimdSrc1,
                                                       uint8_t   i_regSimdSrc2,
                                                       uint8_t   i_regSimdSrc3,
                                                       regsize_t i_regSize ) {
  uint32_t l_ins = 0x1F000000;

  l_ins |= 0x1f & i_regSimdDes;

  l_ins |= (0x1f & i_regSimdSrc1) << 5;
  l_ins |= (0x1f & i_regSimdSrc2) << 16;
  l_ins |= (0x1f & i_regSimdSrc3) << 10;

  uint8_t l_size = 0;
  if( i_regSize == regsize_t::h ) {
    l_size = 0b11;
  }
  else if( i_regSize == regsize_t::s ) {
    l_size = 0b00;
  }
  else if( i_regSize == regsize_t::d ) {
    l_size = 0b01;
  }
  l_ins |= l_size << 22;

  return l_ins;
}
```

