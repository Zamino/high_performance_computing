#ifndef MINI_JIT_GENERATORS_MY_EXAMPLE_H
#define MINI_JIT_GENERATORS_MY_EXAMPLE_H

#include "../backend/Kernel.h"
#include "../instructions/Base.h"

namespace mini_jit {
  namespace generators {
    class MyExample;
  }
}

class mini_jit::generators::MyExample {
  private:
    //! kernel backend
    backend::Kernel m_kernel;

  public:
    /**
     * Generates a simple kernel which calculates the bitwise and of register x0
     * and x1 and stores it in x0.
     *
     * @return function pointer to kernel.
     **/
    uint64_t ( *generate() )( uint64_t i_a, uint64_t i_b );
};

#endif
