#include "MyExample.h"

uint64_t ( *mini_jit::generators::MyExample::generate() )(uint64_t, uint64_t) {
  uint32_t l_ins = 0;
  mini_jit::instructions::Base::shift_t l_shift =
    mini_jit::instructions::Base::shift_t::lsl;

  // and x0, x0, x1
  l_ins = mini_jit::instructions::Base::dpAndSr( 0,
                                                 0,
                                                 1,
                                                 0,
                                                 l_shift,
                                                 1 );
  m_kernel.addInstruction( l_ins );

  // ret
  l_ins = instructions::Base::bRet();
  m_kernel.addInstruction( l_ins );


  // we might debug through file-io
  std::string l_file = "my_example.bin";
  m_kernel.write( l_file.c_str() );

  m_kernel.setKernel();

  return (uint64_t (*)(uint64_t, uint64_t)) m_kernel.getKernel();
}
