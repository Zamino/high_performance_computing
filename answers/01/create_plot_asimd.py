import os
import plotly.graph_objects as go
import pandas as pd
import plotly.io as pio
pio.kaleido.scope.mathjax = None


def setup():
    if not os.path.exists("./plots"):
        os.mkdir("./plots")

def main():
    setup()
    dir = "./benchmark_results/"

    csvs = [
            "threads_1",
            "threads_2",
            "threads_3",
            "threads_4",
            ]
    variants = [
            "peak_asimd_fmla_sp",
            "peak_asimd_fmla_dp",
            "peak_asimd_fmadd_sp",
            "peak_asimd_fmadd_dp",
            ]

    fig = go.Figure()

    df_all = None
    for csv in csvs:
        df = pd.read_csv(open(dir + csv + ".csv"))
        df_all = pd.concat([df_all, df])


    # print(df_all)

    for variant in variants:
        tmp_df = df_all[df.name == variant]
        # print(tmp_df)
        fig.add_trace(
            go.Scatter(
                x=tmp_df["threads"],
                y=tmp_df["GFLOPS"],
                text=[format(x, ".3f")
                      for x in tmp_df["GFLOPS"]],
                name=variant
                )
            )

    fig.update_layout(
            height=400,
            margin=dict(r=5, l=5, t=30, b=5),
            plot_bgcolor="rgba(0,0,0,0)",
            yaxis_gridcolor="LightGray",
            xaxis_gridcolor="LightGray",
            title="one core per thread",
            legend_title="kernels",
            legend=dict(
                x=0.06,
                y=1,
                ),
            xaxis=go.layout.XAxis(
                title="thread count",
                gridcolor="LightGray",
                ),
            yaxis=go.layout.YAxis(
                title="GFLOPS",
                gridcolor="LightGray",
                ),
            )
    fig.write_image("./plots/asimd.pdf")
    fig.write_image("./plots/asimd.png")


if __name__ == '__main__':
    main()
