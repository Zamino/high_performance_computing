# 01

## Hardware Information

```
$ lscpu
Architecture:                    aarch64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
CPU(s):                          4
On-line CPU(s) list:             0-3
Vendor ID:                       ARM
Model name:                      Neoverse-V1
Model:                           1
Thread(s) per core:              1
Core(s) per socket:              4
Socket(s):                       1
Stepping:                        r1p1
BogoMIPS:                        2100.00
Flags:                           fp asimd evtstrm aes pmull sha1 sha2 crc32
atomics fphp asimdhp cpuid asimdrdm jscvt fcma lrcpc dcpop sha3 sm3 sm4 asimddp
sha512 sve asimdfhm dit uscat ilrcpc flagm ssbs paca pacg dcpodp svei8mm svebf16
i8mm bf16 dgh rng
L1d cache:                       256 KiB (4 instances)
L1i cache:                       256 KiB (4 instances)
L2 cache:                        4 MiB (4 instances)
L3 cache:                        32 MiB (1 instance)
NUMA node(s):                    1
NUMA node0 CPU(s):               0-3
Vulnerability Itlb multihit:     Not affected
Vulnerability L1tf:              Not affected
Vulnerability Mds:               Not affected
Vulnerability Meltdown:          Not affected
Vulnerability Mmio stale data:   Not affected
Vulnerability Retbleed:          Not affected
Vulnerability Spec store bypass: Mitigation; Speculative Store Bypass disabled
via prctl
Vulnerability Spectre v1:        Mitigation; __user pointer sanitization
Vulnerability Spectre v2:        Mitigation; CSV2, BHB
Vulnerability Srbds:             Not affected
Vulnerability Tsx async abort:   Not affected

$ lscpu -C
NAME ONE-SIZE ALL-SIZE WAYS TYPE        LEVEL  SETS PHY-LINE COHERENCY-SIZE
L1d       64K     256K    4 Data            1   256                      64
L1i       64K     256K    4 Instruction     1   256                      64
L2         1M       4M    8 Unified         2  2048                      64
L3        32M      32M   16 Unified         3 32768                      64

$ lstopo-no-graphics
Machine (7760MB total)
  Package L#0
    NUMANode L#0 (P#0 7760MB)
    L3 L#0 (32MB)
      L2 L#0 (1024KB) + L1d L#0 (64KB) + L1i L#0 (64KB) + Core L#0 + PU L#0 (P#0)
      L2 L#1 (1024KB) + L1d L#1 (64KB) + L1i L#1 (64KB) + Core L#1 + PU L#1 (P#1)
      L2 L#2 (1024KB) + L1d L#2 (64KB) + L1i L#2 (64KB) + Core L#2 + PU L#2 (P#2)
      L2 L#3 (1024KB) + L1d L#3 (64KB) + L1i L#3 (64KB) + Core L#3 + PU L#3 (P#3)
  HostBridge
    PCI 00:04.0 (NVMExp)
      Block(Disk) "nvme0n1"
    PCI 00:05.0 (Ethernet)
      Net "eth0"
```

### Theoretical Peak Performance

**`fmla`**:\
$takt\_frequenzy = 2600MHz = 2.6GTakt/s$\
$cores = 4$\
$execution\_throughput = 4OP/Takt$\
$FLOP\_per\_OP\_sp = 2 * 4FLOP/OP = 8FLOP/OP$\
$FLOP\_per\_OP\_dp = 2 * 2FLOP/OP = 4FLOP/OP$

$theoretical\_peak\_performance = takt\_frequenzy * cores * execution\_throughput * FLOP\_per\_OP$

$theoretical\_peak\_performance\_sp = 2.6GTakt/s * 4 * 4OP/Takt * 8FLOP/OP = 332.8GFLOPS$

$theoretical\_peak\_performance\_dp = 2.6GTakt/s * 4 * 4OP/Takt * 4FLOP/OP = 166.4GFLOPS$

## Microbenchmark

### Test

```
$ cd ./aarch64_micro
$ make
gcc -pedantic -Wall -Wextra -Werror -c kernels/peak_asimd_fmla_sp.s -o ./build/peak_asimd_fmla_sp.o
g++ -pedantic -Wall -Wextra -Werror -O2 -fopenmp driver_asimd.cpp ./build/peak_asimd_fmla_sp.o -o ./build/micro_asimd
$ ./build/micro_asimd
running ASIMD microbenchmarks
  threads: 4
peak_asimd_fmla_sp
  duration: 25.5733 seconds
  GFLOPS: 187.696
finished ASIMD microbenchmarks
$ cd ..
```

### Code

- **Driver:** `./aarch64_micro/driver_asimd.cpp`
- **Kernels:**
    - `./aarch64_micro/kernels/peak_asimd_fmla_sp.s`
    - `./aarch64_micro/kernels/peak_asimd_fmla_dp.s`
    - `./aarch64_micro/kernels/peak_asimd_fmadd_sp.s`
    - `./aarch64_micro/kernels/peak_asimd_fmadd_dp.s`
- **Makefile:** `./aarch64_micro/Makefile`

```
$ cd ./aarch64_micro
$ make
$ cd ..
```

### Experiment

`[i]` has been incremented from 1 to 4
```
$ OMP_NUM_THEADS=[i] ./aarch64_micro/build/micro_asimd > ./benchmark_results/threads_[i].csv
$ OMP_PLACES={0} OMP_NUM_THEADS=[i] ./aarch64_micro/build/micro_asimd > ./benchmark_results/pinned_threads_[i].csv
```

### Result

```
$ python ./create_plot_asimd.py
```
![asmid](plots/asimd.png)


```
$ python ./create_plot_pinned_asimd.py
```
![pinned_asmid](plots/pinned_asimd.png)


