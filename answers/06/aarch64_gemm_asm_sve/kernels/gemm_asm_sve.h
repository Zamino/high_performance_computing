#include <cstdint>

extern "C" {

  void gemm_asm_sve_32_6_1( float const * i_a,
                            float const * i_b,
                            float       * io_c );

  void gemm_asm_sve_32_6_48( float const * i_a,
                             float const * i_b,
                             float       * io_c );

  void gemm_asm_sve_128_6_48( float const * i_a,
                              float const * i_b,
                              float       * io_c );

  void gemm_asm_sve_128_48_48( float const * i_a,
                               float const * i_b,
                               float       * io_c );

  // void gemm_asm_sve(
  //   float*  const i_a,   float*   const i_b,   float*  io_c, // data
  //   int64_t       i_m,   int64_t        i_n,   int64_t i_k,  // dimensions
  //   int64_t       i_lda, int64_t        i_ldb, int64_t i_ldc // data_row lengths
  //     );

}
