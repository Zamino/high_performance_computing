        .text
        .type gemm_asm_sve_32_6_1, %function
        .global gemm_asm_sve_32_6_1
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (32x6) = (32x1) * (1x6).
         * The input-data is of type float.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */
gemm_asm_sve_32_6_1:
        // store
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        ptrue p0.s

#define i_A x0
#define i_B x1
#define i_C x2

        // set register for dimensions of M, N and K
#define i_m x3
        mov i_m, #32
#define i_n x4
        mov i_n, #1
#define i_k x5
        mov i_k, #6

        // set register for leading dimensions of A, B and C
#define i_lda x6
        mov i_lda, #32
#define i_ldb x7
        mov i_ldb, #1
#define i_ldc x8
        mov i_ldc, #32

        // set register for m, n and k
#define m x9
        mov m, #0
#define n x10
        mov n, #0
#define k x11
        mov k, #0

        // mov x12, #0
        // incp x12, p0.s // get count of 32 bits elements in vector register

// unrole part -----------------------------------------------------------------

        // Load C
        ld1w { z0.s,  z1.s,  z2.s,  z3.s}, p6/z, [x2], #128
        ld1w { z4.s,  z5.s,  z6.s,  z7.s}, p6/z, [x2], #128
        ld1w { z8.s,  z9.s, z10.s, z11.s}, p6/z, [x2], #128
        ld1w {z12.s, z13.s, z14.s, z15.s}, p6/z, [x2], #128
        ld1w {z16.s, z17.s, z18.s, z19.s}, p6/z, [x2], #128
        ld1w {z20.s, z21.s, z22.s, z23.s}, p6/z, [x2], #128

        sub x2, x2, #32*6*4

        // Load A
        ld1w {z24.s, z25.s, z26.s, z27.s}, p6/z [x0], #128

        // Load B
        //ld1 {v29.4s, v30.2s}, [x1]
        ld1 {v29.4s}, [x1], #16
        ld1 {v30.2s}, [x1], #8

        fmla  v1.4s, v25.4s, v29.s[0]
        fmla  v2.4s, v26.4s, v29.s[0]
        fmla  v3.4s, v27.4s, v29.s[0]
        fmla  v4.4s, v28.4s, v29.s[0]

        fmla  v5.4s, v25.4s, v29.s[1]
        fmla  v6.4s, v26.4s, v29.s[1]
        fmla  v7.4s, v27.4s, v29.s[1]
        fmla  v8.4s, v28.4s, v29.s[1]

        fmla  v9.4s, v25.4s, v29.s[2]
        fmla v10.4s, v26.4s, v29.s[2]
        fmla v11.4s, v27.4s, v29.s[2]
        fmla v12.4s, v28.4s, v29.s[2]

        fmla v13.4s, v25.4s, v29.s[3]
        fmla v14.4s, v26.4s, v29.s[3]
        fmla v15.4s, v27.4s, v29.s[3]
        fmla v16.4s, v28.4s, v29.s[3]

        fmla v17.4s, v25.4s, v30.s[0]
        fmla v18.4s, v26.4s, v30.s[0]
        fmla v19.4s, v27.4s, v30.s[0]
        fmla v20.4s, v28.4s, v30.s[0]

        fmla v21.4s, v25.4s, v30.s[1]
        fmla v22.4s, v26.4s, v30.s[1]
        fmla v23.4s, v27.4s, v30.s[1]
        fmla v24.4s, v28.4s, v30.s[1]

        // Store C
        st1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        st1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        st1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        st1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        st1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        st1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64


// rest part -------------------------------------------------------------------
// whilele

        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ret
        .size gemm_asm_sve_32_6_1, (. - gemm_asm_sve_32_6_1)
