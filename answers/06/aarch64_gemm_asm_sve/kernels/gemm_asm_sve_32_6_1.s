        .text
        .type gemm_asm_sve_32_6_1, %function
        .global gemm_asm_sve_32_6_1
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (32x6) = (32x1) * (1x6).
         * The input-data is of type float.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */
gemm_asm_sve_32_6_1:
        // set bits for 32 bits view of predicate register p0 to 1
        ptrue p0.s

        // store
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        // Load C
        ld4w { z0.s,  z1.s,  z2.s,  z3.s}, p0/z, [x2, #0 , MUL VL]
        ld4w { z4.s,  z5.s,  z6.s,  z7.s}, p0/z, [x2, #4 , MUL VL]
        ld4w { z8.s,  z9.s, z10.s, z11.s}, p0/z, [x2, #8 , MUL VL]
        ld4w {z12.s, z13.s, z14.s, z15.s}, p0/z, [x2, #12, MUL VL]
        ld4w {z16.s, z17.s, z18.s, z19.s}, p0/z, [x2, #16, MUL VL]
        ld4w {z20.s, z21.s, z22.s, z23.s}, p0/z, [x2, #20, MUL VL]

        // Load A
        ld4w {z24.s, z25.s, z26.s, z27.s}, p0/z, [x0]

        // Load B
        ld1rw {z28.s}, p0/z, [x1, #0]
        ld1rw {z29.s}, p0/z, [x1, #4]
        ld1rw {z30.s}, p0/z, [x1, #8]

        fmla  z0.s, p0/m, z24.s, z28.s
        fmla  z1.s, p0/m, z25.s, z28.s
        fmla  z2.s, p0/m, z26.s, z28.s
        fmla  z3.s, p0/m, z27.s, z28.s

        fmla  z4.s, p0/m, z24.s, z29.s
        fmla  z5.s, p0/m, z25.s, z29.s
        fmla  z6.s, p0/m, z26.s, z29.s
        fmla  z7.s, p0/m, z27.s, z29.s

        fmla  z8.s, p0/m, z24.s, z30.s
        fmla  z9.s, p0/m, z25.s, z30.s
        fmla z10.s, p0/m, z26.s, z30.s
        fmla z11.s, p0/m, z27.s, z30.s

        // Load B
        ld1rw {z28.s}, p0/z, [x1, #12]
        ld1rw {z29.s}, p0/z, [x1, #16]
        ld1rw {z30.s}, p0/z, [x1, #20]

        fmla z12.s, p0/m, z24.s, z28.s
        fmla z13.s, p0/m, z25.s, z28.s
        fmla z14.s, p0/m, z26.s, z28.s
        fmla z15.s, p0/m, z27.s, z28.s

        fmla z16.s, p0/m, z24.s, z29.s
        fmla z17.s, p0/m, z25.s, z29.s
        fmla z18.s, p0/m, z26.s, z29.s
        fmla z19.s, p0/m, z27.s, z29.s

        fmla z20.s, p0/m, z24.s, z30.s
        fmla z21.s, p0/m, z25.s, z30.s
        fmla z22.s, p0/m, z26.s, z30.s
        fmla z23.s, p0/m, z27.s, z30.s

        // Store C
        st4w { z0.s,  z1.s,  z2.s,  z3.s}, p0, [x2, #0 , MUL VL]
        st4w { z4.s,  z5.s,  z6.s,  z7.s}, p0, [x2, #4 , MUL VL]
        st4w { z8.s,  z9.s, z10.s, z11.s}, p0, [x2, #8 , MUL VL]
        st4w {z12.s, z13.s, z14.s, z15.s}, p0, [x2, #12, MUL VL]
        st4w {z16.s, z17.s, z18.s, z19.s}, p0, [x2, #16, MUL VL]
        st4w {z20.s, z21.s, z22.s, z23.s}, p0, [x2, #20, MUL VL]

        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ret
        .size gemm_asm_sve_32_6_1, (. - gemm_asm_sve_32_6_1)
