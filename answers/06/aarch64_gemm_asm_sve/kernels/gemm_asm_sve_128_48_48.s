        .text
        .type gemm_asm_sve_128_48_48, %function
        .global gemm_asm_sve_128_48_48
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (32x6) = (32x1) * (1x6).
         * The input-data is of type float.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */
gemm_asm_sve_128_48_48:
        // set bits for 32 bits view of predicate register p0 to 1
        ptrue p0.s

        // i_m
        mov x3, #128
        // i_n
        mov x4, #48
        // i_k
        mov x5, #48

        // block size A (4*m*k)
        mov x6, #4
        mul x6, x6, x3
        mul x6, x6, x5

        // block size B (4*k*n)
        mov x7, #4
        mul x7, x7, x5
        mul x7, x7, x4

        // block size C (4*m*n)
        mov x8, #4
        mul x8, x8, x3
        mul x8, x8, x4

        // store
        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        cbz x3, end
        cbz x5, end

        // set m
        mov x9, #4
loop_m:
        sub x9, x9, #1


        // set n
        mov x10, #8
loop_n:
        sub x10, x10, #1

        // Load C
        ld4w { z0.s,  z1.s,  z2.s,  z3.s}, p0/z, [x2]
        add x2, x2, #128*4
        ld4w { z4.s,  z5.s,  z6.s,  z7.s}, p0/z, [x2]
        add x2, x2, #128*4
        ld4w { z8.s,  z9.s, z10.s, z11.s}, p0/z, [x2]
        add x2, x2, #128*4
        ld4w {z12.s, z13.s, z14.s, z15.s}, p0/z, [x2]
        add x2, x2, #128*4
        ld4w {z16.s, z17.s, z18.s, z19.s}, p0/z, [x2]
        add x2, x2, #128*4
        ld4w {z20.s, z21.s, z22.s, z23.s}, p0/z, [x2]
        sub x2, x2, #5*128*4 //undo addition

        // set k
        mov x11, x5
loop_k:
        sub x11, x11, #1
        // Load A
        ld4w {z24.s, z25.s, z26.s, z27.s}, p0/z, [x0]
        add x0, x0, #128*4 // next colum A

        // Load B
        ld1rw {z28.s}, p0/z, [x1]
        add x1, x1, #48*4
        ld1rw {z29.s}, p0/z, [x1]
        add x1, x1, #48*4
        ld1rw {z30.s}, p0/z, [x1]
        add x1, x1, #48*4

        fmla  z0.s, p0/m, z24.s, z28.s
        fmla  z1.s, p0/m, z25.s, z28.s
        fmla  z2.s, p0/m, z26.s, z28.s
        fmla  z3.s, p0/m, z27.s, z28.s

        fmla  z4.s, p0/m, z24.s, z29.s
        fmla  z5.s, p0/m, z25.s, z29.s
        fmla  z6.s, p0/m, z26.s, z29.s
        fmla  z7.s, p0/m, z27.s, z29.s

        fmla  z8.s, p0/m, z24.s, z30.s
        fmla  z9.s, p0/m, z25.s, z30.s
        fmla z10.s, p0/m, z26.s, z30.s
        fmla z11.s, p0/m, z27.s, z30.s

        // Load B
        ld1rw {z28.s}, p0/z, [x1]
        add x1, x1, #48*4
        ld1rw {z29.s}, p0/z, [x1]
        add x1, x1, #48*4
        ld1rw {z30.s}, p0/z, [x1]
        sub x1, x1, #5*48*4 // reset

        add x1, x1, #4 // next row B

        fmla z12.s, p0/m, z24.s, z28.s
        fmla z13.s, p0/m, z25.s, z28.s
        fmla z14.s, p0/m, z26.s, z28.s
        fmla z15.s, p0/m, z27.s, z28.s

        fmla z16.s, p0/m, z24.s, z29.s
        fmla z17.s, p0/m, z25.s, z29.s
        fmla z18.s, p0/m, z26.s, z29.s
        fmla z19.s, p0/m, z27.s, z29.s

        fmla z20.s, p0/m, z24.s, z30.s
        fmla z21.s, p0/m, z25.s, z30.s
        fmla z22.s, p0/m, z26.s, z30.s
        fmla z23.s, p0/m, z27.s, z30.s

        cbnz x11, loop_k
// end loop_k

        // Store C
        st4w { z0.s,  z1.s,  z2.s,  z3.s}, p0, [x2]
        add x2, x2, #128*4
        st4w { z4.s,  z5.s,  z6.s,  z7.s}, p0, [x2]
        add x2, x2, #128*4
        st4w { z8.s,  z9.s, z10.s, z11.s}, p0, [x2]
        add x2, x2, #128*4
        st4w {z12.s, z13.s, z14.s, z15.s}, p0, [x2]
        add x2, x2, #128*4
        st4w {z16.s, z17.s, z18.s, z19.s}, p0, [x2]
        add x2, x2, #128*4
        st4w {z20.s, z21.s, z22.s, z23.s}, p0, [x2]
        sub x2, x2, #5*128*4 // undo addition

        // next block C
        add x2, x2, #6*128*4
        // next block A
        sub x0, x0, x6 // reset
        // next block B
        add x1, x1, #5*48*4

        cbnz x10, loop_n
// end loop_n

        // next block C
        sub x2, x2, x8 // reset
        add x2, x2, #32*4
        // next block A
        add x0, x0, #32*4
        // next block B
        sub x1, x1, x7 // reset

        cbnz x9, loop_m
// end loop_m

end:
        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ret
        .size gemm_asm_sve_128_48_48, (. - gemm_asm_sve_128_48_48)
