#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <stdint.h>
#include <iostream>

#include "kernels/gemm_ref.cpp"
#include "kernels/gemm_asm_sve.h"

void create_matrices(
        float** a, float** b, float** c,
        int64_t m, int64_t n, int64_t k
        ) {
  *a = new float[k * m];
  *b = new float[n * k];
  *c = new float[n * m];
}

void fill_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda,
    float value
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = value;
    }
  }
}

void fill_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = (m * i_n + n) / 100.0;
    }
  }
}

void print_matrix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      std::cout << a[m + i_lda * n] << ", ";
    }
    std::cout << std::endl;
    if (m % 32 == 31)
      std::cout << std::endl;
  }
  std::cout << std::endl;
}

void copy_matrix(
    float* a, float* b,
    int64_t i_m, int64_t i_n,
    int64_t i_lda
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = b[m + i_lda * n];
    }
  }
}

void compare_matrices(
    float* a, float* b,
    int64_t i_m, int64_t i_n,
    int64_t i_lda, int64_t i_ldb,
    int precision
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      REQUIRE_THAT(a[m + i_lda * n],
                   Catch::Matchers::WithinULP(b[m + i_lda * n], precision));
    }
  }
}

void delete_matrices(float* a, float* b, float* c) {
    delete[] a;
    delete[] b;
    delete[] c;
}

TEST_CASE( "gemm_ref 32 32 32 [0] += [1] * [1]", "[gemm_ref]" ) {
  int64_t m = 32; int64_t n = 32; int64_t k = 32;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matrix(a, m, k, m, 1);
  fill_matrix(b, k, n, k, 1);
  fill_matrix(c, m, n, m, 0);

  gemm_ref(
      a, b, c,
      m, n, k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matrix(
      c_result,
      m, n,
      m,
      k
      );

  compare_matrices(
      c, c_result,
      m, n,
      m, m,
      3
      );

  delete_matrices(a, b, c);
  delete[] c_result;
}

TEST_CASE( "gemm_ref 32 32 32 [0.3] += [0.3] * [0.3]", "[gemm_ref]" ) {
  int64_t m = 32; int64_t n = 32; int64_t k = 32;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matrix(a, m, k, m, 0.3);
  fill_matrix(b, k, n, k, 0.3);
  fill_matrix(c, m, n, m, 0.3);

  gemm_ref(
      a, b, c,
      m, n, k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matrix(
      c_result,
      m, n,
      m,
      0.3 + k * (0.3 * 0.3)
      );

  compare_matrices(
      c, c_result,
      m, n,
      m, m,
      3
      );

  delete_matrices(a, b, c);
  delete[] c_result;
}

TEST_CASE( "gemm_ref 64 64 64 [0] += [1] * [1]", "[gemm_ref]" ) {
  int64_t m = 64; int64_t n = 64; int64_t k = 64;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matrix(a, m, k, m, 1);
  fill_matrix(b, k, n, k, 1);
  fill_matrix(c, m, n, m, 0);

  gemm_ref(
      a, b, c,
      m, n, k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matrix(
      c_result,
      m, n,
      m,
      k
      );

  compare_matrices(
      c, c_result,
      m, n,
      m, m,
      3
      );

  delete_matrices(a, b, c);
  delete[] c_result;
}

TEST_CASE( "gemm_ref 64 64 64 [0.3] += [0.3] * [0.3]", "[gemm_ref]" ) {
  int64_t m = 64; int64_t n = 64; int64_t k = 64;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matrix(a, m, k, m, 0.3);
  fill_matrix(b, k, n, k, 0.3);
  fill_matrix(c, m, n, m, 0.3);

  gemm_ref(
      a, b, c,
      m, n, k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matrix(
      c_result,
      m, n,
      m,
      0.3 + k * (0.3 * 0.3)
      );

  compare_matrices(
      c, c_result,
      m, n,
      m, m,
      4
      );

  delete_matrices(a, b, c);
  delete[] c_result;
}

TEST_CASE( "gemm_ref 32 32 32 64 64 64 [0] += [1] * [1]", "[gemm_ref]" ) {
  int64_t   m = 64; int64_t   n = 64; int64_t   k = 64;
  int64_t i_m = 32; int64_t i_n = 32; int64_t i_k = 32;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matrix(a, m, k, m, 1);
  fill_matrix(b, k, n, k, 1);
  fill_matrix(c, m, n, m, 0);

  gemm_ref(
      a, b, c,
      i_m, i_n, i_k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matrix(
      c_result,
      m, n,
      m,
      i_k
      );

  compare_matrices(
      c, c_result,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(a, b, c);
  delete[] c_result;
}


TEST_CASE( "gemm_ref 4 16 32 64 64 64 [0] += [1] * [1]", "[gemm_ref]" ) {
  int64_t   m = 64; int64_t   n = 64; int64_t   k = 64;
  int64_t i_m =  4; int64_t i_n = 16; int64_t i_k = 16;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matrix(a, m, k, m, 1);
  fill_matrix(b, k, n, k, 1);
  fill_matrix(c, m, n, m, 0);

  gemm_ref(
      a, b, c,
      i_m, i_n, i_k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matrix(
      c_result,
      m, n,
      m,
      i_k
      );

  compare_matrices(
      c, c_result,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(a, b, c);
  delete[] c_result;
}

TEST_CASE( "gemm_asm_sve_32_6_1", "[gemm_asm_sve_32_6_1]") {
  int64_t   m = 32; int64_t   n = 6; int64_t   k = 1;
  int64_t i_m = 32; int64_t i_n = 6; int64_t i_k = 1;
  float *a, *b, *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  fill_matrix(a, m, k, m);
  fill_matrix(b, k, n, k);
  fill_matrix(c_ref, m, n, m);
  float* c_asimd = new float[m * n];
  copy_matrix(
      c_asimd, c_ref,
      m, n,
      m
      );

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  gemm_asm_sve_32_6_1(a, b, c_asimd);
  // print_matrix(c_ref, m, n, m);
  // print_matrix(c_asimd, m, n, m);

  compare_matrices(
      c_ref, c_asimd,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(a, b, c_ref);
  delete[] c_asimd;
}

TEST_CASE( "gemm_asm_sve_32_6_48", "[gemm_asm_sve_32_6_48]") {
  int64_t   m = 32; int64_t   n = 6; int64_t   k = 48;
  int64_t i_m = 32; int64_t i_n = 6; int64_t i_k = 48;
  float *a, *b, *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  fill_matrix(a, m, k, m);
  // print_matrix(a, m, k, m);
  fill_matrix(b, k, n, k);
  // print_matrix(b, k, n, k);
  fill_matrix(c_ref, m, n, m);
  float* c_asimd = new float[m * n];
  copy_matrix(
      c_asimd, c_ref,
      m, n,
      m
      );
  // print_matrix(c_asimd, m, n, m);

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  gemm_asm_sve_32_6_48(a, b, c_asimd);

  compare_matrices(
      c_ref, c_asimd,
      i_m, i_n,
      m, m,
      4
      );

  delete_matrices(a, b, c_ref);
  delete[] c_asimd;
}

TEST_CASE( "gemm_asm_sve_128_6_48", "[gemm_asm_sve_128_6_48]") {
  int64_t   m = 128; int64_t   n = 6; int64_t   k = 48;
  int64_t i_m = 128; int64_t i_n = 6; int64_t i_k = 48;
  float *a, *b, *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  fill_matrix(a, m, k, m);
  // print_matrix(a, m, k, m);
  fill_matrix(b, k, n, k);
  // print_matrix(b, k, n, k);
  fill_matrix(c_ref, m, n, m);
  float* c_asimd = new float[m * n];
  copy_matrix(
      c_asimd, c_ref,
      m, n,
      m
      );

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  gemm_asm_sve_128_6_48(a, b, c_asimd);

  // print_matrix(c_ref, m, n, m);
  // print_matrix(c_asimd, m, n, m);

  compare_matrices(
      c_ref, c_asimd,
      i_m, i_n,
      m, m,
      5
      );

  delete_matrices(a, b, c_ref);
  delete[] c_asimd;
}

TEST_CASE( "gemm_asm_sve_128_48_48", "[gemm_asm_sve_128_48_48]") {
  int64_t   m = 128; int64_t   n = 48; int64_t   k = 48;
  int64_t i_m = 128; int64_t i_n = 48; int64_t i_k = 48;
  float *a, *b, *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  fill_matrix(a, m, k, m);
  // print_matrix(a, m, k, m);
  fill_matrix(b, k, n, k);
  print_matrix(b, k, n, k);
  fill_matrix(c_ref, m, n, m);
  float* c_asimd = new float[m * n];
  copy_matrix(
      c_asimd, c_ref,
      m, n,
      m
      );

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  gemm_asm_sve_128_48_48(a, b, c_asimd);

  print_matrix(c_ref, m, n, m);
  print_matrix(c_asimd, m, n, m);

  compare_matrices(
      c_ref, c_asimd,
      i_m, i_n,
      m, m,
      5
      );

  delete_matrices(a, b, c_ref);
  delete[] c_asimd;
}
