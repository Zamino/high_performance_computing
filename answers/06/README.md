# 06

## 7. Small GEMMs: SVE

| Team         | program                | Time (s) | #execution | GFLOPS  | %peak |
|--------------|------------------------|----------|------------|---------|-------|
| peak climber | gemm_asm_sve_32_6_1    | 1.66723  | 50000000   | 11.5161 | 18    |
| peak climber | gemm_asm_sve_32_6_48   | 2.12279  | 5000000    | 43.4145 | 67.8  |
| peak climber | gemm_asm_sve_128_6_48  | 3.6474   | 2000000    | 40.4277 | 63.2  |
| peak climber | gemm_asm_sve_128_48_48 | 2.82386  | 200000     | 41.7743 | 65.3  |
