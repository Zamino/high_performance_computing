cmake_minimum_required(VERSION 3.9 FATAL_ERROR)
project(aarch64_gemm LANGUAGES CXX C ASM)

include(libs/catch2.cmake)

add_executable(aarch64_gemm_test tests.cpp kernels/gemm_asm_asimd.h kernels/gemm_asm_asimd_16_6_1.s kernels/gemm_asm_asimd_16_6_48.s)
target_link_libraries(aarch64_gemm_test PRIVATE Catch2::Catch2WithMain)

add_executable(aarch64_gemm_micro driver.cpp kernels/gemm_asm_asimd.h kernels/gemm_asm_asimd_16_6_1.s kernels/gemm_asm_asimd_16_6_48.s)
# Add OpenMP support
# find_package(OpenMP REQUIRED)
# target_link_libraries(aarch64_gemm_micro PRIVATE OpenMP::OpenMP_CXX)

# default to Release build type
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release")
endif()
