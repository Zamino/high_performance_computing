        .text
        .type gemm_asm_asimd_16_6_1, %function
        .global gemm_asm_asimd_16_6_1
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (16x6) = (16x1) * (1x6).
         * The input-data is of type float.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */ 
gemm_asm_asimd_16_6_1:
        // Load C
        ld1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        ld1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        ld1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        ld1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        ld1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        ld1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64

        sub x2, x2, #16*6*4

        // Load A
        ld1 {v25.4s, v26.4s, v27.4s, v28.4s}, [x0], #64

        // Load B
        //ld1 {v29.4s, v30.2s}, [x1]
        ld1 {v29.4s}, [x1], #16
        ld1 {v30.2s}, [x1], #8

        fmla  v1.4s, v25.4s, v29.s[0]
        fmla  v2.4s, v26.4s, v29.s[0]
        fmla  v3.4s, v27.4s, v29.s[0]
        fmla  v4.4s, v28.4s, v29.s[0]

        st1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64

        fmla  v5.4s, v25.4s, v29.s[1]
        fmla  v6.4s, v26.4s, v29.s[1]
        fmla  v7.4s, v27.4s, v29.s[1]
        fmla  v8.4s, v28.4s, v29.s[1]

        st1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64

        fmla  v9.4s, v25.4s, v29.s[2]
        fmla v10.4s, v26.4s, v29.s[2]
        fmla v11.4s, v27.4s, v29.s[2]
        fmla v12.4s, v28.4s, v29.s[2]

        st1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64

        fmla v13.4s, v25.4s, v29.s[3]
        fmla v14.4s, v26.4s, v29.s[3]
        fmla v15.4s, v27.4s, v29.s[3]
        fmla v16.4s, v28.4s, v29.s[3]

        st1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64

        fmla v17.4s, v25.4s, v30.s[0]
        fmla v18.4s, v26.4s, v30.s[0]
        fmla v19.4s, v27.4s, v30.s[0]
        fmla v20.4s, v28.4s, v30.s[0]

        st1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64

        fmla v21.4s, v25.4s, v30.s[1]
        fmla v22.4s, v26.4s, v30.s[1]
        fmla v23.4s, v27.4s, v30.s[1]
        fmla v24.4s, v28.4s, v30.s[1]

        // Store C
        st1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64

        ret
        .size gemm_asm_asimd_16_6_1, (. - gemm_asm_asimd_16_6_1)
