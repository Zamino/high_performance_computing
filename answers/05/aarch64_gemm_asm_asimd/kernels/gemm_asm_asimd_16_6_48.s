        .text
        .type gemm_asm_asimd_16_6_48, %function
        .global gemm_asm_asimd_16_6_48
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (16x6) = (16x1) * (1x6).
         * The input-data is of type float.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */ 
gemm_asm_asimd_16_6_48:
        // Load C
        ld1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        ld1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        ld1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        ld1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        ld1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        ld1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64
        sub x2, x2, #64*6

        // set count for k_loop
        mov x4, #48

k_loop:
        cbz x4, end
        sub x4, x4, #1

        // Load A
        ld1 {v25.4s, v26.4s, v27.4s, v28.4s}, [x0], #64


        // Load B
        ldr s29, [x1], #48*4
        ldr s30, [x1], #48*4
        ldr s31, [x1], #48*4

        fmla  v1.4s, v25.4s, v29.s[0]
        fmla  v2.4s, v26.4s, v29.s[0]
        fmla  v3.4s, v27.4s, v29.s[0]
        fmla  v4.4s, v28.4s, v29.s[0]

        fmla  v5.4s, v25.4s, v30.s[0]
        fmla  v6.4s, v26.4s, v30.s[0]
        fmla  v7.4s, v27.4s, v30.s[0]
        fmla  v8.4s, v28.4s, v30.s[0]

        fmla  v9.4s, v25.4s, v31.s[0]
        fmla v10.4s, v26.4s, v31.s[0]
        fmla v11.4s, v27.4s, v31.s[0]
        fmla v12.4s, v28.4s, v31.s[0]

        // Load B
        ldr s29, [x1], #48*4
        ldr s30, [x1], #48*4
        ldr s31, [x1], #48*4

        // next line
        add x1, x1, #-6*48*4+4

        fmla v13.4s, v25.4s, v29.s[0]
        fmla v14.4s, v26.4s, v29.s[0]
        fmla v15.4s, v27.4s, v29.s[0]
        fmla v16.4s, v28.4s, v29.s[0]

        fmla v17.4s, v25.4s, v30.s[0]
        fmla v18.4s, v26.4s, v30.s[0]
        fmla v19.4s, v27.4s, v30.s[0]
        fmla v20.4s, v28.4s, v30.s[0]

        fmla v21.4s, v25.4s, v31.s[0]
        fmla v22.4s, v26.4s, v31.s[0]
        fmla v23.4s, v27.4s, v31.s[0]
        fmla v24.4s, v28.4s, v31.s[0]

        // repeat loop
        b k_loop

end:
        // Store C
        st1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        st1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        st1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        st1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        st1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        st1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64

        ret
        .size gemm_asm_asimd_16_6_48, (. - gemm_asm_asimd_16_6_48)
