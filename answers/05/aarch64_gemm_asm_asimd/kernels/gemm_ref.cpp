#include <cstdint>

void gemm_ref( // C^MxN += A^MxK * B^KxN
    float*  const i_a,   float*   const i_b,   float*  io_c, // data
    int64_t       i_m,   int64_t        i_n,   int64_t i_k,  // dimensions
    int64_t       i_lda, int64_t        i_ldb, int64_t i_ldc // data_row lengths
    ) {
  for (int m=0; m<i_m; m++) {
    for (int n=0; n<i_n; n++) {
      for (int k=0; k<i_k; k++) {
        io_c[m + n * i_ldc] += i_a[m + k * i_lda] * i_b[k + n * i_ldb];
      }
    }
  }
}
