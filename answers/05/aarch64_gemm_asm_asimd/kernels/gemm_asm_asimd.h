extern "C" {
    void gemm_asm_asimd_16_6_1( float const * i_a,
                                float const * i_b,
                                float       * io_c );

    void gemm_asm_asimd_16_6_48( float const * i_a,
                                 float const * i_b,
                                 float       * io_c );
}
