# 05

## 6.1. Conditions and Loops

### 1.

- **high_lvl_0**: Gets an input value and returns it.
- **high_lvl_1**: Gets an input value and returns 0.
- **high_lvl_2**: Gets an input value and returns 1 if the value is less than
  32, 0 otherwise.
- **high_lvl_3**: Gets two input pointers and writes 1 to the address of the
  second pointer if the value after the first is less than 32, otherwise 0.
- **high_lvl_4**: Gets three input values and returns 1 if the first is the
  smallest, 2 if the second is the smallest, otherwise 3.
- **high_lvl_5**: Gets an input value and a pointer, and increments the value
  behind the pointer by the multiple of the value.
- **high_lvl_6**: Gets two input values and a pointer, and increments the value
  behind the pointer by the second value at least once and at most by the amount
  of the first value.
- **high_lvl_7**: Gets one input value and two pointers and copies as many
  elements as the amount of the value from the first pointer address to the
  second.

### 2.

The code is in the folder `./aarch64_low_level/`

`kernels/low_level.h`
```cpp
#include <cstdint>


extern "C" {
  int32_t low_lvl_0( int32_t i_value );

  uint64_t low_lvl_1( uint64_t );

  int32_t low_lvl_2( int32_t i_option );

  void low_lvl_3( int32_t * i_option,
                  int32_t * o_result );

  uint32_t low_lvl_4( uint32_t i_x,
                      uint32_t i_y,
                      uint32_t i_z );

  void low_lvl_5( uint32_t   i_nIters,
                  int32_t  * io_value );

  void low_lvl_6( uint64_t   i_nIters,
                  int64_t    i_inc,
                  int64_t  * io_value );

  void low_lvl_7( uint64_t   i_nValues,
                  int64_t  * i_valuesIn,
                  int64_t  * i_valuesOut );
}
```

`kernels/low_lvl_0.s`
```assembly
        .text
        .type   low_lvl_0, %function
        .global low_lvl_0
low_lvl_0:
        ret
        .size low_lvl_0, (. - low_lvl_0)
```

`kernels/low_lvl_1.s`
```assembly
        .text
        .type   low_lvl_1, %function
        .global low_lvl_1
low_lvl_1:
        mov x0, xzr
        ret
        .size low_lvl_1, (. - low_lvl_1)
```

`kernels/low_lvl_2.s`
```assembly
        .text
        .type   low_lvl_2, %function
        .global low_lvl_2
low_lvl_2:
        cmp w0, #32
        B.LT less_than
        mov w0, wzr
end:
        ret
less_than:
        mov w0, #1
        b end
        .size low_lvl_2, (. - low_lvl_2)
```

`kernels/low_lvl_3.s`
```assembly
        .text
        .type   low_lvl_3, %function
        .global low_lvl_3
low_lvl_3:
        ldr w2, [x0]
        cmp w2, #25
        B.LT less_than
        mov w3, wzr
end:
        str w3, [x1]
        ret
less_than:
        mov w3, #1
        b end
        .size low_lvl_3, (. - low_lvl_3)
```

`kernels/low_lvl_4.s`
```assembly
        .text
        .type   low_lvl_4, %function
        .global low_lvl_4
low_lvl_4:
        cmp w0, w1
        B.GE w0_not_smallest
        cmp w0, w2
        B.LT w0_smallest
w0_not_smallest:
        cmp w1, w2
        B.LT w1_smallest
        mov w0, #3
end:
        ret
w0_smallest:
        mov w0, #1
        b end
w1_smallest:
        mov w0, #2
        b end
        .size low_lvl_4, (. - low_lvl_4)
```

`kernels/low_lvl_5.s`
```assembly
        .text
        .type   low_lvl_5, %function
        .global low_lvl_5
low_lvl_5:
loop_repeat:
        cbz w0, end
        sub w0, w0, #1
        ldr w2, [x1]
        add w2, w2, #1
        str w2, [x1]
        b loop_repeat
end:
        ret
        .size low_lvl_5, (. - low_lvl_5)
```

`kernels/low_lvl_6.s`
```assembly
        .text
        .type   low_lvl_6, %function
        .global low_lvl_6
low_lvl_6:
loop_repeat:
        ldr x3, [x2]
        add x3, x3, x1
        str x3, [x2]
        sub x0, x0, #1
        cbz x0, end
        b loop_repeat
end:
        ret
        .size low_lvl_6, (. - low_lvl_6)
```

`kernels/low_lvl_7.s`
```assembly
        .text
        .type   low_lvl_7, %function
        .global low_lvl_7
low_lvl_7:
loop_repeat:
        cbz x0, end
        sub x0, x0, #1
        ldr x3, [x1], #8
        str x3, [x2], #8
        b loop_repeat
end:
        ret
        .size low_lvl_7, (. - low_lvl_7)
```

### 3.

`driver.cpp`
```cpp
#include <iostream>
#include "kernels/high_level.h"
#include "kernels/low_level.h"


int main() {
  std::cout << "running driver" << std::endl;

  ....

  // low-level part goes here
  std::cout << "low_lvl_0(10): "
            << low_lvl_0( 10 )
            << std::endl;
  std::cout << "low_lvl_1(10): "
            << low_lvl_1( 10 ) << std::endl;

  std::cout << "low_lvl_2(32): "
            << low_lvl_2( 32 ) << std::endl;
  std::cout << "low_lvl_2( 5): "
            << low_lvl_2(  5 ) << std::endl;

  int32_t l_lowLvlOpt3 = 17;
  int32_t l_lowLvlRes3 = -1;
  low_lvl_3( &l_lowLvlOpt3,
              &l_lowLvlRes3 );
  std::cout << "low_lvl_3 #1: "
            << l_lowLvlRes3 << std::endl;
  l_lowLvlOpt3 = 43;
  low_lvl_3( &l_lowLvlOpt3,
              &l_lowLvlRes3 );
  std::cout << "low_lvl_3 #2: "
            << l_lowLvlRes3 << std::endl;

  std::cout << "low_lvl_4(1,2,3): "
            << low_lvl_4( 1, 2, 3 ) << std::endl;
  std::cout << "low_lvl_4(4,2,3): "
            << low_lvl_4( 4, 2, 3 ) << std::endl;
  std::cout << "low_lvl_4(4,3,3): "
            << low_lvl_4( 4, 3, 3 ) << std::endl;

  int32_t l_lowLvlValue5 = 500;
  low_lvl_5(  17,
              &l_lowLvlValue5 );
  std::cout << "low_lvl_5: " << l_lowLvlValue5 << std::endl;

  int64_t l_lowLvlValue6 = 23;
  low_lvl_6( 5,
              13,
              &l_lowLvlValue6 );
  std::cout << "low_lvl_6: "
            << l_lowLvlValue6 << std::endl;

  int64_t l_lowLvlVasIn7[10] = { 0, 7, 7, 4, 3,\
                                 -10, -50, 40, 2, 3 };
  int64_t l_lowLvlVasOut7[10] = { 0 };
  low_lvl_7( 10,
              l_lowLvlVasIn7,
              l_lowLvlVasOut7 );

  std::cout << "low_lvl_7: "
            << l_lowLvlVasOut7[0] << " / "
            << l_lowLvlVasOut7[1] << " / "
            << l_lowLvlVasOut7[2] << " / "
            << l_lowLvlVasOut7[3] << " / "
            << l_lowLvlVasOut7[4] << " / "
            << l_lowLvlVasOut7[5] << " / "
            << l_lowLvlVasOut7[6] << " / "
            << l_lowLvlVasOut7[7] << " / "
            << l_lowLvlVasOut7[8] << " / "
            << l_lowLvlVasOut7[9] << std::endl;


  std::cout << "finished, exiting" << std::endl;
  return EXIT_SUCCESS;
}
```

```
$ make
...
$ ./build/driver
running driver
high_lvl_0(10): 10
high_lvl_1(10): 0
high_lvl_2(32): 0
high_lvl_2( 5): 1
high_lvl_3 #1: 1
high_lvl_3 #2: 0
high_lvl_4(1,2,3): 1
high_lvl_4(4,2,3): 2
high_lvl_4(4,3,3): 3
high_lvl_5: 517
high_lvl_6: 88
high_lvl_7: 0 / 7 / 7 / 4 / 3 / -10 / -50 / 40 / 2 / 3
low_lvl_0(10): 10
low_lvl_1(10): 0
low_lvl_2(32): 0
low_lvl_2( 5): 1
low_lvl_3 #1: 1
low_lvl_3 #2: 0
low_lvl_4(1,2,3): 1
low_lvl_4(4,2,3): 2
low_lvl_4(4,3,3): 3
low_lvl_5: 517
low_lvl_6: 88
low_lvl_7: 0 / 7 / 7 / 4 / 3 / -10 / -50 / 40 / 2 / 3
finished, exiting
```

## 6.2. Small GEMM: ASIMD

The code is in the folder `./aarch64_gemm_asm_asimd//`

### 1.

```
$ mkdir build && cd build
$ cmake -DCMAKE_BUILD_TYPE=Release .. && cmake --build .
...
$ ./aarch64_gemm_micro
name,m,n,k,duration,GFLOPS
gemm_asm_asimd_16_6_1,16,6,1,1.71481,11.1966
```

```assembly
        .text
        .type gemm_asm_asimd_16_6_1, %function
        .global gemm_asm_asimd_16_6_1
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (16x6) = (16x1) * (1x6).
         * The input-data is of type float.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */ 
gemm_asm_asimd_16_6_1:
        // store
        stp x19, x20, [sp, #-16]!
        stp x21, x22, [sp, #-16]!
        stp x23, x24, [sp, #-16]!
        stp x25, x26, [sp, #-16]!
        stp x27, x28, [sp, #-16]!
        stp x29, x30, [sp, #-16]!

        stp  d8,  d9, [sp, #-16]!
        stp d10, d11, [sp, #-16]!
        stp d12, d13, [sp, #-16]!
        stp d14, d15, [sp, #-16]!

        // Load C
        ld1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        ld1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        ld1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        ld1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        ld1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        ld1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64

        // Load A
        ld1 {v25.4s, v26.4s, v27.4s, v28.4s}, [x0], #64

        // Load B
        //ld1 {v29.4s, v30.2s}, [x1]
        ld1 {v29.4s}, [x1], #16
        ld1 {v30.2s}, [x1], #8

        fmla  v1.4s, v25.4s, v29.s[0]
        fmla  v2.4s, v26.4s, v29.s[0]
        fmla  v3.4s, v27.4s, v29.s[0]
        fmla  v4.4s, v28.4s, v29.s[0]

        fmla  v5.4s, v25.4s, v29.s[1]
        fmla  v6.4s, v26.4s, v29.s[1]
        fmla  v7.4s, v27.4s, v29.s[1]
        fmla  v8.4s, v28.4s, v29.s[1]

        fmla  v9.4s, v25.4s, v29.s[2]
        fmla v10.4s, v26.4s, v29.s[2]
        fmla v11.4s, v27.4s, v29.s[2]
        fmla v12.4s, v28.4s, v29.s[2]

        fmla v13.4s, v25.4s, v29.s[3]
        fmla v14.4s, v26.4s, v29.s[3]
        fmla v15.4s, v27.4s, v29.s[3]
        fmla v16.4s, v28.4s, v29.s[3]

        fmla v17.4s, v25.4s, v30.s[0]
        fmla v18.4s, v26.4s, v30.s[0]
        fmla v19.4s, v27.4s, v30.s[0]
        fmla v20.4s, v28.4s, v30.s[0]

        fmla v21.4s, v25.4s, v30.s[1]
        fmla v22.4s, v26.4s, v30.s[1]
        fmla v23.4s, v27.4s, v30.s[1]
        fmla v24.4s, v28.4s, v30.s[1]

        // Load C
        sub x2, x2, #16*6*4
        st1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        st1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        st1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        st1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        st1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        st1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64


        // restore
        ldp d14, d15, [sp], #16
        ldp d12, d13, [sp], #16
        ldp d10, d11, [sp], #16
        ldp  d8,  d9, [sp], #16

        ldp x29, x30, [sp], #16
        ldp x27, x28, [sp], #16
        ldp x25, x26, [sp], #16
        ldp x23, x24, [sp], #16
        ldp x21, x22, [sp], #16
        ldp x19, x20, [sp], #16

        ret
        .size gemm_asm_asimd_16_6_1, (. - gemm_asm_asimd_16_6_1)
```

### 2.

`kernels/gemm_asm_asimd_16_6_1.s`
```assembly
        .text
        .type gemm_asm_asimd_16_6_1, %function
        .global gemm_asm_asimd_16_6_1
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (16x6) = (16x1) * (1x6).
         * The input-data is of type float.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */ 
gemm_asm_asimd_16_6_1:
        // Load C
        ld1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        ld1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        ld1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        ld1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        ld1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        ld1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64

        sub x2, x2, #16*6*4

        // Load A
        ld1 {v25.4s, v26.4s, v27.4s, v28.4s}, [x0], #64

        // Load B
        //ld1 {v29.4s, v30.2s}, [x1]
        ld1 {v29.4s}, [x1], #16
        ld1 {v30.2s}, [x1], #8

        fmla  v1.4s, v25.4s, v29.s[0]
        fmla  v2.4s, v26.4s, v29.s[0]
        fmla  v3.4s, v27.4s, v29.s[0]
        fmla  v4.4s, v28.4s, v29.s[0]

        st1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64

        fmla  v5.4s, v25.4s, v29.s[1]
        fmla  v6.4s, v26.4s, v29.s[1]
        fmla  v7.4s, v27.4s, v29.s[1]
        fmla  v8.4s, v28.4s, v29.s[1]

        st1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64

        fmla  v9.4s, v25.4s, v29.s[2]
        fmla v10.4s, v26.4s, v29.s[2]
        fmla v11.4s, v27.4s, v29.s[2]
        fmla v12.4s, v28.4s, v29.s[2]

        st1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64

        fmla v13.4s, v25.4s, v29.s[3]
        fmla v14.4s, v26.4s, v29.s[3]
        fmla v15.4s, v27.4s, v29.s[3]
        fmla v16.4s, v28.4s, v29.s[3]

        st1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64

        fmla v17.4s, v25.4s, v30.s[0]
        fmla v18.4s, v26.4s, v30.s[0]
        fmla v19.4s, v27.4s, v30.s[0]
        fmla v20.4s, v28.4s, v30.s[0]

        st1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64

        fmla v21.4s, v25.4s, v30.s[1]
        fmla v22.4s, v26.4s, v30.s[1]
        fmla v23.4s, v27.4s, v30.s[1]
        fmla v24.4s, v28.4s, v30.s[1]

        // Store C
        st1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64

        ret
        .size gemm_asm_asimd_16_6_1, (. - gemm_asm_asimd_16_6_1)
```

```
$ ./aarch64_gemm_micro
name,m,n,k,duration,GFLOPS
gemm_asm_asimd_16_6_1,16,6,1,1.03443,18.5609
...
```

### 3.

`kernels/gemm_asm_asimd_16_6_48.s`
```assembly
        .text
        .type gemm_asm_asimd_16_6_48, %function
        .global gemm_asm_asimd_16_6_48
        /*
         * Performs the matrix-multiplication C+=A*B
         * with the shapes (16x6) = (16x1) * (1x6).
         * The input-data is of type float.
         *
         * @param x0 pointer to A.
         * @param x1 pointer to B.
         * @param x2 pointer to C.
         */ 
gemm_asm_asimd_16_6_48:
        // Load C
        ld1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        ld1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        ld1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        ld1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        ld1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        ld1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64
        sub x2, x2, #64*6

        // set count for k_loop
        mov x4, #48

k_loop:
        cbz x4, end
        sub x4, x4, #1

        // Load A
        ld1 {v25.4s, v26.4s, v27.4s, v28.4s}, [x0], #64


        // Load B
        ldr s29, [x1], #48*4
        ldr s30, [x1], #48*4
        ldr s31, [x1], #48*4

        fmla  v1.4s, v25.4s, v29.s[0]
        fmla  v2.4s, v26.4s, v29.s[0]
        fmla  v3.4s, v27.4s, v29.s[0]
        fmla  v4.4s, v28.4s, v29.s[0]

        fmla  v5.4s, v25.4s, v30.s[0]
        fmla  v6.4s, v26.4s, v30.s[0]
        fmla  v7.4s, v27.4s, v30.s[0]
        fmla  v8.4s, v28.4s, v30.s[0]

        fmla  v9.4s, v25.4s, v31.s[0]
        fmla v10.4s, v26.4s, v31.s[0]
        fmla v11.4s, v27.4s, v31.s[0]
        fmla v12.4s, v28.4s, v31.s[0]

        // Load B
        ldr s29, [x1], #48*4
        ldr s30, [x1], #48*4
        ldr s31, [x1], #48*4

        // next line
        add x1, x1, #-6*48*4+4

        fmla v13.4s, v25.4s, v29.s[0]
        fmla v14.4s, v26.4s, v29.s[0]
        fmla v15.4s, v27.4s, v29.s[0]
        fmla v16.4s, v28.4s, v29.s[0]

        fmla v17.4s, v25.4s, v30.s[0]
        fmla v18.4s, v26.4s, v30.s[0]
        fmla v19.4s, v27.4s, v30.s[0]
        fmla v20.4s, v28.4s, v30.s[0]

        fmla v21.4s, v25.4s, v31.s[0]
        fmla v22.4s, v26.4s, v31.s[0]
        fmla v23.4s, v27.4s, v31.s[0]
        fmla v24.4s, v28.4s, v31.s[0]

        // repeat loop
        b k_loop

end:
        // Store C
        st1 { v1.4s,  v2.4s,  v3.4s,  v4.4s}, [x2], #64
        st1 { v5.4s,  v6.4s,  v7.4s,  v8.4s}, [x2], #64
        st1 { v9.4s, v10.4s, v11.4s, v12.4s}, [x2], #64
        st1 {v13.4s, v14.4s, v15.4s, v16.4s}, [x2], #64
        st1 {v17.4s, v18.4s, v19.4s, v20.4s}, [x2], #64
        st1 {v21.4s, v22.4s, v23.4s, v24.4s}, [x2], #64

        ret
        .size gemm_asm_asimd_16_6_48, (. - gemm_asm_asimd_16_6_48)
```

```
$ ./aarch64_gemm_micro
name,m,n,k,duration,GFLOPS
...
gemm_asm_asimd_16_6_48,16,6,48,17.947,51.3511
```

### 4.

#### gemm_asm_asimd_16_6_1

| Team         | Time (s) | #execution | GFLOPS  | %peak |
|--------------|----------|------------|---------|-------|
| peak climber | 1.03443  | 100000000  | 18.5609 | 29    |


#### gemm_asm_asimd_16_6_48
| Team         | Time (s) | #execution | GFLOPS  | %peak |
|--------------|----------|------------|---------|-------|
| peak climber | 17.947   | 100000000  | 51.3511 | 80.2  |

