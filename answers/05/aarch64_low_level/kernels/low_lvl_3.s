        .text
        .type   low_lvl_3, %function
        .global low_lvl_3
low_lvl_3:
        ldr w2, [x0]
        cmp w2, #25
        B.LT less_than
        mov w3, wzr
end:
        str w3, [x1]
        ret
less_than:
        mov w3, #1
        b end
        .size low_lvl_3, (. - low_lvl_3)
