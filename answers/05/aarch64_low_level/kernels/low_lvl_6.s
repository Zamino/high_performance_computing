        .text
        .type   low_lvl_6, %function
        .global low_lvl_6
low_lvl_6:
loop_repeat:
        ldr x3, [x2]
        add x3, x3, x1
        str x3, [x2]
        sub x0, x0, #1
        cbz x0, end
        b loop_repeat
end:
        ret
        .size low_lvl_6, (. - low_lvl_6)
