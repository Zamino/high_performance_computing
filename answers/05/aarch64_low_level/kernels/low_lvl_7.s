        .text
        .type   low_lvl_7, %function
        .global low_lvl_7
low_lvl_7:
loop_repeat:
        cbz x0, end
        sub x0, x0, #1
        ldr x3, [x1], #8
        str x3, [x2], #8
        b loop_repeat
end:
        ret
        .size low_lvl_7, (. - low_lvl_7)
