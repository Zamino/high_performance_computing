        .text
        .type   low_lvl_5, %function
        .global low_lvl_5
low_lvl_5:
loop_repeat:
        cbz w0, end
        sub w0, w0, #1
        ldr w2, [x1]
        add w2, w2, #1
        str w2, [x1]
        b loop_repeat
end:
        ret
        .size low_lvl_5, (. - low_lvl_5)
