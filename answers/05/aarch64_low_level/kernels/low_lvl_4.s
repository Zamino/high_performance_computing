        .text
        .type   low_lvl_4, %function
        .global low_lvl_4
low_lvl_4:
        cmp w0, w1
        B.GE w0_not_smallest
        cmp w0, w2
        B.LT w0_smallest
w0_not_smallest:
        cmp w1, w2
        B.LT w1_smallest
        mov w0, #3
end:
        ret
w0_smallest:
        mov w0, #1
        b end
w1_smallest:
        mov w0, #2
        b end
        .size low_lvl_4, (. - low_lvl_4)
