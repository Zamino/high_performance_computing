        .text
        .type   low_lvl_2, %function
        .global low_lvl_2
low_lvl_2:
        cmp w0, #32
        B.LT less_than
        mov w0, wzr
end:
        ret
less_than:
        mov w0, #1
        b end
        .size low_lvl_2, (. - low_lvl_2)
