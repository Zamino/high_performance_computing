# 02

## 2. Bits

### 2.1 Arrays and Pointers

`./test_size_and_pointer.cpp`
```cpp
#include <iostream>

int main (int /*argc*/, char** /*argv*/) {
  std::cout << "sizeof:" << std::endl;
  std::cout << " - unsigned char: " << sizeof(unsigned char) << std::endl;
  std::cout << " - char         : " << sizeof(char)          << std::endl;
  std::cout << " - unsigned int : " << sizeof(unsigned int)  << std::endl;
  std::cout << " - int          : " << sizeof(int)           << std::endl;
  std::cout << " - float        : " << sizeof(float)         << std::endl;
  std::cout << " - double       : " << sizeof(double)        << std::endl;
  std::cout << " - pointers : " << std::endl;
  std::cout << "   - int        : " << sizeof(int *)         << std::endl;
  std::cout << "   - float      : " << sizeof(float *)       << std::endl;
  std::cout << "   - double     : " << sizeof(double *)      << std::endl;

  int* a = (int*) malloc(1500 * sizeof(int));

  for (int i=0; i<1500; i++) {
      a[i] = 3 * i;
  }

  std::cout << std::endl
    << "Value at:" << std::endl;
  std::cout << " -         postion 250: " << *(a + 250) << std::endl;
  std::cout << " - (void*) postion 250: " << * (int*) ((void*)a + 250 * sizeof(int)) << std::endl;
  std::cout << " -         postion 500: " << *(a + 500) << std::endl;
  std::cout << " -         postion 750: " << *(a + 750) << std::endl;

  free(a);
}
```

```
$ g++ -g -O0 test_size_and_pointer.cpp -o test_size_and_pointer.bin
test_size_and_pointer.cpp: In function ‘int main(int, char**)’:
test_size_and_pointer.cpp:25:65: warning: pointer of type ‘void *’ used in arithmetic [-Wpointer-arith]
   25 |   std::cout << " - (void*) postion 250: " << * (int*) ((void*)a + 250 * sizeof(int)) << std::endl;
      |                                                        ~~~~~~~~~^~~~~~~~~~~~~~~~~~~
$ valgrind --leak-check=yes ./test_size_and_pointer.bin
==314724== Memcheck, a memory error detector
==314724== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==314724== Using Valgrind-3.20.0 and LibVEX; rerun with -h for copyright info
==314724== Command: ./test_size_and_pointer.bin
==314724==
sizeof:
 - unsigned char: 1
 - char         : 1
 - unsigned int : 4
 - int          : 4
 - float        : 4
 - double       : 8
 - pointers :
   - int        : 8
   - float      : 8
   - double     : 8

Value at:
 -         postion 250: 750
 - (void*) postion 250: 750
 -         postion 500: 1500
 -         postion 750: 2250
==314724==
==314724== HEAP SUMMARY:
==314724==     in use at exit: 0 bytes in 0 blocks
==314724==   total heap usage: 3 allocs, 3 frees, 79,728 bytes allocated
==314724==
==314724== All heap blocks were freed -- no leaks are possible
==314724==
==314724== For lists of detected and suppressed errors, rerun with: -s
==314724== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```

### 2.2 Working with Bits

#### 1.

`./print_bits.cpp`
```cpp
#include <iostream>
#include <bitset>

int main (int /*argc*/, char** /*argv*/) {
  unsigned char l_data1  = 1;
  unsigned char l_data2  = 255;
  unsigned char l_data3  = l_data2 + 1;
  unsigned char l_data4  = 0xA1;
  unsigned char l_data5  = 0b1001011;
  unsigned char l_data6  = 'H';
           char l_data7  = -4;
  unsigned int  l_data8  = 1u << 11;
  unsigned int  l_data9 = l_data8 << 21;
  unsigned int  l_data10  = 0xFFFFFFFF >> 5;
  unsigned int  l_data11 = 0b1001 ^ 0b01111;
  unsigned int  l_data12 = ~0b1001;
  unsigned int  l_data13 = 0xF0 & 0b1010101;
  unsigned int  l_data14 = 0b001 | 0b101;
  unsigned int  l_data15 = 7743;
           int  l_data16 = -7743;

  std::cout << "l_data1 : " <<  std::bitset<8>( l_data1) << std::endl;;
  std::cout << "l_data2 : " <<  std::bitset<8>( l_data2) << std::endl;;
  std::cout << "l_data3 : " <<  std::bitset<8>( l_data3) << std::endl;;
  std::cout << "l_data4 : " <<  std::bitset<8>( l_data4) << std::endl;;
  std::cout << "l_data5 : " <<  std::bitset<8>( l_data5) << std::endl;;
  std::cout << "l_data6 : " <<  std::bitset<8>( l_data6) << std::endl;;
  std::cout << "l_data7 : " <<  std::bitset<8>( l_data7) << std::endl;;
  std::cout << "l_data8 : " <<  std::bitset<16>(l_data8) << std::endl;;
  std::cout << "l_data9 : " <<  std::bitset<16>(l_data9) << std::endl;;
  std::cout << "l_data10: " <<  std::bitset<16>(l_data10) << std::endl;;
  std::cout << "l_data11: " <<  std::bitset<16>(l_data11) << std::endl;;
  std::cout << "l_data12: " <<  std::bitset<16>(l_data12) << std::endl;;
  std::cout << "l_data13: " <<  std::bitset<16>(l_data13) << std::endl;;
  std::cout << "l_data14: " <<  std::bitset<16>(l_data14) << std::endl;;
  std::cout << "l_data15: " <<  std::bitset<16>(l_data15) << std::endl;;
  std::cout << "l_data16: " <<  std::bitset<16>(l_data16) << std::endl;;
}
```

```
$ g++ -g -O0 print_bits.cpp -o print_bits.bin
$ ./print_bits.bin
l_data1 : 00000001
l_data2 : 11111111
l_data3 : 00000000
l_data4 : 10100001
l_data5 : 01001011
l_data6 : 01001000
l_data7 : 11111100
l_data8 : 0000100000000000
l_data9 : 0000000000000000
l_data10: 1111111111111111
l_data11: 0000000000000110
l_data12: 1111111111110110
l_data13: 0000000001010000
l_data14: 0000000000000101
l_data15: 0001111000111111
l_data16: 1110000111000001
```

#### 2.

`./instruction_asimd_compute.cpp`
```cpp
#include <bitset>
#include <iostream>
#include <assert.h>

unsigned int instruction_asimd_compute( unsigned int  i_vec_instr,
                                        unsigned char i_vec_reg_dst,
                                        unsigned char i_vec_reg_src_0,
                                        unsigned char i_vec_reg_src_1 ) {
  unsigned int ret_val =
        (0xFFE0FC00 & i_vec_instr)
      | ((0x1F & i_vec_reg_src_1) << 16)
      | ((0x1F & i_vec_reg_src_0) << 5)
      | ( 0x1F & i_vec_reg_dst)
      ;
  return ret_val;
}

int main (int /*argc*/, char** /*argv*/) {
  unsigned int  i_vec_instr     = 0b01001110001000001100110000000000;
  unsigned char i_vec_reg_dst   = 0b00000000;
  unsigned char i_vec_reg_src_0 = 0b00000001;
  unsigned char i_vec_reg_src_1 = 0b00000010;

  auto ret_val = instruction_asimd_compute(
      i_vec_instr, i_vec_reg_dst, i_vec_reg_src_0, i_vec_reg_src_1 );

  std::cout << std::bitset<32>(ret_val) << std::endl;
  assert(ret_val == 0b01001110001000101100110000100000);
}
```

```
$ g++ -g -O0 instruction_asimd_compute.cpp -o instruction_asimd_compute.bin
$ ./instruction_asimd_compute.bin
01001110001000101100110000100000
```

#### 3.

We have created the FMLA-operation, multiplying registers 1 and 2 and adding to
register 0. The vector size is set to 128 bits and single-precision is used,
i.e. the operation operates on 4 single-precision float values.

## 3. Pipelining

### 1.

| Instruction Group            | AArch64 Instructions | Execution Latency | Execution Throughput | Utilized Pipelines | Notes |
|------------------------------|----------------------|-------------------|----------------------|--------------------|-------|
| ASIMD FP multiply            | **FMUL**, FMULX      | 3                 | 4                    | V                  | -     |
| ASIMD FP multiply accumulate | **FMLA**, FMLS       | 4(2)              | 4                    | V                  | -     |

### 2.

If the workload is latency bound, then the next operation can only be executed
when the previous operation is finished. FMUL has a latency of 3 cycles, so only
every 3 cycles a new operation can be started. This results in an execution
throughput of 1/3 operations per cycle. The clock frequency is 2.6GHz, one core
is used and there are 4 FLOPs per operation with single-precision. Thus, the
theoretical peak performance for this problem is 3.47GFLOPS.

peak\_performance\_latency\_src\_fmul\_sp=$2.6GTakt/s∗1∗1OP/3Takt∗4FLOP/OP=3.47GFLOPS$

With FMLA, the latency is 4 cycles and thus the execution throughput is only 1/4
operations per cycle, but the FLOPs per operation increase to 8. This results in
5.2GFLOPS.

peak\_performance\_latency\_src\_fmla\_sp=$2.6GTakt/s∗1∗1OP/4Takt∗8FLOP/OP=5.2GFLOPS$

### 3.

- Kernel: `./aarch64_micro/kernels/latency_src_asimd_fmla_sp.s`

```
$ OMP_NUM_THREADS=1 ./build/micro_asimd
name,threads,duration,GFLOPS
latency_src_asimd_fmla_sp,1,2.31036,5.19399
...
```

The measured value for GFLOPS is only slightly lower than the theoretically
calculated value.

### 4.

- Kernel: `./aarch64_micro/kernels/latency_src_asimd_fmul_sp.s`

```
$ OMP_NUM_THREADS=1 ./build/micro_asimd
name,threads,duration,GFLOPS
...
latency_src_asimd_fmul_sp,1,1.73275,3.46271
...
```

For `fmul` it is the same, the measured value is less than one percentage point
lower.

### 5.

- Kernels:
    - `./aarch64_micro/kernels/dist_2_latency_src_asimd_fmla_sp.s`
    - `./aarch64_micro/kernels/dist_4_latency_src_asimd_fmla_sp.s`

```
[tamino@scalable aarch64_micro]$ OMP_NUM_THREADS=1 ./build/micro_asimd
name,threads,duration,GFLOPS
...
dist_2_latency_src_asimd_fmla_sp,1,0.308048,41.552
dist_4_latency_src_asimd_fmla_sp,1,0.203628,62.8596
...
```

In `dist_2_latency_src_asimd_fmla_sp` the read and write operations to the same
register are 8 operations apart. This should result in 4 operations for 2 cycles
and then two more cycles must be waited for the result until further operations
can be executed. Thus one has 8 operations in 4 cycles. The measured value is
again very close to the calculated one.

peak\_performance\_dist\_2\_latency\_src\_fmla\_sp=$2.6GTakt/s∗1∗2OP/Takt∗8FLOP/OP=41.6GFLOPS$

In `dist_4_latency_src_asimd_fmla_sp` the read and write operations to the same
register are 16 operations apart. This should be enough to use all 4 pipelines
permanently, because after 4 cycles the calculations of the pipelines are
finished. Thus, 4 operations per cycle should be achieved. This time, the
measured GFLOPS are far below the calculated peak performance.

peak\_performance\_dist\_4\_latency\_src\_fmla\_sp=$2.6GTakt/s∗1∗4OP/Takt∗8FLOP/OP=83.2GFLOPS$

### 6.

- Kernels:
    - `./aarch64_micro/kernels/latency_dst_asimd_fmul_sp.s`
    - `./aarch64_micro/kernels/latency_dst_asimd_fmul_sp.s`

```
$ OMP_NUM_THREADS=1 ./build/micro_asimd
name,threads,duration,GFLOPS
...
latency_dst_asimd_fmla_sp,1,1.15524,10.3874
latency_dst_asimd_fmul_sp,1,0.144391,41.5538
```

`fmul` has no latency anymore, because it writes to the destination register
without looking. Further more 4 operations are started at a time, they ignore
that they write in the same target register, i.e. only one pipeline is used.

`fmla` now has a latency of 2 cycles, because the destination register is only
needed for the addition, which is done in the middle of the pipeline. Only one
operation is started at a time, since these require the destination register for
the calculation, i.e. only one pipeline is used.

peak\_performance\_latency\_dst\_fmul\_sp$=2.6GTakt/s∗1∗4OP/Takt∗4FLOP/OP=41.6GFLOPS$

peak\_performance\_latency\_dst\_fmla\_sp=$2.6GTakt/s∗1∗1OP/2Takt∗8FLOP/OP=10.4GFLOPS$

Both measured values are very close to the theoretical peak performance.
