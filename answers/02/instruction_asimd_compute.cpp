#include <bitset>
#include <iostream>
#include <assert.h>

unsigned int instruction_asimd_compute( unsigned int  i_vec_instr,
                                        unsigned char i_vec_reg_dst,
                                        unsigned char i_vec_reg_src_0,
                                        unsigned char i_vec_reg_src_1 ) {
  unsigned int ret_val =
        (0xFFE0FC00 & i_vec_instr)
      | ((0x1F & i_vec_reg_src_1) << 16)
      | ((0x1F & i_vec_reg_src_0) << 5)
      | ( 0x1F & i_vec_reg_dst)
      ;
  return ret_val;
}

int main (int /*argc*/, char** /*argv*/) {
  unsigned int  i_vec_instr     = 0b01001110001000001100110000000000;
  unsigned char i_vec_reg_dst   = 0b00000000;
  unsigned char i_vec_reg_src_0 = 0b00000001;
  unsigned char i_vec_reg_src_1 = 0b00000010;

  auto ret_val = instruction_asimd_compute(
      i_vec_instr, i_vec_reg_dst, i_vec_reg_src_0, i_vec_reg_src_1 );

  std::cout << std::bitset<32>(ret_val) << std::endl;
  assert(ret_val == 0b01001110001000101100110000100000);
}
