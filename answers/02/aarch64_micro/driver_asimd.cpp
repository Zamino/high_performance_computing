#include <cstdint>
#include <cmath>
#include <chrono>
#include <omp.h>
#include <iostream>
#include <string>
#include <functional>

extern "C" {
  uint64_t latency_src_asimd_fmla_sp( uint64_t i_n_repetitions );
  uint64_t latency_src_asimd_fmul_sp( uint64_t i_n_repetitions );
  uint64_t dist_2_latency_src_asimd_fmla_sp( uint64_t i_n_repetitions );
  uint64_t dist_4_latency_src_asimd_fmla_sp( uint64_t i_n_repetitions );
  uint64_t latency_dst_asimd_fmla_sp( uint64_t i_n_repetitions );
  uint64_t latency_dst_asimd_fmul_sp( uint64_t i_n_repetitions );
}

void print_csv_header() {
    std::cout
        <<  "name"
        << ",threads"
        << ",duration"
        << ",GFLOPS"
        << std::endl;
}

void run_benchmark(std::string name, std::function<uint64_t(uint64_t)> func) {
  std::chrono::steady_clock::time_point l_tp0, l_tp1;
  std::chrono::duration< double > l_dur;
  double l_g_flops = 0;
  int l_n_threads = 1;
  uint64_t l_n_repetitions = 5000;
  l_n_repetitions *= 10000;

#pragma omp parallel
  {
#pragma omp master
   {
     l_n_threads = omp_get_num_threads();
   }
  }

  std::cout << name; // name
  std::cout << "," << l_n_threads; // threads

#pragma omp parallel
  {
  // dry-run
  func(1);
#pragma omp barrier
#pragma omp master
    {
      l_tp0 = std::chrono::steady_clock::now();
    }
    l_g_flops = func( l_n_repetitions );
#pragma omp barrier
#pragma omp master
    {
      l_tp1 = std::chrono::steady_clock::now();
    }
  }

  l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );

  std::cout << "," << l_dur.count(); // duration

  l_g_flops *= l_n_threads;
  l_g_flops *= l_n_repetitions;
  l_g_flops *= 1.0E-9;
  l_g_flops /= l_dur.count();
  std::cout << "," << l_g_flops; // GFLOPS

  std::cout << std::endl;
}

int main() {
  print_csv_header();
  run_benchmark("latency_src_asimd_fmla_sp", &latency_src_asimd_fmla_sp);
  run_benchmark("latency_src_asimd_fmul_sp", &latency_src_asimd_fmul_sp);
  run_benchmark("dist_2_latency_src_asimd_fmla_sp", &dist_2_latency_src_asimd_fmla_sp);
  run_benchmark("dist_4_latency_src_asimd_fmla_sp", &dist_4_latency_src_asimd_fmla_sp);
  run_benchmark("latency_dst_asimd_fmla_sp", &latency_dst_asimd_fmla_sp);
  run_benchmark("latency_dst_asimd_fmul_sp", &latency_dst_asimd_fmul_sp);

  return EXIT_SUCCESS;
}
