#include <iostream>

int main (int /*argc*/, char** /*argv*/) {
  std::cout << "sizeof:" << std::endl;
  std::cout << " - unsigned char: " << sizeof(unsigned char) << std::endl;
  std::cout << " - char         : " << sizeof(char)          << std::endl;
  std::cout << " - unsigned int : " << sizeof(unsigned int)  << std::endl;
  std::cout << " - int          : " << sizeof(int)           << std::endl;
  std::cout << " - float        : " << sizeof(float)         << std::endl;
  std::cout << " - double       : " << sizeof(double)        << std::endl;
  std::cout << " - pointers : " << std::endl;
  std::cout << "   - int        : " << sizeof(int *)         << std::endl;
  std::cout << "   - float      : " << sizeof(float *)       << std::endl;
  std::cout << "   - double     : " << sizeof(double *)      << std::endl;

  int* a = (int*) malloc(1500 * sizeof(int));

  for (int i=0; i<1500; i++) {
      a[i] = 3 * i;
  }

  std::cout << std::endl
    << "Value at:" << std::endl;
  std::cout << " -         postion 250: " << *(a + 250) << std::endl;
  std::cout << " - (void*) postion 250: " << * (int*) ((void*)a + 250 * sizeof(int)) << std::endl;
  std::cout << " -         postion 500: " << *(a + 500) << std::endl;
  std::cout << " -         postion 750: " << *(a + 750) << std::endl;

  free(a);
}
