# 03

## 4. Small GEMMs: Getting Started

### 4.1. Matrix Kernels: Reference

#### 1

`./aarch64_gemm_micro/driver.cpp`

#### 2

`./aarch64_gemm_micro/kernels/gemm_ref.cpp`

#### 3

For the floating point comparison the function `Catch::Matchers::WithinULP` is
used with precision 3 and 4. For better understanding I implemented the same
functionality in `./aarch64_gemm_micro/include/compare_floating_point.cpp` which
is also used in the tests.

`./aarch64_gemm_micro/tests.cpp`

### 4.2. Matrix Kernels: Measuring Performance

#### 1

For each value in the matrix $C$ $k$ multiplications and $k$ additions are made,
thus the formula $\text{FLOP} = 2 * m * n * k$ is obtained.

#### 2, 3

The number of iterations was adjusted to the size of the matrices by dividing
the iterations by the number of FLOP per loop.

```cpp
  ...
  uint64_t flop_per_iteration = m * n * k * 2; // multiplication + addition;
  uint64_t l_n_repetitions = 500000000;
  l_n_repetitions /= flop_per_iteration; // reduce repetitions by array sizes

  l_tp0 = std::chrono::steady_clock::now();
  for (uint64_t i = 0; i < l_n_repetitions; i++) {
      func(
              a, b, c,
              m, n, k,
              m, k, m
          );
  }
  l_tp1 = std::chrono::steady_clock::now();

  l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );
  ...
```

#### 4

To build the project, we create the build directory inside the directory
`./aarch64_gemm_micro/` and change the working directory to it. Then we run
`cmake` to build it.

```
$ mkdir ./aarch64_gemm_micro/build
$ cd ./aarch64_gemm_micro/build
$ cmake -DCMAKE_BUILD_TYPE=Debug .. && cmake --build .
...
```

We build the project without compiler optimizations by passing the
`-DCMAKE_BUILD_TYPE=Debug` argument to `cmake`.

If we run the benchmark `./aarch64_gemm_micro`, we see that all except the small
matrices arrive at the same GFLOPS.

```
$ ./aarch64_gemm_micro
name,m,n,k,duration,GFLOPS
gemm_ref,4,4,k,1.27712,0.391504
gemm_ref,8,8,k,0.962965,0.519229
gemm_ref,12,12,k,0.917973,0.544675
gemm_ref,16,16,k,0.916584,0.545502
gemm_ref,24,24,k,0.90885,0.550131
gemm_ref,32,32,k,0.905706,0.552027
gemm_ref,48,48,k,0.908829,0.550022
gemm_ref,64,64,k,0.910358,0.548846
...
```

| name      | m  | n  | k  | duration | GFLOPS   |
|-----------|----|----|----|----------|----------|
| gemm\_ref | 4  | 4  | 4  | 1.27712  | 0.391504 |
| gemm\_ref | 8  | 8  | 8  | 0.962965 | 0.519229 |
| gemm\_ref | 12 | 12 | 12 | 0.917973 | 0.544675 |
| gemm\_ref | 16 | 16 | 16 | 0.916584 | 0.545502 |
| gemm\_ref | 24 | 24 | 24 | 0.90885  | 0.550131 |
| gemm\_ref | 32 | 32 | 32 | 0.905706 | 0.552027 |
| gemm\_ref | 48 | 48 | 48 | 0.908829 | 0.550022 |
| gemm\_ref | 64 | 64 | 64 | 0.910358 | 0.548846 |

We can also run the `./aarch64_gemm_test` test and see that all tests are
successful.

```
$ ./aarch64_gemm_test
Randomness seeded to: 2784120778
===============================================================================
All tests passed (26752 assertions in 8 test cases)
```

If we want to enable the compiler optimizations, we can change the flag for
`cmake` to `-DCMAKE_BUILD_TYPE=Release` and run the benchmark again. It uses the
compiler optimization flag `-O3`. For `-O2` we can pass
`-DCMAKE_BUILD_TYPE=RelWithDebInfo` to `cmake`. Here we see a sweet spot at 16
and each run is about 7-8x faster.

```
$ cmake -DCMAKE_BUILD_TYPE=Release .. && cmake --build .
...
$ ./aarch64_gemm_micro
name,m,n,k,duration,GFLOPS
gemm_ref,4,4,k,0.160508,3.11511
gemm_ref,8,8,k,0.126495,3.95272
gemm_ref,12,12,k,0.115751,4.3196
gemm_ref,16,16,k,0.110358,4.5307
gemm_ref,24,24,k,0.114814,4.35476
gemm_ref,32,32,k,0.123846,4.03706
gemm_ref,48,48,k,0.147787,3.38241
gemm_ref,64,64,k,0.149165,3.34963
...
```

| name     | m  | n  | k  | duration | GFLOPS  |
|----------|----|----|----|----------|---------|
| gemm_ref | 4  | 4  | 4  | 0.160508 | 3.11511 |
| gemm_ref | 8  | 8  | 8  | 0.126495 | 3.95272 |
| gemm_ref | 12 | 12 | 12 | 0.115751 | 4.3196  |
| gemm_ref | 16 | 16 | 16 | 0.110358 | 4.5307  |
| gemm_ref | 24 | 24 | 24 | 0.114814 | 4.35476 |
| gemm_ref | 32 | 32 | 32 | 0.123846 | 4.03706 |
| gemm_ref | 48 | 48 | 48 | 0.147787 | 3.38241 |
| gemm_ref | 64 | 64 | 64 | 0.149165 | 3.34963 |

### 4.3. Matrix Kernels: Helping the Compiler

#### 1

`./aarch64_gemm_micro/kernels/gemm_compiler_32_32_32_32_32_32.cpp`
```cpp
void gemm_compiler_32_32_32_32_32_32_mnk(
        float const * i_a, float const * i_b, float * io_c ) {
  for (int m=0; m<32; m++) {
    for (int n=0; n<32; n++) {
      for (int k=0; k<32; k++) {
        io_c[m + n * 32] += i_a[m + k * 32] * i_b[k + n * 32];
      }
    }
  }
}
```

#### 2

`./aarch64_gemm_micro/kernels/gemm_compiler_32_32_32_32_32_32.cpp`
```cpp
void gemm_compiler_32_32_32_32_32_32_nkm(
        float const * i_a, float const * i_b, float * io_c ) {
    for (int n=0; n<32; n++) {
        for (int k=0; k<32; k++) {
            for (int m=0; m<32; m++) {
        io_c[m + n * 32] += i_a[m + k * 32] * i_b[k + n * 32];
      }
    }
  }
}
```

#### 3

`./aarch64_gemm_micro/tests.cpp`


#### 4

With the `nkm` access pattern, we see an improvement in GFLOPS, especially with
compiler optimizations enabled.

```
$ cmake -DCMAKE_BUILD_TYPE=Debug .. && cmake --build .
...
$ ./aarch64_gemm_micro
name,m,n,k,duration,GFLOPS
...
gemm_compiler_32_32_32_32_32_32_mnk,32,32,k,0.85859,0.58232
gemm_compiler_32_32_32_32_32_32_nkm,32,32,k,0.800061,0.62492
```

| name                                | m  | n  | k  | duration | GFLOPS  |
|-------------------------------------|----|----|----|----------|---------|
| gemm_compiler_32_32_32_32_32_32_mnk | 32 | 32 | 32 | 0.85859  | 0.58232 |
| gemm_compiler_32_32_32_32_32_32_nkm | 32 | 32 | 32 | 0.800061 | 0.62492 |

```
$ cmake -DCMAKE_BUILD_TYPE=Release .. && cmake --build .
...
$ ./aarch64_gemm_micro
name,m,n,k,duration,GFLOPS
...
gemm_compiler_32_32_32_32_32_32_mnk,32,32,k,0.121917,4.10095
gemm_compiler_32_32_32_32_32_32_nkm,32,32,k,0.0354539,14.1021
```

| name                                | m  | n  | k  | duration  | GFLOPS  |
|-------------------------------------|----|----|----|-----------|---------|
| gemm_compiler_32_32_32_32_32_32_mnk | 32 | 32 | 32 | 0.121917  | 4.10095 |
| gemm_compiler_32_32_32_32_32_32_nkm | 32 | 32 | 32 | 0.0354539 | 14.1021 |

We could use an intrinsic function to force the compiler to use vector
operations, or say that the array is restricted with the keyword `__restrict__`
in the function declaration. If we don't use the function in different parts of
the code, we can inline it.

### 4.4 Code Generation

#### 1

```
$ git clone https://github.com/libxsmm/libxsmm
...
$ cd libxsmm
$ make BLAS=0
...
```

#### 2

The dispatch function generates a function Just In Time (JIT) and returns a
reference to it. The JIT-generated code is adapted to the parameters of the
dispatch function to achieve the highest possible performance.

```
$ cd samples/hello/
$ make BLAS=0
...
$ ./hello
...
```

#### 3
