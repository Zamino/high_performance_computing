# include(FetchContent)
# FetchContent_Declare(
#   xsmm
#   URL https://github.com/chelini/libxsmm/archive/<your-preferred-revision>.tar.gz
#   URL_HASH SHA256=<sha256sum-corresponding-to-above-revision>
# )
# FetchContent_GetProperties(xsmm)
# if(NOT xsmm_POPULATED)
#   FetchContent_Populate(xsmm)
# endif()
#
#
# set(LIBXSMMROOT ${xsmm_SOURCE_DIR})
# file(GLOB _GLOB_XSMM_SRCS LIST_DIRECTORIES false CONFIGURE_DEPENDS ${LIBXSMMROOT}/src/*.c)
# list(REMOVE_ITEM _GLOB_XSMM_SRCS ${LIBXSMMROOT}/src/libxsmm_generator_gemm_driver.c)
# set(XSMM_INCLUDE_DIRS ${LIBXSMMROOT}/include)
#
# add_library(xsmm STATIC ${_GLOB_XSMM_SRCS})
# target_include_directories(xsmm PUBLIC ${XSMM_INCLUDE_DIRS})
# target_compile_definitions(xsmm PUBLIC
#   LIBXSMM_DEFAULT_CONFIG
# )
# target_compile_definitions(xsmm PRIVATE
#   __BLAS=0
# )

include(ExternalProject)
find_package(Git REQUIRED)

ExternalProject_Add(
        libxsmm_src
        PREFIX "vendor/libxsmm"
        GIT_REPOSITORY "https://github.com/libxsmm/libxsmm"
        GIT_TAG main_stable
        TIMEOUT 10
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        UPDATE_COMMAND ""
)

ExternalProject_Get_Property(libxsmm_src source_dir)
add_library(libxsmm INTERFACE)
set_property(TARGET libxsmm APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${source_dir}/include)
add_dependencies(libxsmm libxsmm_src)
