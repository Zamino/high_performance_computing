#include <cstdint>
#include <cmath>
#include <chrono>
#include <omp.h>
#include <iostream>
#include <string>
#include <functional>

#include "kernels/gemm_ref.cpp"
#include "kernels/gemm_compiler_32_32_32_32_32_32.cpp"

void gemm_compiler_32_32_32_32_32_32_mnk_wrapper(
    float*  const i_a,   float*   const i_b,   float*  io_c,
    [[maybe_unused]] int64_t i_m,   [[maybe_unused]] int64_t i_n,   [[maybe_unused]] int64_t i_k,
    [[maybe_unused]] int64_t i_lda, [[maybe_unused]] int64_t i_ldb, [[maybe_unused]] int64_t i_ldc
    ) {
    gemm_compiler_32_32_32_32_32_32_mnk(i_a, i_b, io_c);
}

void gemm_compiler_32_32_32_32_32_32_nkm_wrapper(
    float*  const i_a,   float*   const i_b,   float*  io_c,
    [[maybe_unused]] int64_t i_m,   [[maybe_unused]] int64_t i_n,   [[maybe_unused]] int64_t i_k,
    [[maybe_unused]] int64_t i_lda, [[maybe_unused]] int64_t i_ldb, [[maybe_unused]] int64_t i_ldc
    ) {
    gemm_compiler_32_32_32_32_32_32_nkm(i_a, i_b, io_c);
}

void print_csv_header() {
    std::cout
        << "name"
        << ",m" << ",n" << ",k"
        << ",duration"
        << ",GFLOPS"
        << std::endl;
}

void run_benchmark(
    std::string name, std::function<void(
      float*  const, float*   const , float*,  // data
      int64_t      , int64_t        , int64_t, // dimensions
      int64_t      , int64_t        , int64_t  // row lengths
      )> func,
    uint64_t m, uint64_t n, uint64_t k) {
  float* a = new float[k * m];
  float* b = new float[n * k];
  float* c = new float[n * m];
  std::chrono::steady_clock::time_point l_tp0, l_tp1;
  std::chrono::duration< double > l_dur;
  uint64_t flop_per_iteration = m * n * k * 2; // multiplication + addition;
  uint64_t l_n_repetitions = 500000000;
  l_n_repetitions /= flop_per_iteration; // reduce repetitions by array sizes

  l_tp0 = std::chrono::steady_clock::now();
  for (uint64_t i = 0; i < l_n_repetitions; i++) {
      func(
              a, b, c,
              m, n, k,
              m, k, m
          );
  }
  l_tp1 = std::chrono::steady_clock::now();

  l_dur = std::chrono::duration_cast< std::chrono::duration< double> >( l_tp1 - l_tp0 );

  double l_g_flops = flop_per_iteration;
  l_g_flops *= l_n_repetitions;
  l_g_flops *= 1.0E-9;
  l_g_flops /= l_dur.count();

  std::cout << name; // name
  std::cout << "," << m << "," << n << "," << k; // dimensions
  std::cout << "," << l_dur.count(); // duration
  std::cout << "," << l_g_flops; // GFLOPS
  std::cout << std::endl;

  delete[] a; delete[] b; delete[] c;
}

int main() {
  print_csv_header();
  run_benchmark("gemm_ref", &gemm_ref,  4,  4,  4);
  run_benchmark("gemm_ref", &gemm_ref,  8,  8,  8);
  run_benchmark("gemm_ref", &gemm_ref, 12, 12, 12);
  run_benchmark("gemm_ref", &gemm_ref, 16, 16, 16);
  run_benchmark("gemm_ref", &gemm_ref, 24, 24, 24);
  run_benchmark("gemm_ref", &gemm_ref, 32, 32, 32);
  run_benchmark("gemm_ref", &gemm_ref, 48, 48, 48);
  run_benchmark("gemm_ref", &gemm_ref, 64, 64, 64);
  run_benchmark("gemm_compiler_32_32_32_32_32_32_mnk", &gemm_compiler_32_32_32_32_32_32_mnk_wrapper, 32, 32, 32);
  run_benchmark("gemm_compiler_32_32_32_32_32_32_nkm", &gemm_compiler_32_32_32_32_32_32_nkm_wrapper, 32, 32, 32);

  return EXIT_SUCCESS;
}
