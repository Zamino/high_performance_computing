#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <stdint.h>

#include "include/compare_floating_point.cpp"

#include "kernels/gemm_ref.cpp"
#include "kernels/gemm_compiler_32_32_32_32_32_32.cpp"

void create_matrices(
        float** a, float** b, float** c,
        int64_t m, int64_t n, int64_t k
        ) {
  *a = new float[k * m];
  *b = new float[n * k];
  *c = new float[n * m];
}

void fill_matix(
    float* a,
    int64_t i_m, int64_t i_n,
    int64_t i_lda,
    float value
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      a[m + i_lda * n] = value;
    }
  }
}

void compare_matrices(
    float* a, float* b,
    int64_t i_m, int64_t i_n,
    int64_t i_lda, int64_t i_ldb,
    int precision
    ) {
  for (int64_t m = 0; m < i_m; m++) {
    for (int64_t n = 0; n < i_n; n++) {
      REQUIRE_THAT(a[m + i_lda * n],
                   Catch::Matchers::WithinULP(b[m + i_lda * n], precision));
      REQUIRE(AlmostEqualUlps(a[m + i_lda * n], b[m + i_lda * n], precision));
    }
  }
}

void delete_matrices(float** a, float** b, float** c) {
    delete[] *a;
    delete[] *b;
    delete[] *c;
}

TEST_CASE( "gemm_ref 32 32 32 [0] += [1] * [1]", "[gemm_ref]" ) {
  int64_t m = 32; int64_t n = 32; int64_t k = 32;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matix(a, k, n, k, 1);
  fill_matix(b, m, k, m, 1);
  fill_matix(c, m, n, m, 0);

  gemm_ref(
      a, b, c,
      m, n, k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matix(
      c_result,
      m, n,
      m,
      k
      );

  compare_matrices(
      c, c_result,
      m, n,
      m, m,
      3
      );

  delete_matrices(&a, &b, &c);
  delete[] c_result;
}

TEST_CASE( "gemm_ref 32 32 32 [0.3] += [0.3] * [0.3]", "[gemm_ref]" ) {
  int64_t m = 32; int64_t n = 32; int64_t k = 32;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matix(a, k, n, k, 0.3);
  fill_matix(b, m, k, m, 0.3);
  fill_matix(c, m, n, m, 0.3);

  gemm_ref(
      a, b, c,
      m, n, k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matix(
      c_result,
      m, n,
      m,
      0.3 + k * (0.3 * 0.3)
      );

  compare_matrices(
      c, c_result,
      m, n,
      m, m,
      3
      );

  delete_matrices(&a, &b, &c);
  delete[] c_result;
}

TEST_CASE( "gemm_ref 64 64 64 [0] += [1] * [1]", "[gemm_ref]" ) {
  int64_t m = 64; int64_t n = 64; int64_t k = 64;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matix(a, k, n, k, 1);
  fill_matix(b, m, k, m, 1);
  fill_matix(c, m, n, m, 0);

  gemm_ref(
      a, b, c,
      m, n, k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matix(
      c_result,
      m, n,
      m,
      k
      );

  compare_matrices(
      c, c_result,
      m, n,
      m, m,
      3
      );

  delete_matrices(&a, &b, &c);
  delete[] c_result;
}

TEST_CASE( "gemm_ref 64 64 64 [0.3] += [0.3] * [0.3]", "[gemm_ref]" ) {
  int64_t m = 64; int64_t n = 64; int64_t k = 64;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matix(a, k, n, k, 0.3);
  fill_matix(b, m, k, m, 0.3);
  fill_matix(c, m, n, m, 0.3);

  gemm_ref(
      a, b, c,
      m, n, k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matix(
      c_result,
      m, n,
      m,
      0.3 + k * (0.3 * 0.3)
      );

  compare_matrices(
      c, c_result,
      m, n,
      m, m,
      4
      );

  delete_matrices(&a, &b, &c);
  delete[] c_result;
}

TEST_CASE( "gemm_ref 32 32 32 64 64 64 [0] += [1] * [1]", "[gemm_ref]" ) {
  int64_t   m = 64; int64_t   n = 64; int64_t   k = 64;
  int64_t i_m = 32; int64_t i_n = 32; int64_t i_k = 32;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matix(a, k, n, k, 1);
  fill_matix(b, m, k, m, 1);
  fill_matix(c, m, n, m, 0);

  gemm_ref(
      a, b, c,
      i_m, i_n, i_k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matix(
      c_result,
      m, n,
      m,
      i_k
      );

  compare_matrices(
      c, c_result,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(&a, &b, &c);
  delete[] c_result;
}


TEST_CASE( "gemm_ref 4 16 32 64 64 64 [0] += [1] * [1]", "[gemm_ref]" ) {
  int64_t   m = 64; int64_t   n = 64; int64_t   k = 64;
  int64_t i_m =  4; int64_t i_n = 16; int64_t i_k = 16;
  float *a, *b, *c;
  create_matrices(&a, &b, &c, m, n, k);
  fill_matix(a, k, n, k, 1);
  fill_matix(b, m, k, m, 1);
  fill_matix(c, m, n, m, 0);

  gemm_ref(
      a, b, c,
      i_m, i_n, i_k,
      m, k, m
      );

  float* c_result = new float[m * n];
  fill_matix(
      c_result,
      m, n,
      m,
      i_k
      );

  compare_matrices(
      c, c_result,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(&a, &b, &c);
  delete[] c_result;
}

TEST_CASE( "gemm_compiler_32_32_32_32_32_32_mnk [0.3] += [0.3] * [0.3]", "[gemm_mnk]") {
  int64_t   m = 32; int64_t   n = 32; int64_t   k = 32;
  int64_t i_m = 32; int64_t i_n = 32; int64_t i_k = 32;
  float *a, *b, *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  fill_matix(a, k, n, k, 0.3);
  fill_matix(b, m, k, m, 0.3);
  fill_matix(c_ref, m, n, m, 0.3);

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  float* c_mnk = new float[m * n];
  fill_matix(
      c_mnk,
      m, n,
      m,
      0.3
      );

  gemm_compiler_32_32_32_32_32_32_mnk(a, b, c_mnk);

  compare_matrices(
      c_ref, c_mnk,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(&a, &b, &c_ref);
  delete[] c_mnk;
}

TEST_CASE( "gemm_compiler_32_32_32_32_32_32_nkm [0.3] += [0.3] * [0.3]", "[gemm_nkm]") {
  int64_t   m = 32; int64_t   n = 32; int64_t   k = 32;
  int64_t i_m = 32; int64_t i_n = 32; int64_t i_k = 32;
  float *a, *b, *c_ref;
  create_matrices(&a, &b, &c_ref, m, n, k);
  fill_matix(a, k, n, k, 0.3);
  fill_matix(b, m, k, m, 0.3);
  fill_matix(c_ref, m, n, m, 0.3);

  gemm_ref(
      a, b, c_ref,
      i_m, i_n, i_k,
      m, k, m
      );

  float* c_nkm = new float[m * n];
  fill_matix(
      c_nkm,
      m, n,
      m,
      0.3
      );

  gemm_compiler_32_32_32_32_32_32_nkm(a, b, c_nkm);

  compare_matrices(
      c_nkm, c_ref,
      i_m, i_n,
      m, m,
      3
      );

  delete_matrices(&a, &b, &c_ref);
  delete[] c_nkm;
}
